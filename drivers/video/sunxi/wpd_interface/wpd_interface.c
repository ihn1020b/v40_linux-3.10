#include <linux/module.h>
#include <linux/init.h>
#include <linux/pm.h>
#include <linux/fs.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/device.h>
#include <linux/clk.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/regulator/consumer.h>
#include <linux/uaccess.h>
#include <linux/bug.h>
#include <linux/errno.h>
#include <linux/resource.h>
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <linux/of_net.h>
#include <linux/of_gpio.h>
#include <linux/io.h>
#include <linux/sys_config.h>


#define IOCTL_WPD_MAGIC     'G'  

typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_wpd_info; 
 
 
#define IOCTL_WPD_RD			_IOR(IOCTL_WPD_MAGIC, 0 , ioctl_wpd_info )  
#define IOCTL_WPD_WR			_IOW(IOCTL_WPD_MAGIC, 1 , ioctl_wpd_info )  


#define 	TYPE_GPIO_CMD		(0xFF << 8)
#define 	TYPE_USER_CMD		(0xF5 << 8)


#define 	IOCTL_SPKAMPEN_ONOFF	(TYPE_GPIO_CMD|0x01)
#define 	IOCTL_CAMPWEN_ONOFF	(TYPE_GPIO_CMD|0x02)
#define 	IOCTL_RFNRESET_ONOFF	(TYPE_GPIO_CMD|0x03)
#define 	IOCTL_VIDEOOUTSEL_ONOFF	(TYPE_GPIO_CMD|0x04)
#define 	IOCTL_ENCLEDB_ONOFF	(TYPE_GPIO_CMD|0x05)
#define 	IOCTL_ENCLEDR_ONOFF	(TYPE_GPIO_CMD|0x06)
#define 	IOCTL_LEDNEMR_ONOFF	(TYPE_GPIO_CMD|0x07)
#define 	IOCTL_PWMGG_ONOFF	(TYPE_GPIO_CMD|0x08)
#define 	IOCTL_ENCLEDA_ONOFF	(TYPE_GPIO_CMD|0x09)
#define 	IOCTL_34118MUTE_ONOFF	(TYPE_GPIO_CMD|0x0A)

#define 	IOCTL_ENCA_GET		(TYPE_GPIO_CMD|0x0B)
#define 	IOCTL_ENCB_GET		(TYPE_GPIO_CMD|0x0C)
#define 	IOCTL_V40EENC_GET	(TYPE_GPIO_CMD|0x0D)
#define 	IOCTL_BTNEMRGA_GET	(TYPE_GPIO_CMD|0x0E)
#define 	IOCTL_DOFFLEROUT_GET	(TYPE_GPIO_CMD|0x0F)


#define		IOCTL_ALL_DOWN		(TYPE_USER_CMD|0x01)
#define		IOCTL_ALL_UP		(TYPE_USER_CMD|0x02)
#define		IOCTL_ALL_PRINT		(TYPE_USER_CMD|0x03)

#if 0
#define 	TYPE_GPIO_CMD		(0xFF << 8)
#define 	TYPE_VIDEO_CMD		(0xFE << 8)
#define 	TYPE_AUDIO_CMD		(0xFD << 8)
#define 	TYPE_BACKLIGHT_CMD	(0xFC << 8)	
#define 	TYPE_ETHERNET_CMD	(0xFB << 8)
#define 	TYPE_DETECTION_TIME_CMD	(0xFA << 8)
#define 	TYPE_CAMERA_CMD		(0xF9 << 8)
#define 	TYPE_SENSOR_STATE_CMD	(0xF8 << 8)
#define 	TYPE_KEY_CONTROL_CMD	(0xF7 << 8)
#define		TYPE_OTHER_CMD		(0xF5 << 8)
#define 	IOCTL_DOOR_DINGDONG		(TYPE_GPIO_CMD|0x01) //ff
#define		IOCTL_BTNEMR_LEDONOFF		(TYPE_GPIO_CMD|0x02)
#define		IOCTL_BTNCALL_LEDONOFF		(TYPE_GPIO_CMD|0x03)
#define 	IOCTL_BTNOPEN_LEDONOFF		(TYPE_GPIO_CMD|0x04)
#define 	IOCTL_SUBTOUCH_POWERONOFF	(TYPE_GPIO_CMD|0x05)
#define 	IOCTL_VIDEOOUT_DOORONOFF	(TYPE_GPIO_CMD|0x06)
#define 	IOCTL_CAMERA_POWERONOFF		(TYPE_GPIO_CMD|0x07)
#define 	IOCTL_SUBPHONE_MUTEONOFF	(TYPE_GPIO_CMD|0x08)
#define 	IOCTL_DOOR_LOCK_OPEN_REGIS	(TYPE_GPIO_CMD|0x09)
#define 	IOCTL_RFREMOCON_RESET		(TYPE_GPIO_CMD|0x0A)
#define 	IOCTL_DOOR_POWERONOFF		(TYPE_GPIO_CMD|0x0B)
#define		IOCTL_FRONTHOME_LEDONOFF	(TYPE_GPIO_CMD|0x0C)
#define		IOCTL_FRONTEMR_LEDONOFF		(TYPE_GPIO_CMD|0x0D)
#define		IOCTL_BTNEMR_LED_TRIGGER	(TYPE_GPIO_CMD|0x0E)
#define		IOCTL_DOOR_POWER_STATE		(TYPE_GPIO_CMD|0x0F)
#define		IOCTL_DOOR_PATH_ENABLE		(TYPE_GPIO_CMD|0x10)
#define		IOCTL_DOOR_CX93021_RESET	(TYPE_GPIO_CMD|0x11)
	
#define 	IOCTL_VIDEOINPUT_DOORCAMERA	(TYPE_VIDEO_CMD|0x01) //fe

#define 	IOCTL_MIC_GAIN_SET		(TYPE_AUDIO_CMD|0x01) //fd
#define 	IOCTL_MIC_GAIN_GET		(TYPE_AUDIO_CMD|0x02)
#define 	IOCTL_MIC_RECORD_GAIN_SET	(TYPE_AUDIO_CMD|0x03)
#define 	IOCTL_MIC_RECORD_GAIN_GET	(TYPE_AUDIO_CMD|0x04)
#define 	IOCTL_MIC_BOOST_GAIN_SET	(TYPE_AUDIO_CMD|0x05)
#define 	IOCTL_MIC_BOOST_GAIN_GET	(TYPE_AUDIO_CMD|0x06)
#define 	IOCTL_MIC_LINE1_GAIN_SET	(TYPE_AUDIO_CMD|0x07)
#define 	IOCTL_MIC_LINE1_GAIN_GET	(TYPE_AUDIO_CMD|0x08)
#define 	IOCTL_SPK_EQ_GAIN_SET		(TYPE_AUDIO_CMD|0x09)
#define 	IOCTL_SPK_EQ_GAIN_GET		(TYPE_AUDIO_CMD|0x0A)
#define 	IOCTL_MIC_DSP_MAX_GAIN_SET	(TYPE_AUDIO_CMD|0x0B)
#define 	IOCTL_MIC_DSP_MAX_GAIN_GET	(TYPE_AUDIO_CMD|0x0C)
#define 	IOCTL_MONOOUT_SUB_GAIN_SET	(TYPE_AUDIO_CMD|0x0D)
#define 	IOCTL_MONOOUT_SUB_GAIN_GET	(TYPE_AUDIO_CMD|0x0E)

#define 	IOCTL_MOOD_BACKLIGHT_SET	(TYPE_BACKLIGHT_CMD|0x01) //fc

#define 	IOCTL_ETHERNET_MAC_SET		(TYPE_ETHERNET_CMD|0x01) //fb

#define 	IOCTL_DOOR_DETECTIME_SET	    (TYPE_DETECTION_TIME_CMD|0x01)  //fa
#define 	IOCTL_SENSOR_MAG0_DETECTIME_SET	    (TYPE_DETECTION_TIME_CMD|0x02)
#define 	IOCTL_SENSOR_EXIT_DETECTIME_SET	    (TYPE_DETECTION_TIME_CMD|0x04)
#define 	IOCTL_SENSOR_MOTIN_DETECTIME_SET    (TYPE_DETECTION_TIME_CMD|0x05)
#define 	IOCTL_SENSOR_MOTOUT_DETECTIME_SET   (TYPE_DETECTION_TIME_CMD|0x06)
#define 	IOCTL_SENSOR_EMR_DETECTIME_SET	    (TYPE_DETECTION_TIME_CMD|0x07)

#define 	IOCTL_CAMERA_BRIGHTNESS_SET	    (TYPE_CAMERA_CMD|0x01) //f9
#define 	IOCTL_CAMERA_CONTRAST_SET           (TYPE_CAMERA_CMD|0x02)

#define 	IOCTL_SENSOR_MAG0_STATE             (TYPE_SENSOR_STATE_CMD|0x01) //f8
#define 	IOCTL_SENSOR_EXIT_STATE             (TYPE_SENSOR_STATE_CMD|0x03)
#define 	IOCTL_SENSOR_MOTIN_STATE            (TYPE_SENSOR_STATE_CMD|0x04)
#define 	IOCTL_SENSOR_MOTOUT_STATE           (TYPE_SENSOR_STATE_CMD|0x05)
#define 	IOCTL_SENSOR_EMR_STATE              (TYPE_SENSOR_STATE_CMD|0x06)

#define 	IOCTL_KEY_ONOFF_SET                 (TYPE_KEY_CONTROL_CMD|0x01) //f7
#define 	IOCTL_KEY_ONOFF_GET                 (TYPE_KEY_CONTROL_CMD|0x02)


#define 	IOCTL_DEVICE_ALL_OPEN		(TYPE_OTHER_CMD | 0x01) //f5
#define 	IOCTL_DEVICE_ALL_CLOSE		(TYPE_OTHER_CMD | 0x02)
#define		IOCTL_AUDIO_PATH_NORMAL		(TYPE_OTHER_CMD | 0x03)
#define		IOCTL_AUDIO_PATH_DOOR_WALLPAD	(TYPE_OTHER_CMD | 0x04)
#define		IOCTL_AUDIO_PATH_MOIP_PSTN	(TYPE_OTHER_CMD | 0x05)
#define		IOCTL_AUDIO_PATH_GUARD_WALLPAD	(TYPE_OTHER_CMD | 0x06)
#define		IOCTL_AUDIO_PATH_GUARD_SUB	(TYPE_OTHER_CMD | 0x07)
#define		IOCTL_AUDIO_PATH_DOOR_SUB	(TYPE_OTHER_CMD | 0x08)

#define		IOCTL_HW_REVISION		(TYPE_OTHER_CMD | 0x09)

#define		IOCTL_LED_PWR_CONT		(TYPE_OTHER_CMD | 0x0a)
#define		IOCTL_SUB_PWR_CONT		(TYPE_OTHER_CMD | 0x0b)
#define		IOCTL_SENSE_PWR_CONT		(TYPE_OTHER_CMD | 0x0c)
#define		IOCTL_USB_ON_OFF		(TYPE_OTHER_CMD | 0x0d)
#define		IOCTL_IHN_MODEL			(TYPE_OTHER_CMD | 0x0f)
#define		IOCTL_CAM_STATE			(TYPE_OTHER_CMD | 0xf5)
#define		IOCTL_ANALOG_CAM_STAT		(TYPE_OTHER_CMD | 0xf6)
#define		IOCTL_ANALOG_CAM_SET		(TYPE_OTHER_CMD | 0xf7)
#endif

struct gpio_config wpd_spkamp_en;
struct gpio_config wpd_cam_pwen;
struct gpio_config wpd_rf_nreset;
struct gpio_config wpd_video_out_sel;
struct gpio_config wpd_enc_ledb;
struct gpio_config wpd_enc_ledr;
struct gpio_config wpd_led_nemr;
struct gpio_config wpd_pwm_gg;
struct gpio_config wpd_enc_leda;
struct gpio_config wpd_34118_mute;

struct gpio_config wpd_enc_a;
struct gpio_config wpd_enc_b;
struct gpio_config wpd_v40_eenc;
struct gpio_config wpd_bt_nemrga;
struct gpio_config wpd_doffler_out;

struct gpio_config wpd_ga_dtmf0;
struct gpio_config wpd_ga_dtmf1;
struct gpio_config wpd_ga_dtmf2;
struct gpio_config wpd_ga_dtmf3;
struct gpio_config wpd_ga_dtmfnce;

struct gpio_config wpd_ht9_irq;
struct gpio_config wpd_ht9_pwdn;
struct gpio_config wpd_ht9_d0;
struct gpio_config wpd_ht9_d1;
struct gpio_config wpd_ht9_d2;
struct gpio_config wpd_ht9_d3;




struct device_node *np = NULL;


extern s32 bsp_disp_lcd_set_bright(u32 disp, u32 bright);
extern s32 bsp_disp_lcd_get_bright(u32 disp);

static int v40_wpd_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int v40_wpd_release(struct inode *inode, struct file *file)
{
 	
    return 0;
}


void wpd_port_set(int arg)
{
	gpio_direction_output(wpd_spkamp_en.gpio, arg);	//spk_amp enable (sun8iw11p1_codec.c same)
	gpio_direction_output(wpd_cam_pwen.gpio, arg);
	gpio_direction_output(wpd_rf_nreset.gpio, arg);
	gpio_direction_output(wpd_video_out_sel.gpio, arg);
	gpio_direction_output(wpd_enc_ledb.gpio, arg);
	gpio_direction_output(wpd_enc_ledr.gpio, arg);
	gpio_direction_output(wpd_led_nemr.gpio, arg);
	gpio_direction_output(wpd_pwm_gg.gpio, arg);
	gpio_direction_output(wpd_enc_leda.gpio, arg);
	gpio_direction_output(wpd_34118_mute.gpio, arg);
}


static long v40_wpd_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	int ret = -1;

	switch(cmd)
	{
		case IOCTL_SPKAMPEN_ONOFF:
			gpio_direction_output(wpd_spkamp_en.gpio, arg);	//spk_amp enable (sun8iw11p1_codec.c same)
			ret = 0;
		break;
		case IOCTL_CAMPWEN_ONOFF:
			gpio_direction_output(wpd_cam_pwen.gpio, arg);
			ret = 0;
		break;
		case IOCTL_RFNRESET_ONOFF:
			gpio_direction_output(wpd_rf_nreset.gpio, arg);
			ret = 0;
		break;
		case IOCTL_VIDEOOUTSEL_ONOFF:
			gpio_direction_output(wpd_video_out_sel.gpio, arg);
			ret = 0;
		break;
		case IOCTL_ENCLEDB_ONOFF:
			gpio_direction_output(wpd_enc_ledb.gpio, arg);
			ret = 0;
		break;
		case IOCTL_ENCLEDR_ONOFF:
			gpio_direction_output(wpd_enc_ledr.gpio, arg);
			ret = 0;
		break;
		case IOCTL_LEDNEMR_ONOFF:
			gpio_direction_output(wpd_led_nemr.gpio, arg);
			ret = 0;
		break;
		case IOCTL_PWMGG_ONOFF:
			gpio_direction_output(wpd_pwm_gg.gpio, arg);
			ret = 0;
		break;
		case IOCTL_ENCLEDA_ONOFF:
			gpio_direction_output(wpd_enc_leda.gpio, arg);
			ret = 0;
		break;
		case IOCTL_34118MUTE_ONOFF:
			gpio_direction_output(wpd_34118_mute.gpio, arg);
			ret = 0;
		break;

		case IOCTL_ENCA_GET:
			ret = __gpio_get_value(wpd_enc_a.gpio);
		break;
		case IOCTL_ENCB_GET:
			ret = __gpio_get_value(wpd_enc_b.gpio);
		break;
		case IOCTL_V40EENC_GET:
			ret = __gpio_get_value(wpd_v40_eenc.gpio);
		break;
		case IOCTL_BTNEMRGA_GET:
			ret = __gpio_get_value(wpd_bt_nemrga.gpio);
		break;
		case IOCTL_DOFFLEROUT_GET:
			ret = __gpio_get_value(wpd_doffler_out.gpio);
		break;

		case IOCTL_ALL_DOWN:
			wpd_port_set(0);
		break;
		case IOCTL_ALL_UP:
			wpd_port_set(1);
		break;
		case IOCTL_ALL_PRINT:
			printk("wpd_spkamp_en = %d\n",__gpio_get_value(wpd_spkamp_en.gpio));
			printk("wpd_cam_pwen = %d\n",__gpio_get_value(wpd_cam_pwen.gpio));
			printk("wpd_rf_nreset = %d\n",__gpio_get_value(wpd_rf_nreset.gpio));
			printk("wpd_video_out_sel = %d\n",__gpio_get_value(wpd_video_out_sel.gpio));
			printk("wpd_enc_ledb = %d\n",__gpio_get_value(wpd_enc_ledb.gpio));
			printk("wpd_enc_ledr = %d\n",__gpio_get_value(wpd_enc_ledr.gpio));
			printk("wpd_led_nemr = %d\n",__gpio_get_value(wpd_led_nemr.gpio));
			printk("wpd_pwm_gg = %d\n",__gpio_get_value(wpd_pwm_gg.gpio));
			printk("wpd_enc_leda = %d\n",__gpio_get_value(wpd_enc_leda.gpio));
			printk("wpd_34118_mute = %d\n",__gpio_get_value(wpd_34118_mute.gpio));

			printk("wpd_enc_a = %d\n",__gpio_get_value(wpd_enc_a.gpio));
			printk("wpd_enc_b = %d\n",__gpio_get_value(wpd_enc_b.gpio));
			printk("wpd_v40_eenc = %d\n",__gpio_get_value(wpd_v40_eenc.gpio));
			printk("wpd_bt_nemrga = %d\n",__gpio_get_value(wpd_bt_nemrga.gpio));
			printk("wpd_doffler_out = %d\n",__gpio_get_value(wpd_doffler_out.gpio));

		break;
	}

	return ret;
}


static struct file_operations v40_wpd_fops = { 
	owner:      THIS_MODULE,
	open:    	v40_wpd_open,
	unlocked_ioctl:		v40_wpd_ioctl,
	release:    v40_wpd_release,
};
static struct miscdevice v40_wpd_dev = {
	minor:		143,
	name:		"v40-wpd",
	fops:		&v40_wpd_fops,
};

int v40_wpd_probe(struct platform_device *pdev)
{	
	int err;

	err = misc_register(&v40_wpd_dev);
	if(err) {
		printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n", 108, err);
	}

	return err;
	
}

static int v40_wpd_suspend(struct platform_device *dev, pm_message_t state)
{
	return 0;
}

static int v40_wpd_resume(struct platform_device *pdev)
{
	return 0;
}

static int v40_wpd_remove(struct platform_device *dev)
{
	misc_deregister(&v40_wpd_dev);

	return 0;
}

static struct platform_device v40_wpd_device = {
	.name           	= "v40-wpd",
	.id             	= -1,//??
};

static struct platform_driver v40_wpd_driver = {
       .probe          = v40_wpd_probe,
       .remove         = v40_wpd_remove,
       .suspend        = v40_wpd_suspend,
       .resume         = v40_wpd_resume,
       .driver		= {
		    .owner	= THIS_MODULE,
		    .name	= "v40-wpd",
	}, 
};


struct gpio_config wpd_gpio_init(char *gpio_name, struct gpio_config wpd_gpio)
{
	wpd_gpio.gpio = of_get_named_gpio_flags(np, gpio_name, 0, (enum of_gpio_flags *)(&(wpd_gpio)));

	if(1 != gpio_is_valid(wpd_gpio.gpio))
		printk("%s - %s gpio is invalid\n", __func__, gpio_name);
	if(0 != gpio_request(wpd_gpio.gpio, NULL))
		printk("%s - %s gpio_request fail\n", __func__, gpio_name);
//	if(0 != gpio_direction_output(wpd_gpio.gpio, 0))
//		printk("%s - %s gpio_output low settting fail\n", __func__, gpio_name);

//	gpio_direction_output(spk_amp.gpio, 0);		//gpio write 0 or 1
//	printk("gpio data default = %d\n",__gpio_get_value(spk_amp.gpio)); //gpio read

	return wpd_gpio;
}

void wpd_port_setting(void)
{
	gpio_direction_output(wpd_spkamp_en.gpio, 1);	//spk_amp enable (sun8iw11p1_codec.c same)
	gpio_direction_output(wpd_cam_pwen.gpio, 0);
	gpio_direction_output(wpd_rf_nreset.gpio, 0);
	gpio_direction_output(wpd_video_out_sel.gpio, 0);
	gpio_direction_output(wpd_enc_ledb.gpio, 0);
	gpio_direction_output(wpd_enc_ledr.gpio, 0);
	gpio_direction_output(wpd_led_nemr.gpio, 0);
	gpio_direction_output(wpd_pwm_gg.gpio, 0);
	gpio_direction_output(wpd_enc_leda.gpio, 0);
	gpio_direction_output(wpd_34118_mute.gpio, 0);
}



int __init v40_wpd_init(void)
{
	int ret;
	np = of_find_node_by_name(NULL,"wpd_gpio");
//	np = of_find_node_by_type(NULL,"wpd_gpio");		

//gpio out

	wpd_spkamp_en = wpd_gpio_init("spkamp_en",wpd_spkamp_en);
	wpd_cam_pwen = wpd_gpio_init("cam_pwen",wpd_cam_pwen);
	wpd_rf_nreset = wpd_gpio_init("rf_nreset",wpd_rf_nreset);
	wpd_video_out_sel = wpd_gpio_init("video_out_sel",wpd_video_out_sel);
	wpd_enc_ledb = wpd_gpio_init("enc_ledb",wpd_enc_ledb);
	wpd_enc_ledr = wpd_gpio_init("enc_ledr",wpd_enc_ledr);
	wpd_led_nemr = wpd_gpio_init("led_nemr",wpd_led_nemr);
	wpd_pwm_gg = wpd_gpio_init("pwm_gg",wpd_pwm_gg);
	wpd_enc_leda = wpd_gpio_init("enc_leda",wpd_enc_leda);
	wpd_34118_mute = wpd_gpio_init("34118_mute",wpd_34118_mute);


	//input
	wpd_enc_a = wpd_gpio_init("enc_a", wpd_enc_a);
	wpd_v40_eenc = wpd_gpio_init("v40_eenc",wpd_v40_eenc);
	wpd_enc_b = wpd_gpio_init("enc_b" , wpd_enc_b);
	wpd_bt_nemrga = wpd_gpio_init("bt_nemrga", wpd_bt_nemrga);
	wpd_doffler_out = wpd_gpio_init("doffler_out",wpd_doffler_out);

	wpd_port_setting();
	
	//gpio port input set
	ret = gpio_direction_input(wpd_enc_a.gpio);
	ret = gpio_direction_input(wpd_enc_b.gpio);
	ret = gpio_direction_input(wpd_bt_nemrga.gpio);
	ret = gpio_direction_input(wpd_doffler_out.gpio);
	ret = gpio_direction_input(wpd_v40_eenc.gpio);


	ret = platform_device_register(&v40_wpd_device);
	if (ret) {
		printk(KERN_ERR "v40 wpd platform device register failed!\n");
		return -1;
	}

	ret = platform_driver_register(&v40_wpd_driver);
	if( ret != 0) {
		printk(KERN_ERR "v40 wpd platform device register failed\n");
		return -1;
	}

	printk("v40 wpd Driver , (c) 2017 dadamMicro INC Martin.Kim\n");


	return 0;
}

void __exit v40_wpd_exit(void)
{
	platform_driver_unregister(&v40_wpd_driver);
	platform_device_unregister(&v40_wpd_device);
	
	printk("v40 Wallpad wpd module exit\n");
}

module_init(v40_wpd_init);
module_exit(v40_wpd_exit);

MODULE_AUTHOR("MyoungJoon <ohara@dadammicro.com");
MODULE_DESCRIPTION("v40 HDC IO contrl Device Driver");
MODULE_LICENSE("GPL");
