#include "fm1288.h"

#define FM_VOLUME_CONTROL
#ifdef FM_VOLUME_CONTROL
#include <linux/miscdevice.h>

#define IOCTL_FM1288_MAGIC    'A'  
typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_fm1288_info; 

//#define IOCTL_FM1288_RD			_IOR(IOCTL_FM1288_MAGIC, 0 , ioctl_fm1288_info )  
//#define IOCTL_FM1288_WR			_IOW(IOCTL_FM1288_MAGIC, 1 , ioctl_fm1288_info )  
#define IOCTL_FM1288_SPKVOLUME		(0xA1) 
#define	IOCTL_FM1288_MICVOLUME		(0xA2)
#define IOCTL_FM1288_SPKVOLUME_APP      (0xA3)
#define IOCTL_FM1288_MICVOLUME_APP      (0xA4)
#define IOCTL_FM1288_SETREG_APP      	(0xA5)
#define IOCTL_FM1288_SET_D_REG_APP      (0xA6)
#endif

struct i2c_client * fm1288_client;
script_item_u fm1288_rst_gpio_val;
script_item_value_type_e fm1288_rst_gpio_type;

static int fm1288_reset(int value);

static struct i2c_board_info fm1288_i2c_board_info[] = {
	{I2C_BOARD_INFO(FM1288_NAME, 0x60), }
};

static const struct i2c_device_id fm1288_ids[] = {
	{FM1288_NAME, 0},
	{/*end of list*/}
};
MODULE_DEVICE_TABLE(i2c, fm1288_ids);
//init
static void fm1288_cfg_init(void)
{
	struct i2c_adapter * fm1288_i2c_adap;

	fm1288_i2c_adap = i2c_get_adapter(FM1288_I2C_NUM);
	if(NULL == fm1288_i2c_adap){
		pr_err("####Get fm1288 adapter failed!!!!####\n");
		return;
	} else {
		pr_err("####Get fm1288 adapter success!!!!####\n");
	}
	fm1288_client = i2c_new_device(fm1288_i2c_adap, fm1288_i2c_board_info);
	if(NULL == fm1288_client){
		pr_err("####Get fm1288 client failed!!!!####\n");
		return;
	} else {
		pr_err("####Get fm1288 client success!!!!####\n");
	}
	i2c_put_adapter(fm1288_i2c_adap);
}

int fm1288_i2c_read(struct i2c_client *client, u8 *buf, int len)
{
	struct i2c_msg msgs[2];
	int ret = -1;
	int retries = 0;

	msgs[0].flags = !I2C_M_RD;
	msgs[0].addr  = client->addr;
	msgs[0].len   = FM1288_ADDR_LENGTH;
	msgs[0].buf   = &buf[0];

	msgs[1].flags = I2C_M_RD;
	msgs[1].addr  = client->addr;
	msgs[1].len   = len - FM1288_ADDR_LENGTH;
	msgs[1].buf   = &buf[FM1288_ADDR_LENGTH];

	while(retries < 2) {
		ret = i2c_transfer(client->adapter, msgs, 2);
		if(ret == 2) 
			break;
		retries++;
	}
#ifdef FM1288_DEBUG
	pr_err("##%s ret = %d##\n", __func__, ret);
	pr_err("##i2c addr  = %d##\n", client->addr);
	pr_err("##buf = %u##\n", buf[0]);
#endif

	if(retries >= 2) {
		//pr_err("%s:I2C retry timeout, reset chip.", __func__);
		pr_err("%s:I2C retry timeout, reset chip.", __func__);
	}
	return ret;
}

/*******************************************************	
Function:
write data to the i2c slave device.

Input:
client:	i2c device.
buf[0]:operate address.
buf[1]~buf[len]:write data buffer.
len:operate length.

Output:
numbers of i2c_msgs to transfer.
 *********************************************************/
int fm1288_i2c_write(struct i2c_client *client,u8 *buf,int len)
{
	struct i2c_msg msg;
	int ret = -1;
	int retries = 0;

	msg.flags = !I2C_M_RD;
	msg.addr  = client->addr;
	msg.len   = len;
	msg.buf   = buf;

	while(retries < 2) {
		ret = i2c_transfer(client->adapter, &msg, 1);
		if (ret == 1) 
			break;
		retries++;
	}
#ifdef FM1288_DEBUG
	pr_err("##%s ret = %d##\n", __func__, ret);
	pr_err("##i2c addr  = %d##\n", client->addr);
	pr_err("##buf = %u##\n", buf[0]);
#endif

	if(retries >= 2) {
		//pr_err("%s:I2C retry timeout, reset chip.", __func__);
		pr_err("%s:I2C retry timeout, reset chip.", __func__);
	}
	return ret;
}

#if defined(FM_VOLUME_CONTROL) /* kojohn */
void fm1288_set_d_register(u8* reg, unsigned char len)
{
	fm1288_i2c_write(fm1288_client, reg,len);
}

void fm1288_set_register(u8 reg, unsigned char val)
{
	unsigned char tmp_tmp[14] = {
    	0xFC, 0xF3, 0x3B, 0x23, 0x0D, 0x01, 0x00,
	0xFC, 0xF3, 0x3B, 0x22, 0xFB, 0x00, 0x00
	};

	tmp_tmp[4]=reg;
	tmp_tmp[5]=val;
	
	fm1288_i2c_write(fm1288_client, tmp_tmp, sizeof(_mode1));
}

void fm1288_spk_set_volume(unsigned char val)
{
	if(val > 127)
		val = 127;
	pr_err("%s : val=%d.\n", __func__,val);
	_mode1[5] = val;
	fm1288_i2c_write(fm1288_client, _mode1, sizeof(_mode1));
}

void fm1288_mic_set_volume(unsigned char val)
{
	if(val > 127)
		val = 127;
	pr_err("%s : val=%d.\n", __func__,val);
	_mode2[5] = val;
	fm1288_i2c_write(fm1288_client, _mode2, sizeof(_mode2));
}

static long fm1288_ioctl_proc(struct file * file, unsigned int cmd, unsigned long arg)
{
	int ret;
	ioctl_fm1288_info ctrl_info;  

	pr_err("####%s(cmd=%d)####\n", __func__, cmd);
	switch(cmd)
	{
		case IOCTL_FM1288_SPKVOLUME:			
			ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_fm1288_info));
			fm1288_spk_set_volume((unsigned char)ctrl_info.data);
			//if(ctrl_info.addr == 0x9999)
			//	ctrl_info.data = spk_out_flag;
			//else if(ctrl_info.addr == 0x9998)
			//	printk("cx20709 RD addr = 0x%04lx data=0x%04lx\n", ctrl_info.addr, ctrl_info.data);
			//else
			//	ctrl_info.data = a20_ac97_read(g_cx2070x_codec, ctrl_info.addr);
			//ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_fm1288_info));    
			break;
		case IOCTL_FM1288_MICVOLUME:
			ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_fm1288_info));
			fm1288_mic_set_volume((unsigned char)ctrl_info.data);
			//if(ctrl_info.addr == 0x9999)
			//	spk_on_flag = ctrl_info.data;
			//else if(ctrl_info.addr == 0x9998)
			//	printk("cx20709 WD addr = 0x%04lx data=0x%04lx\n", ctrl_info.addr, ctrl_info.data);
			//else
			//	a20_ac97_write(g_cx2070x_codec, (unsigned int)ctrl_info.addr, (unsigned int)ctrl_info.data);	// Analogue Audio Outputs
			break;
		case IOCTL_FM1288_SPKVOLUME_APP:
	              pr_err("# fm1288_spk_set_volume  %d\n",  (unsigned char)arg);
	              fm1288_spk_set_volume((unsigned char)arg);
	              break;
		case IOCTL_FM1288_MICVOLUME_APP:
		        pr_err("# fm1288_mic_set_volume  %d\n",  (unsigned char)arg);
		         fm1288_mic_set_volume((unsigned char)arg);
		        break;
		case IOCTL_FM1288_SETREG_APP:
			ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_fm1288_info));
			fm1288_set_register((unsigned char)ctrl_info.addr, (unsigned char)ctrl_info.data);
			 break;
		case IOCTL_FM1288_SET_D_REG_APP:
			 ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_fm1288_info));
			fm1288_set_d_register((unsigned char)ctrl_info.addr, (unsigned char)ctrl_info.data);
			 break;
	}
	return 0;
}


static int fm1288_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int fm1288_release(struct inode *inode, struct file *file)
{ 	
    return 0;
}

static ssize_t fm1288_read(struct file *file, char __user *buf, size_t size, loff_t *opps)
{	
	return 0;
}


static ssize_t fm1288_write(struct file *file, char __user *buf, size_t size, loff_t *opps)
{	
	return 0;
}


static int fm1288_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	int ret;
    ioctl_fm1288_info ctrl_info;  

	pr_err("####%s(cmd=%d)####\n", __func__, cmd);
    switch(cmd)
    {
    	case IOCTL_FM1288_SPKVOLUME:    		
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_fm1288_info));
    		fm1288_spk_set_volume((unsigned char)ctrl_info.data);
			//if(ctrl_info.addr == 0x9999)
			//	ctrl_info.data = spk_out_flag;
			//else if(ctrl_info.addr == 0x9998)
			//	printk("cx20709 RD addr = 0x%04lx data=0x%04lx\n", ctrl_info.addr, ctrl_info.data);
			//else
			//	ctrl_info.data = a20_ac97_read(g_cx2070x_codec, ctrl_info.addr);
        	//ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_fm1288_info));    
    		break;
    	case IOCTL_FM1288_MICVOLUME:
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_fm1288_info));
    		fm1288_mic_set_volume((unsigned char)ctrl_info.data);
			//if(ctrl_info.addr == 0x9999)
			//	spk_on_flag = ctrl_info.data;
			//else if(ctrl_info.addr == 0x9998)
			//	printk("cx20709 WD addr = 0x%04lx data=0x%04lx\n", ctrl_info.addr, ctrl_info.data);
			//else
			//	a20_ac97_write(g_cx2070x_codec, (unsigned int)ctrl_info.addr, (unsigned int)ctrl_info.data);	// Analogue Audio Outputs
			break;
	}
    return 0;
}

struct file_operations fm1288_fops_proc={
	.owner		= THIS_MODULE,
	.compat_ioctl = fm1288_ioctl_proc,
	.read		= fm1288_read,
	.write		= fm1288_write,
	.open 		= fm1288_open,
	.release 	= fm1288_release,
};

static struct file_operations fm1288_fops = {
	owner:      THIS_MODULE,
	open:    	fm1288_open,
	unlocked_ioctl:		fm1288_ioctl,
	release:    fm1288_release,
};


static struct miscdevice fm1288_dev = {
	minor:		MISC_DYNAMIC_MINOR,
	name:		"fm1288_ctl",
	fops:		&fm1288_fops,
};

#endif

/**************************************/
/*           fm1288 resource          */
/**************************************/
static int get_fm1288_resource(void)
{
	int fm1288_used;
	script_item_u fm1288_used_val;
	script_item_value_type_e fm1288_used_type;

	pr_err("####%s####\n", __func__);

	//get fm1288_used
	fm1288_used_type = script_get_item("fm1288_para", "fm1288_used", &fm1288_used_val);
	if (SCIRPT_ITEM_VALUE_TYPE_INT != fm1288_used_type) {
#ifdef FM1288_DEBUG
		pr_err("####Get fm1288 script failed####\n");
#endif
		return FM1288_FALSE;
	}

	fm1288_used = fm1288_used_val.val;
#ifdef FM1288_DEBUG
	pr_err("####fm1288_used = %d####\n", fm1288_used);
#endif

	//get fm1288 reset gpio
	fm1288_rst_gpio_type = script_get_item("fm1288_para", "fm1288_rst_gpio", &fm1288_rst_gpio_val);
	if (SCIRPT_ITEM_VALUE_TYPE_PIO != fm1288_rst_gpio_type) {
#ifdef FM1288_DEBUG
		pr_err("####Get fm1288 script failed####\n");
#endif
		return FM1288_FALSE;
	} else {
		pr_err("####Get fm1288 script success####\n");
	}

	if(0 != gpio_request(fm1288_rst_gpio_val.gpio.gpio, NULL)) {
		pr_err("####Get fm1288 reset gpio failed####\n");
		return FM1288_FALSE;
	} else {
		pr_err("####Get fm1288 reset gpio success####\n");
	}
	if (0 != gpio_direction_output(fm1288_rst_gpio_val.gpio.gpio, 1)) {
		pr_err("####Set fm1288 reset gpio failed####\n");
		return FM1288_FALSE;
	} else {
		pr_err("####Set fm1288 reset gpio success####\n");
	}

	return fm1288_used;
}

/*************************************/
/*           fm1288 reset            */
/*************************************/
static int fm1288_reset(int value){
	__gpio_set_value(fm1288_rst_gpio_val.gpio.gpio, value);
	return FM1288_TRUE;
}

/*************************************/
/*       FM1288 control function     */
/**************************************/
static int fm1288_ctl(void)
{
	pr_err("####%s()size=%d####\n", __func__, sizeof(_mode0));
	fm1288_i2c_write(fm1288_client, _mode0, sizeof(_mode0)); 

	return FM1288_TRUE;
}

static int __init fm1288_init(void)
{
#if defined(FM_VOLUME_CONTROL) /* kojohn */
	int ret;
	struct proc_dir_entry * file;
#endif

	pr_err("#####%s()%d -> default_V1.2 #####\n", __func__, __LINE__);

	fm1288_cfg_init();

	if(USE_RESOURCE == get_fm1288_resource()){
		fm1288_reset(0);//low
		mdelay(10);
		fm1288_reset(1);//high
		mdelay(10);
		fm1288_ctl();
	}
#if defined(FM_VOLUME_CONTROL) /* kojohn */
	ret = misc_register(&fm1288_dev);
	if (ret) {
		pr_err ("cannot register miscdev (%d)\n", ret);
	}
	file = proc_create("fm1288_ctl", 0666, NULL, &fm1288_fops_proc);
	if(!file){
		pr_err ("cannot create proc (%d)\n", file);
		return FM1288_FALSE;
	}
#endif
	pr_err("####%s()%d#### success\n", __func__, __LINE__);
	return FM1288_TRUE;
}
static void __exit fm1288_exit(void)
{
	pr_err("####%s()%d - ####\n", __func__, __LINE__);
	i2c_unregister_device(fm1288_client);
}

module_init(fm1288_init);
module_exit(fm1288_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("WILLIAMXU");
MODULE_DESCRIPTION("FM1288 DRIVER");

