
#include "fm1288.h"

#define FM_VOLUME_CONTROL
#ifdef FM_VOLUME_CONTROL
#include <linux/miscdevice.h>
#include <linux/of_net.h>
#include <linux/of_gpio.h>
#include <linux/io.h>
#include <linux/sys_config.h>


#define IOCTL_FM1288_MAGIC    'A'  
typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_fm1288_info; 

//#define IOCTL_FM1288_RD			_IOR(IOCTL_FM1288_MAGIC, 0 , ioctl_fm1288_info )  
//#define IOCTL_FM1288_WR			_IOW(IOCTL_FM1288_MAGIC, 1 , ioctl_fm1288_info )  
#define IOCTL_FM1288_SPKVOLUME		(0xA1) 
#define	IOCTL_FM1288_MICVOLUME		(0xA2)
#define IOCTL_FM1288_SPKVOLUME_APP      (0xA3)
#define IOCTL_FM1288_MICVOLUME_APP      (0xA4)
#define IOCTL_FM1288_SETREG_APP      	(0xA5)
#define IOCTL_FM1288_SET_D_REG_APP      (0xA6)
#endif

struct i2c_client * fm1288_client;
struct gpio_config fm1288_rst_gpio;
struct device_node *np = NULL;

static int fm1288_reset(int value);

//extern void watchdogreset(void);

static struct i2c_board_info fm1288_i2c_board_info[] = {
	{I2C_BOARD_INFO(FM1288_NAME, 0x60), }
};

static const struct i2c_device_id fm1288_ids[] = {
	{FM1288_NAME, 0},
	{/*end of list*/}
};
MODULE_DEVICE_TABLE(i2c, fm1288_ids);
//init
static void fm1288_cfg_init(void)
{
	struct i2c_adapter * fm1288_i2c_adap;

	fm1288_i2c_adap = i2c_get_adapter(FM1288_I2C_NUM);
	if(NULL == fm1288_i2c_adap){
		pr_err("####Get fm1288 adapter failed!!!!####\n");
		return;
	} else {
		pr_err("####Get fm1288 adapter success!!!!####\n");
	}
	fm1288_client = i2c_new_device(fm1288_i2c_adap, fm1288_i2c_board_info);
	if(NULL == fm1288_client){
		pr_err("####Get fm1288 client failed!!!!####\n");
		return;
	} else {
		pr_err("####Get fm1288 client success!!!!####\n");
	}
	i2c_put_adapter(fm1288_i2c_adap);
}

int fm1288_i2c_read(struct i2c_client *client, u8 *wbuf, u8 *rbuf)
{
	struct i2c_msg msgs[2];
	int ret = -1;
	int retries = 0;

	msgs[0].flags = !I2C_M_RD;
	msgs[0].addr  = client->addr;
	msgs[0].len   = sizeof(wbuf);
	msgs[0].buf   = wbuf;

	msgs[1].flags = I2C_M_RD;
	msgs[1].addr  = client->addr;
	msgs[1].len   = 1;
	msgs[1].buf   = &rbuf[0];


	while(retries < 2) {
		ret = i2c_transfer(client->adapter, msgs, 2);
		if(ret == 2) 
			break;
		retries++;
	}
#ifdef FM1288_DEBUG
	pr_err("##%s ret = %d##\n", __func__, ret);
	pr_err("##i2c addr  = %d##\n", client->addr);
//	pr_err("##buf = %u##\n", buf[0]);
#endif

	if(retries >= 2) {
		//pr_err("%s:I2C retry timeout, reset chip.", __func__);
		pr_err("%s:I2C retry timeout, reset chip.", __func__);
	}
	return ret;
}

/*******************************************************	
Function:
write data to the i2c slave device.

Input:
client:	i2c device.
buf[0]:operate address.
buf[1]~buf[len]:write data buffer.
len:operate length.

Output:
numbers of i2c_msgs to transfer.
 *********************************************************/
int fm1288_i2c_write(struct i2c_client *client,u8 *buf,int len)
{
	struct i2c_msg msg;
	int ret = -1;
	int retries = 0;

	msg.flags = !I2C_M_RD;
	msg.addr  = client->addr;
	msg.len   = len;
	msg.buf   = buf;

	while(retries < 2) {
		ret = i2c_transfer(client->adapter, &msg, 1);
		if (ret == 1) 
			break;
		retries++;
	}
#ifdef FM1288_DEBUG
	pr_err("##%s ret = %d##\n", __func__, ret);
	pr_err("##i2c addr  = %d##\n", client->addr);
	pr_err("##buf = %u##\n", buf[0]);
#endif

	if(retries >= 2) {
		//pr_err("%s:I2C retry timeout, reset chip.", __func__);
		pr_err("%s:I2C retry timeout, reset chip.", __func__);
	}
	return ret;
}

#if defined(FM_VOLUME_CONTROL) /* kojohn */
void fm1288_set_d_register(u8* reg, unsigned char len)
{
	fm1288_i2c_write(fm1288_client, reg,len);
}

void fm1288_set_register(u8 reg, unsigned char val)
{
	unsigned char tmp_tmp[14] = {
    	0xFC, 0xF3, 0x3B, 0x23, 0x0D, 0x01, 0x00,
	0xFC, 0xF3, 0x3B, 0x22, 0xFB, 0x00, 0x00
	};

	tmp_tmp[4]=reg;
	tmp_tmp[5]=val;
	
	fm1288_i2c_write(fm1288_client, tmp_tmp, sizeof(_mode1));
}

void fm1288_spk_set_volume(unsigned char val)
{
	if(val > 127)
		val = 127;
	pr_err("%s : val=%d.\n", __func__,val);
	_mode1[5] = val;
	fm1288_i2c_write(fm1288_client, _mode1, sizeof(_mode1));
}

void fm1288_mic_set_volume(unsigned char val)
{
	if(val > 127)
		val = 127;
	pr_err("%s : val=%d.\n", __func__,val);
	_mode2[5] = val;
	fm1288_i2c_write(fm1288_client, _mode2, sizeof(_mode2));
}

static long fm1288_ioctl_proc(struct file * file, unsigned int cmd, unsigned long arg)
{
	int ret;
	ioctl_fm1288_info ctrl_info;  

	pr_err("####%s(cmd=%d)####\n", __func__, cmd);
	switch(cmd)
	{
		case IOCTL_FM1288_SPKVOLUME:			
			ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_fm1288_info));
			fm1288_spk_set_volume((unsigned char)ctrl_info.data);
			//if(ctrl_info.addr == 0x9999)
			//	ctrl_info.data = spk_out_flag;
			//else if(ctrl_info.addr == 0x9998)
			//	printk("cx20709 RD addr = 0x%04lx data=0x%04lx\n", ctrl_info.addr, ctrl_info.data);
			//else
			//	ctrl_info.data = a20_ac97_read(g_cx2070x_codec, ctrl_info.addr);
			//ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_fm1288_info));    
			break;
		case IOCTL_FM1288_MICVOLUME:
			ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_fm1288_info));
			fm1288_mic_set_volume((unsigned char)ctrl_info.data);
			//if(ctrl_info.addr == 0x9999)
			//	spk_on_flag = ctrl_info.data;
			//else if(ctrl_info.addr == 0x9998)
			//	printk("cx20709 WD addr = 0x%04lx data=0x%04lx\n", ctrl_info.addr, ctrl_info.data);
			//else
			//	a20_ac97_write(g_cx2070x_codec, (unsigned int)ctrl_info.addr, (unsigned int)ctrl_info.data);	// Analogue Audio Outputs
			break;
		case IOCTL_FM1288_SPKVOLUME_APP:
	              pr_err("# fm1288_spk_set_volume  %d\n",  (unsigned char)arg);
	              fm1288_spk_set_volume((unsigned char)arg);
	              break;
		case IOCTL_FM1288_MICVOLUME_APP:
		        pr_err("# fm1288_mic_set_volume  %d\n",  (unsigned char)arg);
		         fm1288_mic_set_volume((unsigned char)arg);
		        break;
		case IOCTL_FM1288_SETREG_APP:
			ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_fm1288_info));
			fm1288_set_register((unsigned char)ctrl_info.addr, (unsigned char)ctrl_info.data);
			 break;
		case IOCTL_FM1288_SET_D_REG_APP:
			 ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_fm1288_info));
			fm1288_set_d_register((unsigned char)ctrl_info.addr, (unsigned char)ctrl_info.data);
			 break;
	}
	return 0;
}


static int fm1288_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int fm1288_release(struct inode *inode, struct file *file)
{ 	
    return 0;
}

static ssize_t fm1288_read(struct file *file, char __user *buf, size_t size, loff_t *opps)
{	
	return 0;
}


static ssize_t fm1288_write(struct file *file, char __user *buf, size_t size, loff_t *opps)
{	
	return 0;
}


static int fm1288_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	int ret;
    ioctl_fm1288_info ctrl_info;  

	pr_err("####%s(cmd=%d)####\n", __func__, cmd);
    switch(cmd)
    {
    	case IOCTL_FM1288_SPKVOLUME:    		
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_fm1288_info));
    		fm1288_spk_set_volume((unsigned char)ctrl_info.data);
			//if(ctrl_info.addr == 0x9999)
			//	ctrl_info.data = spk_out_flag;
			//else if(ctrl_info.addr == 0x9998)
			//	printk("cx20709 RD addr = 0x%04lx data=0x%04lx\n", ctrl_info.addr, ctrl_info.data);
			//else
			//	ctrl_info.data = a20_ac97_read(g_cx2070x_codec, ctrl_info.addr);
        	//ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_fm1288_info));    
    		break;
    	case IOCTL_FM1288_MICVOLUME:
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_fm1288_info));
    		fm1288_mic_set_volume((unsigned char)ctrl_info.data);
			//if(ctrl_info.addr == 0x9999)
			//	spk_on_flag = ctrl_info.data;
			//else if(ctrl_info.addr == 0x9998)
			//	printk("cx20709 WD addr = 0x%04lx data=0x%04lx\n", ctrl_info.addr, ctrl_info.data);
			//else
			//	a20_ac97_write(g_cx2070x_codec, (unsigned int)ctrl_info.addr, (unsigned int)ctrl_info.data);	// Analogue Audio Outputs
			break;
	}
    return 0;
}

struct file_operations fm1288_fops_proc={
	.owner		= THIS_MODULE,
	.compat_ioctl = fm1288_ioctl_proc,
	.read		= fm1288_read,
	.write		= fm1288_write,
	.open 		= fm1288_open,
	.release 	= fm1288_release,
};

static struct file_operations fm1288_fops = {
	owner:      THIS_MODULE,
	open:    	fm1288_open,
	unlocked_ioctl:		fm1288_ioctl,
	release:    fm1288_release,
};


static struct miscdevice fm1288_dev = {
	minor:		MISC_DYNAMIC_MINOR,
	name:		"fm1288_ctl",
	fops:		&fm1288_fops,
};

#endif

/**************************************/
/*           fm1288 resource          */
/**************************************/
static int get_fm1288_resource(void)
{
	int fm1288_used;
	int ret = -1;
	np = of_find_node_by_name(NULL,"fm1288_para");
	if (!np) {
		 printk("ERROR! get fm1288_para failed, func:%s, line:%d\n",__FUNCTION__, __LINE__);
		return FM1288_FALSE;
	}
	else
		printk("####Success get fm1288_para\n");

	ret = of_property_read_u32(np, "fm1288_used", &fm1288_used);
	if (ret) {
		 pr_err("get fm1288_used is fail, %d\n", ret);
		return FM1288_FALSE;
	}
	else
		printk("####Success get fm1288_used\n");


	fm1288_rst_gpio.gpio = of_get_named_gpio_flags(np, "fm1288_rst_gpio", 0, (enum of_gpio_flags *)(&(fm1288_rst_gpio)));
	if (!gpio_is_valid(fm1288_rst_gpio.gpio))
			printk("%s: fm1288_rst_gpio is invalid. \n",__func__ );

	if(0 != gpio_request(fm1288_rst_gpio.gpio, NULL)) {
		printk("####Get fm1288 reset gpio failed####\n");
		return FM1288_FALSE;
	} else {
		printk("####Get fm1288 reset gpio success####\n");
	}
	if (0 != gpio_direction_output(fm1288_rst_gpio.gpio, 1)) {
		printk("####Set fm1288 reset gpio failed####\n");
		return FM1288_FALSE;
	} else {
		printk("####Set fm1288 reset gpio success####\n");
	}

	return fm1288_used;
}

/*************************************/
/*           fm1288 reset            */
/*************************************/
static int fm1288_reset(int value){
	gpio_direction_output(fm1288_rst_gpio.gpio, value);
	printk("fm1288_rst_gpio.gpio read value default = %d\n",__gpio_get_value(fm1288_rst_gpio.gpio));
	return FM1288_TRUE;
}

/*************************************/
/*       FM1288 control function     */
/**************************************/
static int fm1288_ctl(void)
{
	pr_err("####%s()size=%d####\n", __func__, sizeof(_mode0));
	fm1288_i2c_write(fm1288_client, _mode0, sizeof(_mode0)); 

	return FM1288_TRUE;
}

/**
 * rstrtok - Split a string into tokens
 * @s: The string to be searched
 * @ct: The characters to search for
 * * WARNING: strtok is deprecated, use strsep instead. However strsep is not compatible with old architecture.
 */
static char * __rstrtok;
static char * rstrtok(char * s,const char * ct)
{
	char *sbegin, *send;

	sbegin  = s ? s : __rstrtok;
	if (!sbegin)
	{
		return NULL;
	}

	sbegin += strspn(sbegin,ct);
	if (*sbegin == '\0')
	{
		__rstrtok = NULL;
		return( NULL );
	}

	send = strpbrk( sbegin, ct);
	if (send && *send != '\0')
		*send++ = '\0';

	__rstrtok = send;

	return (sbegin);
}

static int parseargs(char *cmdline, char **argv)
{
	static char *delim = ",. \n\r\t\v";
	char *tok;
	int argc = 0;

	argv[argc] = NULL;
                   
	for (tok = rstrtok(cmdline, delim); tok; tok = rstrtok(NULL, delim)) 
	{
		argv[argc++] = tok;
	}

	return argc;
}

#if 0
static int fm1288_proc_cmd( struct file *file, const char __user *buffer, unsigned long count, void *data)
{
	char *argv[256];
	char cbuffer[2048];
	int i, argc;
	
	if(count > 256 ) count = 256;
	copy_from_user(cbuffer, buffer, count);
	argc = parseargs(cbuffer, argv);
	for(i = 0; i < argc; i++) printk("fm1288 set : %d : %02x\n", cbuffer[i]); 
#if 0
	if(argc == 7)
	{		
		for(i =0; i < argc; i++)
		{			
			ts_data->calib.point[i] = simple_strtol(argv[i], NULL, 10);
			//printk("Touch Parameter %d:%ld\n", i, simple_strtol(argv[i], NULL, 10));
		}
		ts_data->calib.use = 1;
	}
#endif	
	return 0;		
}

static void fm1288_register_proc( void )
{
	struct proc_dir_entry *procdir;

	procdir = proc_create_data("fm1288_reg",  S_IFREG | S_IRWXU, NULL, NULL, NULL);

	//procdir = create_proc_entry("fm1288_reg", S_IFREG | S_IRWXU, 0);
	//procdir->read_proc  =;
	procdir->proc_fops = fm1288_proc_cmd;
}
#endif

static ssize_t fm1288_read_proc(struct file *file, char __user *page, size_t size, loff_t *ppos)
{
    char *ptr = page;
    
    if (*ppos)  // CMD call again
    {
        return 0;
    }
    
    ptr += sprintf(ptr, "fm1288 reg: %s\n", "00");
    
    *ppos += ptr - page;
    return (ptr - page);
    /*ptr[0] = current_mode;
    return 1;*/
}

static char *argv[10240];
static char cbuffer[10240];
static char regs[1024];
static int bcount = 0;

static ssize_t fm1288_patch_write_proc(struct file *filp, const char __user *buffer, size_t count, loff_t *off)
{
    s32 ret = 0;
    
	int i, argc;

    if (copy_from_user(&cbuffer[bcount], buffer, count))
    {
        pr_err("copy from user fail\n");
        return -EFAULT;
    }
    
    bcount += count;

	//printk("fm1288 bcount : %d\n", bcount);

	if(bcount > 4096)
	{ 
		argc = parseargs(cbuffer, argv);
		for(i = 0; i < argc; i++) regs[i] = (char)simple_strtol(argv[i], NULL, 16);

		fm1288_reset(0);//low
		msleep(10);
		fm1288_reset(1);//high

		if(fm1288_i2c_write(fm1288_client, regs, argc) < 0) 
		{
			printk("fm1288 patch error! \n");
//			watchdogreset();	
		}					
		else printk("fm1288 patch OK! \n");
		
		memset(regs, 0, 1024);
		memset(cbuffer, 0, 10240);
		bcount = 0;
	}
	    
    return count;
}

//SPK GAIN echo 230d 0000 > /proc/fm1288_mw  - 0000 ~ 0a00
//MIC GAIN echo 230c 0000 > /proc/fm1288_mw  - 0000 ~ 0a00 


static ssize_t fm1288_mw_write_proc(struct file *filp, const char __user *buffer, size_t count, loff_t *off)
{
    s32 ret = 0;
    
	int i, argc, addr, data;
	char cbuffer[100];
	unsigned char mem_set[] = {
    	0xFC, 0xF3, 0x3B, 0x23, 0x0D, 0x01, 0x00,
		0xFC, 0xF3, 0x3B, 0x22, 0xFB, 0x00, 0x00
	};	


	memset(cbuffer, 0, sizeof(cbuffer));
	if(count > sizeof(cbuffer)) return count;
		
    if (copy_from_user(cbuffer, buffer, count))
    {
        pr_err("copy from user fail\n");
        return -EFAULT;
    }

	argc = parseargs(cbuffer, argv);
    if(argc == 2)
    {
		addr = simple_strtol(argv[0], NULL, 16);
		data = simple_strtol(argv[1], NULL, 16);
	
		mem_set[3]=(addr & 0xff00) >> 8;
		mem_set[4]=addr & 0xff;
		mem_set[5]=(data & 0xff00) >> 8;
		mem_set[6]=data & 0xff;

		fm1288_i2c_write(fm1288_client, mem_set, sizeof(mem_set));
	
		printk("fm1288 mem [%04x] <- [%04x]\n", addr, data);
	}
	else
	{
		printk("echo \"0xaddr 0xdata\" > /proc/fm1288_mw\n");
	}
	
    return count;
}

static ssize_t fm1288_mr_write_proc(struct file *filp, const char __user *buffer, size_t count, loff_t *off)
{
    s32 ret = 0;
    
	int i, argc, addr, data;
	char cbuffer[100], low, high;
	unsigned char addr_set[] = {
    	0xFC, 0xF3, 0x37, 0x00, 0x00,
	};	
	unsigned char low_read[] = {
    	0xFC, 0xF3, 0x60, 0x25
	};	
	unsigned char high_read[] = {
    	0xFC, 0xF3, 0x60, 0x26
	};	
	unsigned char i2c_end[] = {
		0xFC, 0xF3, 0x3B, 0x22, 0xFB, 0x00, 0x00
	};	

	memset(cbuffer, 0, sizeof(cbuffer));
	if(count > sizeof(cbuffer)) return count;

    if (copy_from_user(cbuffer, buffer, count))
    {
        pr_err("copy from user fail\n");
        return -EFAULT;
    }

	argc = parseargs(cbuffer, argv);
    if(argc == 1)
    {
		addr = simple_strtol(argv[0], NULL, 16);
	
		addr_set[3]=(addr & 0xff00) >> 8;
		addr_set[4]=addr & 0xff;
		
		fm1288_i2c_write(fm1288_client, addr_set, sizeof(addr_set));
		fm1288_i2c_read(fm1288_client, low_read,  &low);
		fm1288_i2c_read(fm1288_client, high_read, &high);
		fm1288_i2c_write(fm1288_client, i2c_end, sizeof(i2c_end));
	
		printk("fm1288 mem [%04x] -> [%04x]\n", addr, (high<<8)|low);
	}
	else
	{
		printk("echo \"0xaddr\" > /proc/fm1288_mr\n");
	}
			    
    return count;
}

static struct proc_dir_entry *fm1288_patch_proc = NULL;
static const struct file_operations fm1288_patch_proc_ops = {
    .owner = THIS_MODULE,
//    .read = fm1288_read_proc,
    .write = fm1288_patch_write_proc,
};

static struct proc_dir_entry *fm1288_mr_proc = NULL;
static const struct file_operations fm1288_mr_proc_ops = {
    .owner = THIS_MODULE,
//    .read = fm1288_read_proc,
    .write = fm1288_mr_write_proc,
};

static struct proc_dir_entry *fm1288_mw_proc = NULL;
static const struct file_operations fm1288_mw_proc_ops = {
    .owner = THIS_MODULE,
//    .read = fm1288_read_proc,
    .write = fm1288_mw_write_proc,
};
 
static int __init fm1288_init(void)
{
#if defined(FM_VOLUME_CONTROL) /* kojohn */
	int ret;
	struct proc_dir_entry * file;
#endif

	pr_err("#####%s()%d -> default_V1.2 #####\n", __func__, __LINE__);

    fm1288_patch_proc = proc_create("fm1288_patch", 0222, NULL, &fm1288_patch_proc_ops);
    fm1288_mr_proc = proc_create("fm1288_mr", 0222, NULL, &fm1288_mr_proc_ops);
    fm1288_mw_proc = proc_create("fm1288_mw", 0222, NULL, &fm1288_mw_proc_ops);
    
    if (fm1288_patch_proc == NULL || fm1288_mr_proc == NULL || fm1288_mw_proc == NULL )
    {
        pr_err("create_proc_entry %s failed\n", "fm1288");
    }
    else
    {
        pr_err("create proc entry %s success", "fm1288");
    }

	if(USE_RESOURCE == get_fm1288_resource()){

		fm1288_reset(0);//low
		msleep(10);
		fm1288_reset(1);//high
		fm1288_cfg_init();
		fm1288_ctl();
	}
#if defined(FM_VOLUME_CONTROL) /* kojohn */
	ret = misc_register(&fm1288_dev);
	if (ret) {
		pr_err ("cannot register miscdev (%d)\n", ret);
	}
	file = proc_create("fm1288_ctl", 0666, NULL, &fm1288_fops_proc);
	if(!file){
		pr_err ("cannot create proc (%d)\n", file);
		return FM1288_FALSE;
	}
#endif
	pr_err("####%s()%d#### success\n", __func__, __LINE__);
	return FM1288_TRUE;
}
static void __exit fm1288_exit(void)
{
	pr_err("####%s()%d - ####\n", __func__, __LINE__);
	i2c_unregister_device(fm1288_client);
}

module_init(fm1288_init);
module_exit(fm1288_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("WILLIAMXU");
MODULE_DESCRIPTION("FM1288 DRIVER");

