#include <linux/module.h>
#include <linux/init.h>
#include <linux/pm.h>
#include <linux/fs.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/device.h>
#include <linux/clk.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/regulator/consumer.h>
#include <linux/uaccess.h>
#include <linux/bug.h>
#include <linux/errno.h>
#include <linux/resource.h>
#include <linux/moduleparam.h>

#include <linux/workqueue.h>
#include <mach/sys_config.h>
#include <mach/clock.h>
#include <mach/gpio.h>
#include <linux/gpio.h>


//error number
#define ECFGNOEX            1   //config not exit
#define ECFGERR             2   //config error
#define ECAMNUMERR          3   //camera number error
#define ECLKSRC             4   //clock source error
#define EDETECTFAIL         5   //detect camera fail
#define EPMUPIN             6   //get pmu pin fail
#define ENORMALPIN          7   //get normal pin fail
#define ECFGPIN             8   //config pin fail
#define EI2CADAPTER         9   //get I2C adapter fail

#define CAM_STATE_DOOR	    1
#define CAM_STATE_FRONT	    2
#define CAM_STATE_REC	    3
#define CAM_STATE_NONE	    0
extern void btn_door(void);
extern void btn_emr(void);
extern void btn_call(int arg);
extern void btn_bopen(int arg);
extern void evt_ring(void);
extern void evt_emr(void);
extern void evt_mag0(void);
extern void evt_exit(void);
extern void evt_motout(void);
extern void evt_motin(void);
extern void ac97_write(unsigned int addr, unsigned int value);
extern int ac97_read(unsigned int addr);
extern void ac97_dsp_init(void);
extern void TVD_set_bright(int idx,int luma1,int contrast1);
extern void TVD_set_contrast(int idx,int saturation1,int hue1);

extern void blight_off(void);
extern void blight_on(void);

extern int get_LCD_status(void);
static unsigned short incomingcallledrate = (2000 * HZ) / 1000; //2sec
static unsigned short longkeyrate = (1000 * HZ) / 1000; //1sec
static unsigned short emrledrate = (500 * HZ) / 1000; //500ms
static unsigned short longkey_count = 0;


static int cam_state = 0;
static int analog_cam_stat = 0;
struct delayed_work     door_ready_work;

struct delayed_work     door_delay_work;
struct delayed_work     ring_delay_work;
struct delayed_work     emr_key_delay_work;
struct delayed_work     call_key_delay_work;
struct delayed_work     open_key_delay_work;
struct delayed_work     sensor_emr_delay_work;
struct delayed_work     sensor_mag0_delay_work;
struct delayed_work     sensor_exit_delay_work;
struct delayed_work     sensor_motin_delay_work;
struct delayed_work     sensor_motout_delay_work;
struct delayed_work     door_lock_open_work;
struct delayed_work     door_lock_regis_work;

struct timer_list 	incomingcall_led_timer;
struct timer_list 	emr_long_key_timer;
struct timer_list 	emr_led_timer;

static int emr_led_timer_status = 0;

enum { BTN_LED_ON=0, BTN_LED_OFF };
enum { BTN_LED_TRIGGER_ON=1, BTN_LED_TRIGGER_OFF };
enum { LED_OFF=0, LED_ON };
enum { POWER_OFF=0, POWER_ON };
enum { VIDEO_OUT_DOOR_ON=0, VIDEO_OUT_DOOR_OFF };
enum { AUDIO_MUTE_OFF=0, AUDIO_MUTE_ON };
enum { DOOR_LOCK_REGIS=0, DOOR_LOCK_OPEN };
enum { RESET_LOW=0, RESET_HIGH };
enum { VIDEOINPUT_DOOR=0, VIDEOINPUT_CAMERA };
enum { SENSOR_DISABLE=0, SENSOR_ENABLE };

static int door_detection_time;
static int sensor_emr_detection_time;
static int sensor_mag0_detection_time;
static int sensor_exit_detection_time;
static int sensor_motin_detection_time;
static int sensor_motout_detection_time;

static int door_detection_cnt = 0;
static int sensor_emr_detection_cnt = 0;
static int sensor_mag0_detection_cnt = 0;
static int sensor_exit_detection_cnt = 0;
static int sensor_motin_detection_cnt = 0;
static int sensor_motout_detection_cnt = 0;

static int key_control_onoff;

#define BTNEMR_LEDONOFF(btn_led_onoff)			(__gpio_set_value(wpd_led_nemr.gpio.gpio,btn_led_onoff))
#define BTNCALL_LEDONOFF(btn_led_onoff)			(__gpio_set_value(wpd_led_ncall.gpio.gpio,btn_led_onoff))
#define BTNOPEN_LEDONOFF(btn_led_onoff)			(__gpio_set_value(wpd_led_nopen.gpio.gpio,btn_led_onoff))
#define SUBTOUCH_POWERONOFF(power_onoff)		(__gpio_set_value(wpd_bt_pwr_on.gpio.gpio,power_onoff))
#define VIDEOOUT_DOORONOFF(door_onoff)			(__gpio_set_value(wpd_video_out_select.gpio.gpio,door_onoff))
#define CAMERA_POWERONOFF(power_onoff)			(__gpio_set_value(wpd_cam_pwen.gpio.gpio,power_onoff))
#define SUBPHONE_AUDIO_MUTEONOFF(audio_mute_onoff)	(__gpio_set_value(wpd_34118_mute.gpio.gpio,audio_mute_onoff))
#define DOOR_LOCKONOFF(door_lock_onoff)			(__gpio_set_value(wpd_wddl.gpio.gpio,door_lock_onoff))
#define DOOR_POWERONOFF(power_onoff)			(__gpio_set_value(wpd_door_pwr_en.gpio.gpio,power_onoff))
#define DOOR_DINGDONG(power_onoff)			(__gpio_set_value(wpd_dingdong.gpio.gpio,power_onoff))
#define RFREMOCON_RESET(reset_lowhigh)			(__gpio_set_value(wpd_rf_nreset.gpio.gpio,reset_lowhigh))
#define FRONTHOME_LEDONOFF(led_onoff)			(__gpio_set_value(wpd_led_home.gpio.gpio,led_onoff))
#define FRONTEMR_LEDONOFF(led_onoff)			(__gpio_set_value(wpd_led_emr.gpio.gpio,led_onoff))

#define DOOR_KEY_STATE()				(__gpio_get_value(wpd_ndoor_detect.gpio.gpio))
#define EMR_KEY_STATE()					(__gpio_get_value(wpd_bt_nemr.gpio.gpio))
#define CALL_KEY_STATE()				(__gpio_get_value(wpd_bt_ncall.gpio.gpio))
#define OPEN_KEY_STATE()				(__gpio_get_value(wpd_bt_nopen.gpio.gpio))
#define RING_DETECT_STATE()				(__gpio_get_value(wpd_ring_detect.gpio.gpio))
#define SENSOR_EMR_STATE()				(__gpio_get_value(wpd_emergency.gpio.gpio))
#define SENSOR_MAG0_STATE()				(__gpio_get_value(wpd_magnetic0_mcu.gpio.gpio))
#define SENSOR_EXIT_STATE()				(__gpio_get_value(wpd_exit_mcu.gpio.gpio))
#define SENSOR_MOTOUT_STATE()				(__gpio_get_value(wpd_motion_out.gpio.gpio))
#define SENSOR_MOTIN_STATE()				(__gpio_get_value(wpd_motion_in.gpio.gpio))
#define BTNEMR_LED_STATE()				(__gpio_get_value(wpd_led_nemr.gpio.gpio))
#define DOOR_POWER_STATE()			 	(__gpio_get_value(wpd_door_pwr_en.gpio.gpio))

#define REV_OPT()					(__gpio_get_value(wpd_rev_opt.gpio.gpio))
#define REV_SEL()					(__gpio_get_value(wpd_rev_sel.gpio.gpio))

#define 	TYPE_GPIO_CMD		(0xFF << 8)
#define 	TYPE_VIDEO_CMD		(0xFE << 8)
#define 	TYPE_AUDIO_CMD		(0xFD << 8)
#define 	TYPE_BACKLIGHT_CMD	(0xFC << 8)	
#define 	TYPE_ETHERNET_CMD	(0xFB << 8)
#define 	TYPE_DETECTION_TIME_CMD	(0xFA << 8)
#define 	TYPE_CAMERA_CMD		(0xF9 << 8)
#define 	TYPE_SENSOR_STATE_CMD	(0xF8 << 8)
#define 	TYPE_KEY_CONTROL_CMD	(0xF7 << 8)
#define		TYPE_OTHER_CMD		(0xF5 << 8)

#define 	IOCTL_DOOR_DINGDONG		(TYPE_GPIO_CMD|0x01) //ff
#define		IOCTL_BTNEMR_LEDONOFF		(TYPE_GPIO_CMD|0x02)
#define		IOCTL_BTNCALL_LEDONOFF		(TYPE_GPIO_CMD|0x03)
#define 	IOCTL_BTNOPEN_LEDONOFF		(TYPE_GPIO_CMD|0x04)
#define 	IOCTL_SUBTOUCH_POWERONOFF	(TYPE_GPIO_CMD|0x05)
#define 	IOCTL_VIDEOOUT_DOORONOFF	(TYPE_GPIO_CMD|0x06)
#define 	IOCTL_CAMERA_POWERONOFF		(TYPE_GPIO_CMD|0x07)
#define 	IOCTL_SUBPHONE_MUTEONOFF	(TYPE_GPIO_CMD|0x08)
#define 	IOCTL_DOOR_LOCK_OPEN_REGIS	(TYPE_GPIO_CMD|0x09)
#define 	IOCTL_RFREMOCON_RESET		(TYPE_GPIO_CMD|0x0A)
#define 	IOCTL_DOOR_POWERONOFF		(TYPE_GPIO_CMD|0x0B)
#define		IOCTL_FRONTHOME_LEDONOFF	(TYPE_GPIO_CMD|0x0C)
#define		IOCTL_FRONTEMR_LEDONOFF		(TYPE_GPIO_CMD|0x0D)
#define		IOCTL_BTNEMR_LED_TRIGGER	(TYPE_GPIO_CMD|0x0E)
#define		IOCTL_DOOR_POWER_STATE		(TYPE_GPIO_CMD|0x0F)
#define		IOCTL_DOOR_PATH_ENABLE		(TYPE_GPIO_CMD|0x10)
#define		IOCTL_DOOR_CX93021_RESET	(TYPE_GPIO_CMD|0x11)
	
#define 	IOCTL_VIDEOINPUT_DOORCAMERA	(TYPE_VIDEO_CMD|0x01) //fe

#define 	IOCTL_MIC_GAIN_SET		(TYPE_AUDIO_CMD|0x01) //fd
#define 	IOCTL_MIC_GAIN_GET		(TYPE_AUDIO_CMD|0x02)
#define 	IOCTL_MIC_RECORD_GAIN_SET	(TYPE_AUDIO_CMD|0x03)
#define 	IOCTL_MIC_RECORD_GAIN_GET	(TYPE_AUDIO_CMD|0x04)
#define 	IOCTL_MIC_BOOST_GAIN_SET	(TYPE_AUDIO_CMD|0x05)
#define 	IOCTL_MIC_BOOST_GAIN_GET	(TYPE_AUDIO_CMD|0x06)
#define 	IOCTL_MIC_LINE1_GAIN_SET	(TYPE_AUDIO_CMD|0x07)
#define 	IOCTL_MIC_LINE1_GAIN_GET	(TYPE_AUDIO_CMD|0x08)
#define 	IOCTL_SPK_EQ_GAIN_SET		(TYPE_AUDIO_CMD|0x09)
#define 	IOCTL_SPK_EQ_GAIN_GET		(TYPE_AUDIO_CMD|0x0A)
#define 	IOCTL_MIC_DSP_MAX_GAIN_SET	(TYPE_AUDIO_CMD|0x0B)
#define 	IOCTL_MIC_DSP_MAX_GAIN_GET	(TYPE_AUDIO_CMD|0x0C)
#define 	IOCTL_MONOOUT_SUB_GAIN_SET	(TYPE_AUDIO_CMD|0x0D)
#define 	IOCTL_MONOOUT_SUB_GAIN_GET	(TYPE_AUDIO_CMD|0x0E)

#define 	IOCTL_MOOD_BACKLIGHT_SET	(TYPE_BACKLIGHT_CMD|0x01) //fc

#define 	IOCTL_ETHERNET_MAC_SET		(TYPE_ETHERNET_CMD|0x01) //fb

#define 	IOCTL_DOOR_DETECTIME_SET	    (TYPE_DETECTION_TIME_CMD|0x01)  //fa
#define 	IOCTL_SENSOR_MAG0_DETECTIME_SET	    (TYPE_DETECTION_TIME_CMD|0x02)
#define 	IOCTL_SENSOR_EXIT_DETECTIME_SET	    (TYPE_DETECTION_TIME_CMD|0x04)
#define 	IOCTL_SENSOR_MOTIN_DETECTIME_SET    (TYPE_DETECTION_TIME_CMD|0x05)
#define 	IOCTL_SENSOR_MOTOUT_DETECTIME_SET   (TYPE_DETECTION_TIME_CMD|0x06)
#define 	IOCTL_SENSOR_EMR_DETECTIME_SET	    (TYPE_DETECTION_TIME_CMD|0x07)

#define 	IOCTL_CAMERA_BRIGHTNESS_SET	    (TYPE_CAMERA_CMD|0x01) //f9
#define 	IOCTL_CAMERA_CONTRAST_SET           (TYPE_CAMERA_CMD|0x02)

#define 	IOCTL_SENSOR_MAG0_STATE             (TYPE_SENSOR_STATE_CMD|0x01) //f8
#define 	IOCTL_SENSOR_EXIT_STATE             (TYPE_SENSOR_STATE_CMD|0x03)
#define 	IOCTL_SENSOR_MOTIN_STATE            (TYPE_SENSOR_STATE_CMD|0x04)
#define 	IOCTL_SENSOR_MOTOUT_STATE           (TYPE_SENSOR_STATE_CMD|0x05)
#define 	IOCTL_SENSOR_EMR_STATE              (TYPE_SENSOR_STATE_CMD|0x06)

#define 	IOCTL_KEY_ONOFF_SET                 (TYPE_KEY_CONTROL_CMD|0x01) //f7
#define 	IOCTL_KEY_ONOFF_GET                 (TYPE_KEY_CONTROL_CMD|0x02)


#define 	IOCTL_DEVICE_ALL_OPEN		(TYPE_OTHER_CMD | 0x01) //f5
#define 	IOCTL_DEVICE_ALL_CLOSE		(TYPE_OTHER_CMD | 0x02)
#define		IOCTL_AUDIO_PATH_NORMAL		(TYPE_OTHER_CMD | 0x03)
#define		IOCTL_AUDIO_PATH_DOOR_WALLPAD	(TYPE_OTHER_CMD | 0x04)
#define		IOCTL_AUDIO_PATH_MOIP_PSTN	(TYPE_OTHER_CMD | 0x05)
#define		IOCTL_AUDIO_PATH_GUARD_WALLPAD	(TYPE_OTHER_CMD | 0x06)
#define		IOCTL_AUDIO_PATH_GUARD_SUB	(TYPE_OTHER_CMD | 0x07)
#define		IOCTL_AUDIO_PATH_DOOR_SUB	(TYPE_OTHER_CMD | 0x08)

#define		IOCTL_HW_REVISION		(TYPE_OTHER_CMD | 0x09)

#define		IOCTL_LED_PWR_CONT		(TYPE_OTHER_CMD | 0x0a)
#define		IOCTL_SUB_PWR_CONT		(TYPE_OTHER_CMD | 0x0b)
#define		IOCTL_SENSE_PWR_CONT		(TYPE_OTHER_CMD | 0x0c)
#define		IOCTL_USB_ON_OFF		(TYPE_OTHER_CMD | 0x0d)
#define		IOCTL_IHN_MODEL			(TYPE_OTHER_CMD | 0x0f)
#define		IOCTL_CAM_STATE			(TYPE_OTHER_CMD | 0xf5)
#define		IOCTL_ANALOG_CAM_STAT		(TYPE_OTHER_CMD | 0xf6)
#define		IOCTL_ANALOG_CAM_SET		(TYPE_OTHER_CMD | 0xf7)

#define IOCTL_WPD_MAGIC     'G'  

static char vdp_str[50];
static char *vdp_name;
static char vdp_name_null[20];

static script_item_u wpd_cx93021_reset;
static script_item_u wpd_sub_pwr_cont;
static script_item_u wpd_sense_pwr_cont;
static script_item_u wpd_led_pwr_cont;
static script_item_u wpd_usb_on_off;

static script_item_u wpd_rf_nreset;
static script_item_u wpd_cam_pwen;
static script_item_u wpd_audio_path3;
static script_item_u wpd_bt_pwr_on;
static script_item_u wpd_led_nemr;
static script_item_u wpd_led_ncall;
static script_item_u wpd_led_nopen;

//pp
//static script_item_u wpd_cam_enable;


static script_item_u wpd_audio_path0;
static script_item_u wpd_audio_path1;
static script_item_u wpd_audio_path2;
static script_item_u wpd_audio_path_nen;

static script_item_u wpd_34118_mute;
static script_item_u wpd_door_pwr_en;
static script_item_u wpd_door_path_en;

static script_item_u wpd_ring_detect;
static script_item_u wpd_emergency;
static script_item_u wpd_exit_mcu;
static script_item_u wpd_magnetic0_mcu;
static script_item_u wpd_bt_nemr;
static script_item_u wpd_bt_ncall;
static script_item_u wpd_bt_nopen;
static script_item_u wpd_motion_out;
static script_item_u wpd_motion_in;
static script_item_u wpd_ndoor_detect;


static script_item_u wpd_wddl;
static script_item_u wpd_dingdong;
static script_item_u wpd_led_home;
static script_item_u wpd_led_emr;
static script_item_u wpd_video_out_select;

static script_item_u wpd_rev_opt;
static script_item_u wpd_rev_sel;
int ihn_model = -1;
static int wpd_sys_config(char *main_name, char *sub_name, script_item_u *item)
{
    int	                        req_status;
	script_item_value_type_e    type;    

        /* gpio list */
	type = script_get_item(main_name, sub_name, item);
	if(SCIRPT_ITEM_VALUE_TYPE_PIO != type) {
		printk("script_get_item return type err\n");
		return -ECFGERR;
	}

        /* gpio */
	req_status = gpio_request(item->gpio.gpio, NULL);
	if(0 != req_status) {
		printk("request gpio failed\n");
                return -ENORMALPIN;
	}

        /* gpio */
	if(0 != sw_gpio_setall_range(&item->gpio, 1)) {
		printk("sw_gpio_setall_range failed\n");
        gpio_free(item->gpio.gpio);
       return -ECFGPIN;
   }

        /* gpio */
	if(0 == req_status)
		gpio_free(item->gpio.gpio);

    return 0;
}


typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_wpd_info; 
 
 
#define IOCTL_WPD_RD			_IOR(IOCTL_WPD_MAGIC, 0 , ioctl_wpd_info )  
#define IOCTL_WPD_WR			_IOW(IOCTL_WPD_MAGIC, 1 , ioctl_wpd_info )  

int wpd_key_handler_count = 0;
void wpd_key_handler()
{
	if(key_control_onoff)
	{
		BTNEMR_LEDONOFF(BTN_LED_OFF);
		BTNCALL_LEDONOFF(BTN_LED_OFF);
		BTNOPEN_LEDONOFF(BTN_LED_OFF);
	}
}
int doorlock_tmp = 0;
int dl_open = 0;
int dl_regis = 0;
static void doorlock_open_work(struct work_struct *work)
{

	DOOR_LOCKONOFF(dl_open % 2);
	printk("Door lock open %d\n", dl_open % 2);
	dl_open++;
	if(dl_open < 5)
    	schedule_delayed_work(&door_lock_open_work, 20); //10ms
}

static void doorlock_regis_work(struct work_struct *work)
{
	DOOR_LOCKONOFF(dl_regis % 2);
	printk("Door lock regis %d\n", dl_regis % 2);
	dl_regis++;
	if(dl_regis < 7)
    	schedule_delayed_work(&door_lock_regis_work, 20); //10ms}
}

static int door_lock_open(void)
{
	dl_open = 0;
    	schedule_delayed_work(&door_lock_open_work, 10); //10ms
	return 0;
}

static int door_lock_regis(void)
{
	dl_regis = 0;
    	schedule_delayed_work(&door_lock_regis_work, 10); //10ms
	return 0;
}

static void emr_led_handle_timer(unsigned long data)
{
	if(BTNEMR_LED_STATE() == BTN_LED_ON)
	{
	    BTNEMR_LEDONOFF(BTN_LED_OFF);
	    printk("%s: BTNEMR_LED_OFF!\n", __func__);
	}
	else
	{
	    BTNEMR_LEDONOFF(BTN_LED_ON);
	    printk("%s: BTNEMR_LED_ON!\n", __func__);
	}

	emr_led_timer.expires = get_jiffies_64() + emrledrate;
	mod_timer(&emr_led_timer, emr_led_timer.expires);
}

static int emr_led_timer_init(void)
{
	printk("%s\n",__func__);
	emr_led_timer_status = 1;
	del_timer(&emr_led_timer);
	init_timer(&emr_led_timer);
        emr_led_timer.function = emr_led_handle_timer;
        emr_led_timer.expires = get_jiffies_64() + emrledrate;
        add_timer(&emr_led_timer);
	
	return 0;
}

static int emr_led_timer_del(void)
{
	printk("%s\n",__func__);
	if(emr_led_timer_status == 1)
	{
		BTNEMR_LEDONOFF(BTN_LED_OFF);
		del_timer_sync(&emr_led_timer);
	}
	emr_led_timer_status = 0;

	return 0;
}


void write_wpd(int comm, int data)
{
	int i;

	__gpio_set_value(wpd_cx93021_reset.gpio.gpio,data);
	__gpio_set_value(wpd_sub_pwr_cont.gpio.gpio,data);
	__gpio_set_value(wpd_sense_pwr_cont.gpio.gpio,data);
	__gpio_set_value(wpd_led_pwr_cont.gpio.gpio,data);
	__gpio_set_value(wpd_usb_on_off.gpio.gpio,data);

	__gpio_set_value(wpd_rf_nreset.gpio.gpio,data);
	__gpio_set_value(wpd_cam_pwen.gpio.gpio,data);
	__gpio_set_value(wpd_audio_path3.gpio.gpio,data);
	__gpio_set_value(wpd_bt_pwr_on.gpio.gpio,data);
	__gpio_set_value(wpd_led_nemr.gpio.gpio,data);
	__gpio_set_value(wpd_led_ncall.gpio.gpio,data);
	__gpio_set_value(wpd_led_nopen.gpio.gpio,data);

	__gpio_set_value(wpd_audio_path0.gpio.gpio,data);
	__gpio_set_value(wpd_audio_path1.gpio.gpio,data);
	__gpio_set_value(wpd_audio_path2.gpio.gpio,data);
	__gpio_set_value(wpd_audio_path_nen.gpio.gpio,data);

	__gpio_set_value(wpd_34118_mute.gpio.gpio,data);
	__gpio_set_value(wpd_door_pwr_en.gpio.gpio,data);
	__gpio_set_value(wpd_door_path_en.gpio.gpio,data);


	__gpio_set_value(wpd_ring_detect.gpio.gpio,data);
	__gpio_set_value(wpd_emergency.gpio.gpio,data);
	__gpio_set_value(wpd_exit_mcu.gpio.gpio,data);
	__gpio_set_value(wpd_magnetic0_mcu.gpio.gpio,data);
	__gpio_set_value(wpd_bt_nemr.gpio.gpio,data);
	__gpio_set_value(wpd_bt_ncall.gpio.gpio,data);
	__gpio_set_value(wpd_bt_nopen.gpio.gpio,data);
	__gpio_set_value(wpd_motion_out.gpio.gpio,data);
	__gpio_set_value(wpd_motion_in.gpio.gpio,data);
	__gpio_set_value(wpd_ndoor_detect.gpio.gpio,data);


	__gpio_set_value(wpd_wddl.gpio.gpio,data);
	__gpio_set_value(wpd_dingdong.gpio.gpio,data);
	__gpio_set_value(wpd_led_home.gpio.gpio,data);
	__gpio_set_value(wpd_led_emr.gpio.gpio,data);
	__gpio_set_value(wpd_video_out_select.gpio.gpio,data);



	printk("xxx cx93021_reset %d\n",__gpio_get_value(wpd_cx93021_reset.gpio.gpio));
	printk("xxx sub_pwr_cont %d\n",__gpio_get_value(wpd_sub_pwr_cont.gpio.gpio));
	printk("xxx sense_pwr_cont %d\n",__gpio_get_value(wpd_sense_pwr_cont.gpio.gpio));
	printk("xxx led_pwr_cont %d\n",__gpio_get_value(wpd_led_pwr_cont.gpio.gpio));
	printk("xxx usb_on_off %d\n",__gpio_get_value(wpd_usb_on_off.gpio.gpio));



	printk("xxx rf_nreset %d\n",__gpio_get_value(wpd_rf_nreset.gpio.gpio));
	printk("xxx cam_pwen %d\n",__gpio_get_value(wpd_cam_pwen.gpio.gpio));
	printk("xxx audio_path3 %d\n",__gpio_get_value(wpd_audio_path3.gpio.gpio));
	printk("xxx wpd_bt_pwr_on %d\n",__gpio_get_value(wpd_bt_pwr_on.gpio.gpio));
	printk("xxx wpd_led_nemr %d\n",__gpio_get_value(wpd_led_nemr.gpio.gpio));
	printk("xxx wpd_led_ncall %d\n",__gpio_get_value(wpd_led_ncall.gpio.gpio));
	printk("xxx wpd_led_nopen %d\n",__gpio_get_value(wpd_led_nopen.gpio.gpio));

	printk("xxx audio_path0 %d\n",__gpio_get_value(wpd_audio_path0.gpio.gpio));
	printk("xxx wpd_audio_path1 %d\n",__gpio_get_value(wpd_audio_path1.gpio.gpio));
	printk("xxx wpd_audio_path2 %d\n",__gpio_get_value(wpd_audio_path2.gpio.gpio));
	printk("xxx audio_path_nen %d\n",__gpio_get_value(wpd_audio_path_nen.gpio.gpio));

	printk("xxx 34118_mute %d\n",__gpio_get_value(wpd_34118_mute.gpio.gpio));
	printk("xxx door_pwr_en %d\n",__gpio_get_value(wpd_door_pwr_en.gpio.gpio));
	printk("xxx door_path_en %d\n",__gpio_get_value(wpd_door_path_en.gpio.gpio));


	printk("xxx ring_detect %d\n",__gpio_get_value(wpd_ring_detect.gpio.gpio));
	printk("xxx emergency %d\n",__gpio_get_value(wpd_emergency.gpio.gpio));
	printk("xxx exit_mcu %d\n",__gpio_get_value(wpd_exit_mcu.gpio.gpio));
	printk("xxx magnetic0 %d\n",__gpio_get_value(wpd_magnetic0_mcu.gpio.gpio));
	printk("xxx bt_nemr %d\n",__gpio_get_value(wpd_bt_nemr.gpio.gpio));
	printk("xxx bt_ncall %d\n",__gpio_get_value(wpd_bt_ncall.gpio.gpio));
	printk("xxx bt_nopen %d\n",__gpio_get_value(wpd_bt_nopen.gpio.gpio));
	printk("xxx motion_out %d\n",__gpio_get_value(wpd_motion_out.gpio.gpio));
	printk("xxx motion_in %d\n",__gpio_get_value(wpd_motion_in.gpio.gpio));
	printk("xxx ndoor_detect %d\n",__gpio_get_value(wpd_ndoor_detect.gpio.gpio));


	printk("xxx wddl %d\n",__gpio_get_value(wpd_wddl.gpio.gpio));
	printk("xxx dingdong %d\n",__gpio_get_value(wpd_dingdong.gpio.gpio));
	printk("xxx led_home %d\n",__gpio_get_value(wpd_led_home.gpio.gpio));
	printk("xxx led_emr %d\n",__gpio_get_value(wpd_led_emr.gpio.gpio));
	printk("xxx video_out_select %d\n",__gpio_get_value(wpd_video_out_select.gpio.gpio));

}

int read_wpd(int comm)
{
	int data = 0;


	__gpio_set_value(wpd_cx93021_reset.gpio.gpio,data);
	__gpio_set_value(wpd_sub_pwr_cont.gpio.gpio,data);
	__gpio_set_value(wpd_sense_pwr_cont.gpio.gpio,data);
	__gpio_set_value(wpd_led_pwr_cont.gpio.gpio,data);
	__gpio_set_value(wpd_usb_on_off.gpio.gpio,data);

	__gpio_set_value(wpd_rf_nreset.gpio.gpio,data);
	__gpio_set_value(wpd_cam_pwen.gpio.gpio,data);
	__gpio_set_value(wpd_audio_path3.gpio.gpio,data);
	__gpio_set_value(wpd_bt_pwr_on.gpio.gpio,data);
	__gpio_set_value(wpd_led_nemr.gpio.gpio,data);
	__gpio_set_value(wpd_led_ncall.gpio.gpio,data);
	__gpio_set_value(wpd_led_nopen.gpio.gpio,data);

	__gpio_set_value(wpd_audio_path0.gpio.gpio,data);
	__gpio_set_value(wpd_audio_path1.gpio.gpio,data);
	__gpio_set_value(wpd_audio_path2.gpio.gpio,data);
	__gpio_set_value(wpd_audio_path_nen.gpio.gpio,data);

	__gpio_set_value(wpd_34118_mute.gpio.gpio,data);
	__gpio_set_value(wpd_door_pwr_en.gpio.gpio,data);
	__gpio_set_value(wpd_door_path_en.gpio.gpio,data);


	__gpio_set_value(wpd_ring_detect.gpio.gpio,data);
	__gpio_set_value(wpd_emergency.gpio.gpio,data);
	__gpio_set_value(wpd_exit_mcu.gpio.gpio,data);
	__gpio_set_value(wpd_magnetic0_mcu.gpio.gpio,data);
	__gpio_set_value(wpd_bt_nemr.gpio.gpio,data);
	__gpio_set_value(wpd_bt_ncall.gpio.gpio,data);
	__gpio_set_value(wpd_bt_nopen.gpio.gpio,data);
	__gpio_set_value(wpd_motion_out.gpio.gpio,data);
	__gpio_set_value(wpd_motion_in.gpio.gpio,data);
	__gpio_set_value(wpd_ndoor_detect.gpio.gpio,data);


	__gpio_set_value(wpd_wddl.gpio.gpio,data);
	__gpio_set_value(wpd_dingdong.gpio.gpio,data);
	__gpio_set_value(wpd_led_home.gpio.gpio,data);
	__gpio_set_value(wpd_led_emr.gpio.gpio,data);
	__gpio_set_value(wpd_video_out_select.gpio.gpio,data);



	printk("xxx cx93021_reset %d\n",__gpio_get_value(wpd_cx93021_reset.gpio.gpio));
	printk("xxx sub_pwr_cont %d\n",__gpio_get_value(wpd_sub_pwr_cont.gpio.gpio));
	printk("xxx sense_pwr_cont %d\n",__gpio_get_value(wpd_sense_pwr_cont.gpio.gpio));
	printk("xxx led_pwr_cont %d\n",__gpio_get_value(wpd_led_pwr_cont.gpio.gpio));
	printk("xxx usb_on_off %d\n",__gpio_get_value(wpd_usb_on_off.gpio.gpio));

	printk("xxx rf_nreset %d\n",__gpio_get_value(wpd_rf_nreset.gpio.gpio));
	printk("xxx cam_pwen %d\n",__gpio_get_value(wpd_cam_pwen.gpio.gpio));
	printk("xxx audio_path3 %d\n",__gpio_get_value(wpd_audio_path3.gpio.gpio));
	printk("xxx wpd_bt_pwr_on %d\n",__gpio_get_value(wpd_bt_pwr_on.gpio.gpio));
	printk("xxx wpd_led_nemr %d\n",__gpio_get_value(wpd_led_nemr.gpio.gpio));
	printk("xxx wpd_led_ncall %d\n",__gpio_get_value(wpd_led_ncall.gpio.gpio));
	printk("xxx wpd_led_nopen %d\n",__gpio_get_value(wpd_led_nopen.gpio.gpio));

	printk("xxx audio_path0 %d\n",__gpio_get_value(wpd_audio_path0.gpio.gpio));
	printk("xxx wpd_audio_path1 %d\n",__gpio_get_value(wpd_audio_path1.gpio.gpio));
	printk("xxx wpd_audio_path2 %d\n",__gpio_get_value(wpd_audio_path2.gpio.gpio));
	printk("xxx audio_path_nen %d\n",__gpio_get_value(wpd_audio_path_nen.gpio.gpio));

	printk("xxx 34118_mute %d\n",__gpio_get_value(wpd_34118_mute.gpio.gpio));
	printk("xxx door_pwr_en %d\n",__gpio_get_value(wpd_door_pwr_en.gpio.gpio));
	printk("xxx door_path_en %d\n",__gpio_get_value(wpd_door_path_en.gpio.gpio));


	printk("xxx ring_detect %d\n",__gpio_get_value(wpd_ring_detect.gpio.gpio));
	printk("xxx emergency %d\n",__gpio_get_value(wpd_emergency.gpio.gpio));
	printk("xxx exit_mcu %d\n",__gpio_get_value(wpd_exit_mcu.gpio.gpio));
	printk("xxx magnetic0 %d\n",__gpio_get_value(wpd_magnetic0_mcu.gpio.gpio));
	printk("xxx bt_nemr %d\n",__gpio_get_value(wpd_bt_nemr.gpio.gpio));
	printk("xxx bt_ncall %d\n",__gpio_get_value(wpd_bt_ncall.gpio.gpio));
	printk("xxx bt_nopen %d\n",__gpio_get_value(wpd_bt_nopen.gpio.gpio));
	printk("xxx motion_out %d\n",__gpio_get_value(wpd_motion_out.gpio.gpio));
	printk("xxx motion_in %d\n",__gpio_get_value(wpd_motion_in.gpio.gpio));
	printk("xxx ndoor_detect %d\n",__gpio_get_value(wpd_ndoor_detect.gpio.gpio));


	printk("xxx wddl %d\n",__gpio_get_value(wpd_wddl.gpio.gpio));
	printk("xxx dingdong %d\n",__gpio_get_value(wpd_dingdong.gpio.gpio));
	printk("xxx led_home %d\n",__gpio_get_value(wpd_led_home.gpio.gpio));
	printk("xxx led_emr %d\n",__gpio_get_value(wpd_led_emr.gpio.gpio));
	printk("xxx video_out_select %d\n",__gpio_get_value(wpd_video_out_select.gpio.gpio));

	return 0;	
}


static int a20_wpd_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int a20_wpd_release(struct inode *inode, struct file *file)
{
 	
    return 0;
}

void set_bright(int arg)
{
	int bri;
	int con;
	//int i;
	//for(i = 0 ; i < arg ; i++)
	//	ret = ret+4;
	//printk("bri_ret = %d\n",arg);
#if 1
	switch(arg)
	{
		case 2:
			bri = 0x20;
			con = 0x40;
		break;
		case 4:
			bri = 0x28;
			con = 0x40;
		break;
		case 6:
			bri = 0x30;
			con = 0x50;
		break;
		case 8:
			bri = 0x3a;
			con = 0x60;
		break;
		case 10:
			bri = 0x42;
			con = 0x68;
		break;
		case 12:
			bri = 0x54;
			con = 0x68;
		break;
		case 14:
			bri = 0x60;
			con = 0x70;
		break;
		case 16:
			bri = 0x6a;
			con = 0x70;
		break;
		case 18:
			bri = 0x74;
			con = 0x80;
		break;
		case 20:
			bri = 0x80;
			con = 0x80;
		break;
		case 22:
			bri = 0x88;
			con = 0x80;
		break;
	}
#endif

	//TVD_set_bright(0,0x20, ret);
	TVD_set_bright(0,0x20, bri);
	TVD_set_contrast(0,con, 0);


}


void set_contrast(int arg)
{
	int con;
	//int i;
	//for(i = 0 ; i < arg ; i++)
	//	ret = ret+4;
	//printk("bri_ret = %d\n",arg);
#if 1
	switch(arg)
	{
		case 2:
			con = 0x40;
		break;
		case 4:
			con = 0x40;
		break;
		case 6:
			con = 0x50;
		break;
		case 8:
			con = 0x60;
		break;
		case 10:
			con = 0x68;
		break;
		case 12:
			con = 0x68;
		break;
		case 14:
			con = 0x70;
		break;
		case 16:
			con = 0x70;
		break;
		case 18:
			con = 0x80;
		break;
		case 20:
			con = 0x80;
		break;
		case 22:
			con = 0x80;
		break;
	}
#endif

	//TVD_set_bright(0,0x20, ret);
	TVD_set_contrast(0,con, 0);

}

int readyToDoorScan = 0; //0 == ready, 1 == not.
int DoorkeyOn = 0; //1 == evt up on, 0 == evt off.

static void door_ready(struct work_struct *work)
{
	printk("Door path enable default\n");
	readyToDoorScan = 0;
	DoorkeyOn = 0;
}

static long a20_wpd_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{

    int ret;
    int sensor_state;
    unsigned int val;
    ioctl_wpd_info ctrl_info;  
/*	if(cmd != 0xf5f5)
		printk("CMD = 0x%04x %d\n", cmd, arg);
*/
    switch(cmd)
    {
    	case IOCTL_WPD_RD:    		
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_wpd_info));
    		printk("WPD RD: addr[%02lx]->[%02lx]\n", ctrl_info.addr, ctrl_info.data);
        	ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_wpd_info));    
    	break;
    	case IOCTL_WPD_WR:
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_wpd_info));
   		printk("WPD WR: addr[%02lx]<-[%02lx]\n", ctrl_info.addr, ctrl_info.data);
    	break;

    	case IOCTL_DEVICE_ALL_CLOSE:    		
		ret = read_wpd(0);
    	break;
    	case IOCTL_DEVICE_ALL_OPEN:
		write_wpd(1,1);
    	break;

	case IOCTL_DOOR_DINGDONG:
		DOOR_DINGDONG(POWER_ON); 
		mdelay(10);
		DOOR_DINGDONG(POWER_OFF);
	break;

	case IOCTL_BTNEMR_LEDONOFF:
		if(arg == BTN_LED_ON) {
		    BTNEMR_LEDONOFF(BTN_LED_ON);
		}
		else {
		    BTNEMR_LEDONOFF(BTN_LED_OFF);
		}	
	break;

	case IOCTL_BTNEMR_LED_TRIGGER:
		if(arg == BTN_LED_TRIGGER_ON) {
		    val = emr_led_timer_init();
		    return val;
		}
		else {
		    val = emr_led_timer_del();
		    return val;
		}	
	break;

	case IOCTL_BTNCALL_LEDONOFF:
		if(arg == BTN_LED_ON) {
		    BTNCALL_LEDONOFF(BTN_LED_ON);
		}
		else {
		    BTNCALL_LEDONOFF(BTN_LED_OFF);
		}	
	break;

	case IOCTL_BTNOPEN_LEDONOFF:
		if(arg == BTN_LED_ON) {
		    BTNOPEN_LEDONOFF(BTN_LED_ON);
		}
		else {
		    BTNOPEN_LEDONOFF(BTN_LED_OFF);
		}	
	break;

	case IOCTL_SUBTOUCH_POWERONOFF:
		if(arg == POWER_ON) {
		    SUBTOUCH_POWERONOFF(POWER_ON);
		}
		else {
		    SUBTOUCH_POWERONOFF(POWER_OFF);
		}	
	break;

	case IOCTL_VIDEOOUT_DOORONOFF:
		if(arg == VIDEO_OUT_DOOR_ON) {
		    VIDEOOUT_DOORONOFF(VIDEO_OUT_DOOR_ON);
		}
		else {
		    VIDEOOUT_DOORONOFF(VIDEO_OUT_DOOR_OFF);
		}	
	break;

	case IOCTL_CAMERA_POWERONOFF:
		if(arg == POWER_ON) {
		    cam_state = CAM_STATE_FRONT;
		    CAMERA_POWERONOFF(POWER_ON);
		}
		else if(arg == CAM_STATE_REC) {
		    cam_state = CAM_STATE_REC;
		    CAMERA_POWERONOFF(POWER_ON);
		}		
		else {
		    cam_state = CAM_STATE_NONE;
		    CAMERA_POWERONOFF(POWER_OFF);
		}	
	break;

	case IOCTL_SUBPHONE_MUTEONOFF:
		if(arg == AUDIO_MUTE_OFF) {
		    SUBPHONE_AUDIO_MUTEONOFF(AUDIO_MUTE_OFF);
		}
		else {
		    SUBPHONE_AUDIO_MUTEONOFF(AUDIO_MUTE_ON);
		}	
	break;

	case IOCTL_DOOR_LOCK_OPEN_REGIS:
		if(arg == DOOR_LOCK_OPEN) {
		    val = door_lock_open();
		    return val;
		}
		else {
		    val = door_lock_regis();
		    return val;
		}	
	break;

	case IOCTL_RFREMOCON_RESET:
		RFREMOCON_RESET(RESET_LOW);
		mdelay(20);
		RFREMOCON_RESET(RESET_HIGH);
	break;

	case IOCTL_VIDEOINPUT_DOORCAMERA:
		if(arg == VIDEOINPUT_CAMERA) {
		    cam_state = CAM_STATE_FRONT;
		    DOOR_POWERONOFF(POWER_OFF);
		    mdelay(20);
		    //CAMERA_POWERONOFF(POWER_ON);
		}
		else {
		    cam_state = CAM_STATE_DOOR;
		    CAMERA_POWERONOFF(POWER_OFF);
		    mdelay(20);
		    //DOOR_POWERONOFF(POWER_ON);
		}
	break;

	case IOCTL_DOOR_POWERONOFF:
		if(arg == POWER_ON) {
		    DOOR_POWERONOFF(POWER_ON);
		    cam_state = CAM_STATE_DOOR;
		}
		else {
		    cam_state = CAM_STATE_NONE;
		    DOOR_POWERONOFF(POWER_OFF);
    	    	    schedule_delayed_work(&door_ready_work, 300); //1ms
		}	
	break;

	case IOCTL_DOOR_POWER_STATE:
		val = DOOR_POWER_STATE();
		return val;
	break;

	case IOCTL_FRONTHOME_LEDONOFF:
		if(arg == LED_ON) {
		    FRONTHOME_LEDONOFF(LED_ON);
		}
		else {
		    FRONTHOME_LEDONOFF(LED_OFF);
		}	
	break;

	case IOCTL_FRONTEMR_LEDONOFF:
		if(arg == LED_ON) {
		    FRONTEMR_LEDONOFF(LED_ON);
		}
		else {
		    FRONTEMR_LEDONOFF(LED_OFF);
		}	
	break;

	case IOCTL_MIC_GAIN_SET:
	{
		char gain;
		gain = (char)(arg&0xff) - 74;
		//PSTN Dialing Mute
		//if(arg > 79) arg = 79;
		
		ac97_write(0x1015, gain);
		ac97_write(0x1016, gain);
		ac97_dsp_init();
		//mic_set_gain(arg);
	    //ret = cx2070x_write(pcodec, ADC2_GAIN_LEFT//0x1015,  vol );
    	//ret = cx2070x_write(pcodec, ADC2_GAIN_RIGHT//0x1016, vol );
	}
	break;

	case IOCTL_MIC_GAIN_GET:
	{
		char gain;
		
		val = ac97_read(0x1015);
		
		gain = (char)(val&0xff) + 74;
		val = gain;
		return val;
	}
	break;

	case IOCTL_MIC_RECORD_GAIN_SET:
		if(arg > 32767)  arg = 32766;
			
		ac97_write(0x1136, arg&0xff);
		ac97_write(0x1137, (arg>>8)&0xff);
		ac97_dsp_init();
		//mic_set_recording_gain(arg);
		//ret = cx2070x_write(pcodec, RECORD_GAIN//0x1136,  vol ); // 20*Log(G/1024) = XdB

	break;

	case IOCTL_MIC_RECORD_GAIN_GET:
		val = ac97_read(0x1137)<<8|ac97_read(0x1136);
		//val = mic_get_recording_gain();
		return val;
	break;

	case IOCTL_MIC_BOOST_GAIN_SET:
		if(arg > 7)   arg = 7;
		ac97_write(0x101E, arg);
		ac97_dsp_init();
		
		//mic_set_boost_gain(arg);
		//ret = cx2070x_write(pcodec, MIC_CONTROL//0x101E,  vol ); // 6dB steps
		
	break;

	case IOCTL_MIC_BOOST_GAIN_GET:
		val = ac97_read(0x101E);
		//val = mic_get_boost_gain();
		return val;
	break;

	case IOCTL_MIC_DSP_MAX_GAIN_SET:
		if(arg > 79)   arg = 79;
		ac97_write(0x1017, arg);
		ac97_dsp_init();
			
		//mic_set_dsp_max_gain(arg);
		//ret = cx2070x_write(pcodec, DSP_MAX_MIC_GAIN//0x1017, vol ); // -74 ~ 5dB
	break;

	case IOCTL_MIC_DSP_MAX_GAIN_GET:
		val = ac97_read(0x1017);		
		//val = mic_get_dsp_max_gain();
		return val;
	break;

	case IOCTL_MIC_LINE1_GAIN_SET:
		if(arg > 31) arg = 31;
		ac97_write(0x101B, arg);
		ac97_dsp_init();
			
		//mic_set_line1_gain(arg);
		//ret = cx2070x_write(pcodec, LINE1_GAIN//0x101B, vol ); // 1.5dB steps
	break;

	case IOCTL_MIC_LINE1_GAIN_GET:
		val = ac97_read(0x1017);		

		//val = mic_get_line1_gain();
		return val;
	break;

	case IOCTL_SPK_EQ_GAIN_SET:
		if(arg > 32767) arg = 32767;
		ac97_write(0x10D7, arg&0xff);
		ac97_write(0x10D8, (arg>>8)&0xff);
		ac97_dsp_init();
			
		//speaker_set_eq_gain(arg);
		//ret = cx2070x_write(pcodec, EQ_GAIN//0x10D7, vol ); // 20*Log(G/4096) = XdB
	break;

	case IOCTL_SPK_EQ_GAIN_GET:
		val = ac97_read(0x10D8)<<8|ac97_read(0x10D7);
		//val = speaker_get_eq_gain();
		return val;
	break;

	case IOCTL_MONOOUT_SUB_GAIN_SET:
		if(arg > 79)  arg = 79;
		ac97_write(0x1012, arg);
		ac97_dsp_init();
			
		//monoout_set_sub_gain(arg);
		//ret = cx2070x_write(pcodec, DAC3_GAIN_SUB//0x1012, vol ); // -74 ~ 5dB
	break;

	case IOCTL_MONOOUT_SUB_GAIN_GET:
		val = ac97_read(0x1012);				
		//val = monoout_get_sub_gain();
		return val;
	break;

	case IOCTL_MOOD_BACKLIGHT_SET:
		//led_mood_backlight_set(arg);
	break;

	case IOCTL_ETHERNET_MAC_SET:
		//val = copy_from_user((void *)str_mac, (const void *)arg, sizeof(str_mac)); 
		//tcc_gmac_addr_set(str_mac);
	break;

	case IOCTL_DOOR_DETECTIME_SET:
		door_detection_time = arg;
	break;

	case IOCTL_SENSOR_EMR_DETECTIME_SET:
		sensor_emr_detection_time = arg;
	break;

	case IOCTL_SENSOR_MAG0_DETECTIME_SET:
		sensor_mag0_detection_time = arg;
	break;

	case IOCTL_SENSOR_EXIT_DETECTIME_SET:
		sensor_exit_detection_time = arg;
	break;

	case IOCTL_SENSOR_MOTIN_DETECTIME_SET:
		sensor_motin_detection_time = arg;
	break;

	case IOCTL_SENSOR_MOTOUT_DETECTIME_SET:
		sensor_motout_detection_time = arg;
	break;

	case IOCTL_SENSOR_MAG0_STATE:
		sensor_state = SENSOR_MAG0_STATE();
		if(sensor_state)
		    return SENSOR_DISABLE;
		else
		    return SENSOR_ENABLE;
	break;

	case IOCTL_SENSOR_EXIT_STATE:
		sensor_state = SENSOR_EXIT_STATE();
		if(sensor_state)
		    return SENSOR_DISABLE;
		else
		    return SENSOR_ENABLE;
	break;

	case IOCTL_SENSOR_MOTIN_STATE:
		sensor_state = SENSOR_MOTIN_STATE();
		if(sensor_state)
		    return SENSOR_ENABLE;
		else
		    return SENSOR_DISABLE;
	break;

	case IOCTL_SENSOR_MOTOUT_STATE:
		sensor_state = SENSOR_MOTOUT_STATE();
		if(sensor_state)
		    return SENSOR_ENABLE;
		else
		    return SENSOR_DISABLE;
	break;

	case IOCTL_SENSOR_EMR_STATE:
		sensor_state = SENSOR_EMR_STATE();
		if(sensor_state)
		    return SENSOR_DISABLE;
		else
		    return SENSOR_ENABLE;
	break;

	case IOCTL_CAMERA_BRIGHTNESS_SET:
		if(arg > 20) // arg: 0 ~ 20
		    arg = 20;
//		TVD_set_bright(0,0x20, arg*10);
		//printk("bright %d\n", arg);
		set_bright(arg);

	break;

	case IOCTL_CAMERA_CONTRAST_SET:
		if(arg > 20) // arg: 0 ~ 20
		   arg = 20;
		//TVD_set_contrast(0,arg*10+50, 0);
		//printk("contra %d\n", arg);
		set_contrast(arg);				
	break;

	case IOCTL_KEY_ONOFF_SET:
		key_control_onoff = arg;
	break;

	case IOCTL_KEY_ONOFF_GET:
		return key_control_onoff;
	break;

	case IOCTL_CAM_STATE:
		return cam_state;
	break;

	case IOCTL_AUDIO_PATH_NORMAL://0
		printk("-->audio path : 0\n");
	__gpio_set_value(wpd_audio_path_nen.gpio.gpio,1);
	__gpio_set_value(wpd_audio_path3.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path2.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path1.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path0.gpio.gpio,0);
	break;

	case IOCTL_AUDIO_PATH_DOOR_WALLPAD://1
		printk("-->audio path : 1\n");
	__gpio_set_value(wpd_audio_path_nen.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path3.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path2.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path1.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path0.gpio.gpio,0);
	break;

	case IOCTL_AUDIO_PATH_MOIP_PSTN://2
		printk("-->audio path : 2\n");
	__gpio_set_value(wpd_audio_path_nen.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path3.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path2.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path1.gpio.gpio,1);
	__gpio_set_value(wpd_audio_path0.gpio.gpio,0);
	break;

	case IOCTL_AUDIO_PATH_GUARD_WALLPAD://3
		printk("-->audio path : 3\n");
	__gpio_set_value(wpd_audio_path_nen.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path3.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path2.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path1.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path0.gpio.gpio,1);
	break;

	case IOCTL_AUDIO_PATH_GUARD_SUB://4
		printk("-->audio path : 4\n");
	__gpio_set_value(wpd_audio_path_nen.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path3.gpio.gpio,1);
	__gpio_set_value(wpd_audio_path2.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path1.gpio.gpio,1);
	__gpio_set_value(wpd_audio_path0.gpio.gpio,1);
	break;

	case IOCTL_AUDIO_PATH_DOOR_SUB://5
		printk("-->audio path : 5\n");
	__gpio_set_value(wpd_audio_path_nen.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path3.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path2.gpio.gpio,1);
	__gpio_set_value(wpd_audio_path1.gpio.gpio,1);
	__gpio_set_value(wpd_audio_path0.gpio.gpio,1);
	break;

	case IOCTL_DOOR_PATH_ENABLE:
	__gpio_set_value(wpd_door_path_en.gpio.gpio,arg);
 	readyToDoorScan = 1;
	break;

	case IOCTL_DOOR_CX93021_RESET:
		__gpio_set_value(wpd_cx93021_reset.gpio.gpio,arg);
	break;

	case IOCTL_LED_PWR_CONT:
	__gpio_set_value(wpd_led_pwr_cont.gpio.gpio,arg);
	break;

	case IOCTL_SUB_PWR_CONT:		
	__gpio_set_value(wpd_sub_pwr_cont.gpio.gpio,arg);
	break;

	case IOCTL_SENSE_PWR_CONT:		
	__gpio_set_value(wpd_sense_pwr_cont.gpio.gpio,arg);
	break;

	case IOCTL_USB_ON_OFF:		
	__gpio_set_value(wpd_usb_on_off.gpio.gpio,arg);
	break;
	
	case IOCTL_HW_REVISION:
		printk("HW_REVISION -> opt=%d sel=%d\n",REV_OPT(), REV_SEL());
		return REV_OPT()+REV_SEL();
	break;
	case IOCTL_IHN_MODEL:
		return ihn_model;
	break;

	case IOCTL_ANALOG_CAM_STAT:
		return analog_cam_stat;
	break;

	case IOCTL_ANALOG_CAM_SET:
		if(arg == 1)	
			analog_cam_stat = 1;  //analog lobby use
		else
			analog_cam_stat = 0;
	break;


	}
    return 0;

}


int door_old_key = 0;
static void door_key_scan(struct work_struct *work)
{
	int key;
    	schedule_delayed_work(&door_delay_work, 5); //1ms
	key = DOOR_KEY_STATE();

	if(door_old_key != key)
	{
		if(key){
		}
		else{
			if(readyToDoorScan == 0 && DoorkeyOn == 0)
			{
			    btn_door();
			    DoorkeyOn = 1;
		 	    printk("%s %d\n",__func__, key);
#if 0
				DOOR_POWERONOFF(POWER_ON);
				DOOR_DINGDONG(POWER_ON);
				mdelay(10);
				DOOR_DINGDONG(POWER_OFF);
#endif
			}
		}
	}
	
	
	door_old_key = key;
}

int emr_old_key = 1;
int call_old_key = 1;
int open_old_key = 1;
static void emr_long_key_handle_timer(unsigned long data)
{
	int key;

	key = EMR_KEY_STATE();
	if(key)
	{
		longkey_count = 0;
		printk("%s status = %d\n",__func__, key);
		BTNEMR_LEDONOFF(BTN_LED_OFF);
	}
	else
	{
		printk("%s status = %d\n",__func__, key);
		longkey_count++;
		emr_long_key_timer.expires = get_jiffies_64() + longkeyrate;
		mod_timer(&emr_long_key_timer, emr_long_key_timer.expires);
		BTNEMR_LEDONOFF(BTN_LED_ON);
	}

	if(longkey_count == 3) // long key event
	{
		printk("%s status = %d Button Event up\n",__func__, key);
		btn_emr();
	}
}

static void emr_key_scan(struct work_struct *work)
{
	int key;

    	schedule_delayed_work(&emr_key_delay_work, 10); //1ms


	key = EMR_KEY_STATE();
	
	if(key != emr_old_key)
	{
		if(key)
		{
			printk("%s status = %d\n",__func__, key);
			longkey_count = 0;
			BTNEMR_LEDONOFF(BTN_LED_OFF);
		}
		else
		{
			printk("%s status = %d\n",__func__, key);
			BTNEMR_LEDONOFF(BTN_LED_ON);
			if(longkey_count == 0)
			{			
			    emr_long_key_timer.expires = get_jiffies_64() + longkeyrate;
			    mod_timer(&emr_long_key_timer, emr_long_key_timer.expires);
			}
		}
	}
	emr_old_key = key;

}


static void call_key_scan(struct work_struct *work)
{
	int key;

	key = CALL_KEY_STATE();
    	schedule_delayed_work(&call_key_delay_work, 10); //1ms

	if(key != call_old_key)
	{
		if(key)
		{
			printk("%s status = %d\n",__func__, key);
			btn_call(key);
			BTNCALL_LEDONOFF(BTN_LED_OFF);
		}
		else
		{
			printk("%s status = %d Button Event up\n",__func__, key);
			btn_call(key);
			BTNCALL_LEDONOFF(BTN_LED_ON);
		}
	}
	call_old_key = key;

}
int open_led_first = 0;
static void open_key_scan(struct work_struct *work)
{
	int key;

	key = OPEN_KEY_STATE();
    	schedule_delayed_work(&open_key_delay_work, 10); //1ms

	if(key != open_old_key)
	{
		if(key)
		{	
			printk("%s status = %d\n",__func__, key);
			btn_bopen(key);
			BTNOPEN_LEDONOFF(BTN_LED_OFF);
		}
		else
		{
			printk("%s status = %d Button Event up\n",__func__, key);
			btn_bopen(key);
			BTNOPEN_LEDONOFF(BTN_LED_ON);
		}
	}
	open_old_key = key;
	if(open_led_first == 0)
	{
		open_led_first=1;
		BTNOPEN_LEDONOFF(BTN_LED_OFF);
	}

}


int ring_evt_old_status = 1;
static void incomingcall_handle_timer(unsigned long data)
{
	int ring_state;
	ring_state = RING_DETECT_STATE();
	BTNCALL_LEDONOFF(BTN_LED_OFF);

	if(wpd_key_handler_count == 0)
	{
		wpd_key_handler();
		wpd_key_handler_count = 1;
	}

}

static void ring_event(struct work_struct *work
)
{
	int ring_state;

	ring_state = RING_DETECT_STATE();

    	schedule_delayed_work(&ring_delay_work, 10); //10ms

	if(ring_state != ring_evt_old_status)

	{
		if(ring_state)
		{
		}
		else
		{
			printk("%s status = %d\n",__func__, ring_state);
			evt_ring();
			BTNCALL_LEDONOFF(BTN_LED_ON);
			incomingcall_led_timer.expires = get_jiffies_64() + incomingcallledrate;
			mod_timer(&incomingcall_led_timer, incomingcall_led_timer.expires);
		}
	}

	ring_evt_old_status = ring_state;
}



int old_sensor_emr_state = 0;
int old_sensor_motion_out_state = 0;
int old_sensor_motion_in_state = 0;
int old_sensor_exit_state = 0;
int old_sensor_mag0_state = 0;
static void sensor_emr_event(struct work_struct *work)
{
	int sensor_emr_state;

	sensor_emr_state = SENSOR_EMR_STATE();

    	schedule_delayed_work(&sensor_emr_delay_work, 10); //10ms

	if(sensor_emr_state != old_sensor_emr_state)
	{
		if(sensor_emr_state)
		{
			printk("%s status = %d\n",__func__, sensor_emr_state);
		}
		else
		{
			printk("%s status = %d\n",__func__, sensor_emr_state);
			evt_emr();
#if 0
			sensor_emr_detection_cnt++;
			if(sensor_emr_detection_cnt >= sensor_emr_detection_time)
			{
			    evt_emr();
			    sensor_emr_detection_cnt = 0;
			}
#endif
		}	
	}
	old_sensor_emr_state = sensor_emr_state;
}

static void sensor_magnetic0_event(struct work_struct *work)
{
	int sensor_mag0_state;

	sensor_mag0_state = SENSOR_MAG0_STATE();
    	schedule_delayed_work(&sensor_mag0_delay_work, 10); //10ms

	if(sensor_mag0_state != old_sensor_mag0_state)
	{
		if(sensor_mag0_state)
		{
			printk("%s status = %d\n",__func__, sensor_mag0_state);
		}
		else
		{
			printk("%s status = %d\n",__func__, sensor_mag0_state);
			    evt_mag0();
#if 0
			sensor_mag0_detection_cnt++;
			if(sensor_mag0_detection_cnt >= sensor_mag0_detection_time)
			{
			    evt_mag0();
			    sensor_mag0_detection_cnt = 0;
			}
#endif
		}
	}
	old_sensor_mag0_state = sensor_mag0_state;
}


static void sensor_exit_event(struct work_struct *work)
{
	int sensor_exit_state;


	sensor_exit_state = SENSOR_EXIT_STATE();
    	schedule_delayed_work(&sensor_exit_delay_work, 10); //10ms

	if(sensor_exit_state != old_sensor_exit_state)
	{
		if(sensor_exit_state)
		{
			printk("%s status = %d\n",__func__, sensor_exit_state);
		}
		else
		{
			printk("%s status = %d\n",__func__, sensor_exit_state);
			    evt_exit();
#if 0
			sensor_exit_detection_cnt++;
			if(sensor_exit_detection_cnt >= sensor_exit_detection_time)
			{
			    evt_exit();
			    sensor_exit_detection_cnt = 0;
			}
#endif
		}
	}
	old_sensor_exit_state = sensor_exit_state;
}

static void sensor_motion_in_event(struct work_struct *work)
{
	int sensor_motion_in_state;


	sensor_motion_in_state = SENSOR_MOTIN_STATE();
    	schedule_delayed_work(&sensor_motin_delay_work, 10); //10ms

	if(sensor_motion_in_state != old_sensor_motion_in_state)
	{
		if(sensor_motion_in_state)
		{
			printk("%s status = %d\n",__func__, sensor_motion_in_state);
			    evt_motin();
#if 0
			sensor_motin_detection_cnt++;
			if(sensor_motin_detection_cnt >= sensor_motin_detection_time)
			{
			    evt_motin();
			    sensor_motin_detection_cnt = 0;
			}
#endif
		}
		else
		{
			printk("%s status = %d\n",__func__, sensor_motion_in_state);
		}
	}
	old_sensor_motion_in_state = sensor_motion_in_state;
}

static void sensor_motion_out_event(struct work_struct *work)
{
	int sensor_motion_out_state;

	sensor_motion_out_state = SENSOR_MOTOUT_STATE();
    	schedule_delayed_work(&sensor_motout_delay_work, 10); //10ms

	if(sensor_motion_out_state != old_sensor_motion_out_state)
	{
		if(sensor_motion_out_state)
		{
			printk("%s status = %d\n",__func__, sensor_motion_out_state);
			    evt_motout();
#if 0
			sensor_motout_detection_cnt++;
			if(sensor_motout_detection_cnt >= sensor_motout_detection_time)
			{
			    evt_motout();
			    sensor_motout_detection_cnt = 0;
			}
#endif
		}
		else
		{
			printk("%s status = %d\n",__func__, sensor_motion_out_state);
		}
	}
	old_sensor_motion_out_state = sensor_motion_out_state;

}


static struct file_operations a20_wpd_fops = { 
	owner:      THIS_MODULE,
	open:    	a20_wpd_open,
	unlocked_ioctl:		a20_wpd_ioctl,
	release:    a20_wpd_release,
};

static struct miscdevice a20_wpd_dev = {
	minor:		142,
	name:		"wpd_interface",
	fops:		&a20_wpd_fops,
};

int a20_wpd_probe(struct platform_device *pdev)
{	
	int err;

	err = misc_register(&a20_wpd_dev);
	if(err) {
		printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n", 108, err);
	}

	return err;
	
}

static int a20_wpd_suspend(struct platform_device *dev, pm_message_t state)
{
	return 0;
}

static int a20_wpd_resume(struct platform_device *pdev)
{
	return 0;
}

static int a20_wpd_remove(struct platform_device *dev)
{
	misc_deregister(&a20_wpd_dev);

	return 0;
}

static struct platform_device a20_wpd_device = {
	.name           	= "a20-wpd",
	.id             	= -1,//??
};

static struct platform_driver a20_wpd_driver = {
       .probe          = a20_wpd_probe,
       .remove         = a20_wpd_remove,
       .suspend        = a20_wpd_suspend,
       .resume         = a20_wpd_resume,
       .driver		= {
		    .owner	= THIS_MODULE,
		    .name	= "a20-wpd",
	}, 
};


static int wpd_port_init(void)
{
	int ret = 0;

//	__gpio_set_value(wpd_cam_enable.gpio.gpio,0);
//pp

	__gpio_set_value(wpd_led_nemr.gpio.gpio,1);
	__gpio_set_value(wpd_led_ncall.gpio.gpio,1);
	__gpio_set_value(wpd_led_nopen.gpio.gpio,1);
	__gpio_set_value(wpd_dingdong.gpio.gpio,0);
	__gpio_set_value(wpd_door_pwr_en.gpio.gpio,0);
	__gpio_set_value(wpd_door_path_en.gpio.gpio,0);
	__gpio_set_value(wpd_34118_mute.gpio.gpio,1);//SUB PHONE AUDIO MUTE ENABLE
	__gpio_set_value(wpd_wddl.gpio.gpio,0);//DOOR LOCK OFF
	__gpio_set_value(wpd_rf_nreset.gpio.gpio,1);
	__gpio_set_value(wpd_bt_pwr_on.gpio.gpio,1);//SUB TOUCH POWER ENABLE
	__gpio_set_value(wpd_cam_pwen.gpio.gpio,0);
	__gpio_set_value(wpd_video_out_select.gpio.gpio,0);

//Undecided
	__gpio_set_value(wpd_led_home.gpio.gpio,1);
	__gpio_set_value(wpd_led_emr.gpio.gpio,1);

	__gpio_set_value(wpd_sub_pwr_cont.gpio.gpio,1);
	__gpio_set_value(wpd_sense_pwr_cont.gpio.gpio,1);
	__gpio_set_value(wpd_led_pwr_cont.gpio.gpio,1);
	__gpio_set_value(wpd_usb_on_off.gpio.gpio,1);

	__gpio_set_value(wpd_audio_path0.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path1.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path2.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path3.gpio.gpio,0);
	__gpio_set_value(wpd_audio_path_nen.gpio.gpio,0);

	return ret;
}


int __init a20_wpd_init(void)
{
 	unsigned int ret;
    script_item_u val;
    script_item_value_type_e type;
    char *vdp;

//	ret = wpd_sys_config("wpd_gpio", "audio_path0", &wpd_bug1);


	ret = wpd_sys_config("wpd_gpio", "cx93021_reset",&wpd_cx93021_reset);
	ret = wpd_sys_config("wpd_gpio", "sub_pwr_cont",&wpd_sub_pwr_cont);
	ret = wpd_sys_config("wpd_gpio", "sense_pwr_cont",&wpd_sense_pwr_cont);
	ret = wpd_sys_config("wpd_gpio", "led_pwr_cont",&wpd_led_pwr_cont);
	ret = wpd_sys_config("wpd_gpio", "usb_drv_vbus_gpio1",&wpd_usb_on_off);

	ret = wpd_sys_config("wpd_gpio", "rf_nreset",&wpd_rf_nreset);
	ret = wpd_sys_config("wpd_gpio", "cam_pwen",&wpd_cam_pwen);
	ret = wpd_sys_config("wpd_gpio", "audio_path3",&wpd_audio_path3);
	ret = wpd_sys_config("wpd_gpio", "bt_pwr_on",&wpd_bt_pwr_on);
	ret = wpd_sys_config("wpd_gpio", "led_nemr",&wpd_led_nemr);

//pp
	//ret = wpd_sys_config("wpd_gpio", "cam_enable",&wpd_cam_enable);

	ret = wpd_sys_config("wpd_gpio", "led_ncall",&wpd_led_ncall);
	ret = wpd_sys_config("wpd_gpio", "led_nopen",&wpd_led_nopen);

	ret = wpd_sys_config("wpd_gpio", "audio_path0",&wpd_audio_path0);
	ret = wpd_sys_config("wpd_gpio", "audio_path1",&wpd_audio_path1);
	ret = wpd_sys_config("wpd_gpio", "audio_path2",&wpd_audio_path2);
	ret = wpd_sys_config("wpd_gpio", "audio_path_nen",&wpd_audio_path_nen);

	ret = wpd_sys_config("wpd_gpio", "34118_mute",&wpd_34118_mute);
	ret = wpd_sys_config("wpd_gpio", "door_pwr_en",&wpd_door_pwr_en);
	ret = wpd_sys_config("wpd_gpio", "door_path_en",&wpd_door_path_en);


	ret = wpd_sys_config("wpd_gpio", "ring_detect",&wpd_ring_detect);
	ret = wpd_sys_config("wpd_gpio", "emergency",&wpd_emergency);
	ret = wpd_sys_config("wpd_gpio", "exit_mcu",&wpd_exit_mcu);
	ret = wpd_sys_config("wpd_gpio", "magnetic0_mcu",&wpd_magnetic0_mcu);
	ret = wpd_sys_config("wpd_gpio", "bt_nemr",&wpd_bt_nemr);
	ret = wpd_sys_config("wpd_gpio", "bt_ncall",&wpd_bt_ncall);
	ret = wpd_sys_config("wpd_gpio", "bt_nopen",&wpd_bt_nopen);
	ret = wpd_sys_config("wpd_gpio", "motion_out",&wpd_motion_out);
	ret = wpd_sys_config("wpd_gpio", "motion_in",&wpd_motion_in);
	ret = wpd_sys_config("wpd_gpio", "ndoor_detect",&wpd_ndoor_detect);


	ret = wpd_sys_config("wpd_gpio", "wddl",&wpd_wddl);
	ret = wpd_sys_config("wpd_gpio", "dingdong",&wpd_dingdong);
	ret = wpd_sys_config("wpd_gpio", "led_home",&wpd_led_home);
	ret = wpd_sys_config("wpd_gpio", "led_emr",&wpd_led_emr);
	ret = wpd_sys_config("wpd_gpio", "video_out_select",&wpd_video_out_select);

	ret = wpd_sys_config("wpd_gpio", "rev_opt",&wpd_rev_opt);
	ret = wpd_sys_config("wpd_gpio", "rev_sel",&wpd_rev_sel);	

    type = script_get_item("board", "name", &val);
    if (type != SCIRPT_ITEM_VALUE_TYPE_STR) {
		printk(KERN_ERR "A20 wpd platform device register failed!\n");
        return -1;
    }
    sprintf(vdp_str, "%s", val.str);

	if((vdp_name = strstr(vdp_str, "1020b")) != NULL)
	{
		vdp_name = vdp_name;
		ihn_model = 1;
	}
	else if((vdp_name = strstr(vdp_str, "1030b")) != NULL)
	{
		vdp_name = vdp_name;
		ihn_model = 0;
	}

    printk("VDP NAME: %s\n", vdp_str);
	wpd_port_init();

	INIT_DELAYED_WORK(&door_ready_work, door_ready);
	INIT_DELAYED_WORK(&door_delay_work, door_key_scan);
	INIT_DELAYED_WORK(&emr_key_delay_work, emr_key_scan);
	INIT_DELAYED_WORK(&call_key_delay_work, call_key_scan);
	INIT_DELAYED_WORK(&open_key_delay_work, open_key_scan);
	INIT_DELAYED_WORK(&ring_delay_work, ring_event);
	INIT_DELAYED_WORK(&sensor_emr_delay_work, sensor_emr_event);
	INIT_DELAYED_WORK(&sensor_mag0_delay_work, sensor_magnetic0_event);
	INIT_DELAYED_WORK(&sensor_exit_delay_work, sensor_exit_event);
	INIT_DELAYED_WORK(&sensor_motin_delay_work, sensor_motion_in_event);
	INIT_DELAYED_WORK(&sensor_motout_delay_work, sensor_motion_out_event);
	INIT_DELAYED_WORK(&door_lock_open_work, doorlock_open_work);
	INIT_DELAYED_WORK(&door_lock_regis_work, doorlock_regis_work);



    	schedule_delayed_work(&door_delay_work, 10); //10ms
    	schedule_delayed_work(&emr_key_delay_work, 10); //10ms
    	schedule_delayed_work(&call_key_delay_work, 10); //10ms
    	schedule_delayed_work(&open_key_delay_work, 10); //10ms
    	schedule_delayed_work(&ring_delay_work, 10); //10ms
    	schedule_delayed_work(&sensor_emr_delay_work, 10); //10ms
    	schedule_delayed_work(&sensor_mag0_delay_work, 10); //10ms
    	schedule_delayed_work(&sensor_exit_delay_work, 10); //10ms
    	schedule_delayed_work(&sensor_motin_delay_work, 10); //10ms
    	schedule_delayed_work(&sensor_motout_delay_work, 10); //10ms


	door_detection_time = 20; //200ms
	sensor_emr_detection_time = 20; //200ms
	sensor_mag0_detection_time = 20;
	sensor_exit_detection_time = 20;
	sensor_motin_detection_time = 20;
	sensor_motout_detection_time = 20;

	key_control_onoff = 1; //default on
	ret = platform_device_register(&a20_wpd_device);
	if (ret) {
		printk(KERN_ERR "A20 wpd platform device register failed!\n");
		return -1;
	}

	ret = platform_driver_register(&a20_wpd_driver);
	if( ret != 0) {
		printk(KERN_ERR "A20 wpd platform device register failed\n");
		return -1;
	}

	init_timer(&incomingcall_led_timer);
        incomingcall_led_timer.function = incomingcall_handle_timer;
        incomingcall_led_timer.expires = get_jiffies_64() + incomingcallledrate;
        add_timer(&incomingcall_led_timer);

	init_timer(&emr_long_key_timer);
        emr_long_key_timer.function = emr_long_key_handle_timer;
        emr_long_key_timer.expires = get_jiffies_64() + longkeyrate;
        add_timer(&emr_long_key_timer);

	wpd_key_handler();
	printk("A20 wpd Driver, (c) 2016 dadamMicro INC\n");


	return 0;
}

void __exit a20_wpd_exit(void)
{
 	cancel_delayed_work_sync(&door_ready_work);
 	cancel_delayed_work_sync(&door_delay_work);
	cancel_delayed_work_sync(&ring_delay_work);
	cancel_delayed_work_sync(&emr_key_delay_work);
	cancel_delayed_work_sync(&call_key_delay_work);
	cancel_delayed_work_sync(&open_key_delay_work);
	cancel_delayed_work_sync(&sensor_emr_delay_work);
	cancel_delayed_work_sync(&sensor_mag0_delay_work);
	cancel_delayed_work_sync(&sensor_exit_delay_work);
	cancel_delayed_work_sync(&sensor_motin_delay_work);
	cancel_delayed_work_sync(&sensor_motout_delay_work);
	cancel_delayed_work_sync(&door_lock_open_work);
	cancel_delayed_work_sync(&door_lock_regis_work);

	del_timer(&incomingcall_led_timer);
	del_timer(&emr_long_key_timer);

	platform_driver_unregister(&a20_wpd_driver);
	platform_device_unregister(&a20_wpd_device);
	
	printk("a20 Wallpad wpd module exit\n");
}

module_init(a20_wpd_init);
module_exit(a20_wpd_exit);

MODULE_AUTHOR("MyoungJoon <ohara@dadammicro.com");
MODULE_DESCRIPTION("A20 Wallpad wpd Device Driver");
MODULE_LICENSE("GPL");
