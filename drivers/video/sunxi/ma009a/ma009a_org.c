#include <linux/module.h>
#include <linux/init.h>
#include <linux/pm.h>
#include <linux/fs.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/device.h>
#include <linux/clk.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/regulator/consumer.h>
#include <linux/uaccess.h>

#include <mach/sys_config.h>
#include <mach/clock.h>
#include <mach/gpio.h>
#include <linux/gpio.h>

#define CONFIG_CDP_1010_R12_LINE1

//error number
#define ECFGNOEX            1   //config not exit
#define ECFGERR             2   //config error
#define ECAMNUMERR          3   //camera number error
#define ECLKSRC             4   //clock source error
#define EDETECTFAIL         5   //detect camera fail
#define EPMUPIN             6   //get pmu pin fail
#define ENORMALPIN          7   //get normal pin fail
#define ECFGPIN             8   //config pin fail
#define EI2CADAPTER         9   //get I2C adapter fail

#define IOCTL_MA009A_MAGIC    'M'  
#define IOCTL_GUARD_MAGIC     'G'  
#define IOCTL_CLH700_MAGIC    'C'  
#define IOCTL_JNS70MN_MAGIC	  'J'
#define IOCTL_CGW500_MAGIC	  'W'
#define IOCTL_CIP700_MAGIC	  'I'
#define IOCTL_CNS70MN_MAGIC	  'N'
#define IOCTL_GBM700_MAGIC	  'G'
#define IOCTL_CDPVDP_MAGIC	  'V'

static int be_ma009a = 1;

extern int get_sc16is752(void);
extern int sc16is752_read(u_char addr);
extern int sc16is752_i2c_read(u_char addr);
//extern int sc16is752_io_read(void);
extern int sc16is752_write(u_char addr, u_char data);
extern int sc16is7x2_check(void);

extern void hc574outb(u8 reg);
extern u8 hc574readb(void);
extern void sc16is7x2_reset(void);
extern void sc16is7x2_i2c_reset(void);

static int alive_1_status = 0;
static int alive_2_status = 0;

static script_item_u ma009a_cd;
static script_item_u ma009a_sda;
static script_item_u ma009a_clk;
static int p1cfg = 0;
static int p1set = 0;
static int p2set = 0;
static char vdp_str[50];
static char *vdp_name;
static char vdp_name_null[20];

static script_item_u ma009a_bug1;
static script_item_u ma009a_bug2;
static script_item_u ma009a_bug3;
static script_item_u ma009a_bug4;
static script_item_u ma009a_bug5;
static script_item_u ma009a_fire;
static script_item_u ma009a_em;
static script_item_u ma009a_gas;

static script_item_u ma009a_ca1_call;
static script_item_u ma009a_ca2_call;
static script_item_u ma009a_ux_gd_call;
static script_item_u ma009a_int_open;
static script_item_u ma009a_int_hook;
static script_item_u ma009a_int_call_in;
static script_item_u ma009a_conv_detect;

static script_item_u ma009a_door1_open_cont;
static script_item_u ma009a_door2_open_cont;
static script_item_u ma009a_int_call_out;
static script_item_u ma009a_relay1_cont;
static script_item_u ma009a_relay2_cont;
static script_item_u ma009a_relay3_cont;
static script_item_u ma009a_talk_sel_a;
static script_item_u ma009a_talk_sel_b;

static script_item_u ma009a_il34118_power_cont;
static script_item_u ma009a_talk_on;
static script_item_u ma009a_fire_power_cont;
static script_item_u ma009a_ca_power_cont;
static script_item_u ma009a_chime_cont;
static script_item_u ma009a_amp_cont;

/*
; guard 
mute				= port:PC14<1><default><default><0>
conv_sel_ry			= port:PC15<1><default><default><1>
tel_ry_cont			= port:PC16<1><default><default><1>
hook_sw				= port:PC17<0><default><default><default>
tel_ring			= port:PC18<0><default><default><default>
ht9200a_ce			= port:PC19<1><default><default><1>
ht9200a_dat			= port:PC20<1><default><default><0>
ht9200a_clk			= port:PC21<1><default><default><0>
*/
static script_item_u mute;
static script_item_u ht9200a_rdat;
static script_item_u tel_ry_cont;
static script_item_u hook_sw;
static script_item_u tel_ring;
static script_item_u ht9200a_ce;
static script_item_u ht9200a_dat;
static script_item_u ht9200a_clk;

/*
; lobby
cmos_en				= port:PH23<1><default><default><0>
door_open1			= port:PH16<1><default><default><0>
power12v_onoff		= port:PH24<1><default><default><0>
*/

static script_item_u cmos_en;
static script_item_u door_open1;
static script_item_u power12v_onoff;

static int ma009a_sys_config(char *main_name, char *sub_name, script_item_u *item)
{
    int	                        req_status;
	script_item_value_type_e    type;    

        /* »ñÈ¡gpio list */
	type = script_get_item(main_name, sub_name, item);
	if(SCIRPT_ITEM_VALUE_TYPE_PIO != type) {
		printk("script_get_item return type err\n");
		return -ECFGERR;
	}

        /* ÉêÇëgpio */
	req_status = gpio_request(item->gpio.gpio, NULL);
	if(0 != req_status) {
		printk("request gpio failed\n");
                return -ENORMALPIN;
	}

        /* ÅäÖÃgpio */
	if(0 != sw_gpio_setall_range(&item->gpio, 1)) {
		printk("sw_gpio_setall_range failed\n");
        gpio_free(item->gpio.gpio);
       return -ECFGPIN;
   }

        /* ÊÍ·Ågpio */
	if(0 == req_status)
		gpio_free(item->gpio.gpio);

    return 0;
}


typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_ma009a_info; 

typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_cdpvdp_info; 


typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_cip700_info; 

typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_gbm700_info; 

typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_clh700_info; 

typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_jns70mn_info; 

typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_cns70mn_info; 

typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_cgw500_info; 

typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_guard_info; 
 
 
#define RING_ON_S		1
#define RING_OFF_S		2

#define IOCTL_MA009A_RD			_IOR(IOCTL_MA009A_MAGIC, 0 , ioctl_ma009a_info )   
#define IOCTL_MA009A_WR			_IOW(IOCTL_MA009A_MAGIC, 1 , ioctl_ma009a_info )  
#define IOCTL_MA009A_INIT		_IOW(IOCTL_MA009A_MAGIC, 2 , ioctl_ma009a_info )  

#define IOCTL_CDPVDP_RD			_IOR(IOCTL_CDPVDP_MAGIC, 0 , ioctl_cdpvdp_info )   
#define IOCTL_CDPVDP_WR			_IOW(IOCTL_CDPVDP_MAGIC, 1 , ioctl_cdpvdp_info )  

#define IOCTL_CLH700_RD			_IOR(IOCTL_CLH700_MAGIC, 0 , ioctl_clh700_info )  
#define IOCTL_CLH700_WR			_IOW(IOCTL_CLH700_MAGIC, 1 , ioctl_clh700_info )  

#define IOCTL_CIP700_RD			_IOR(IOCTL_CIP700_MAGIC, 0 , ioctl_cip700_info )  
#define IOCTL_CIP700_WR			_IOW(IOCTL_CIP700_MAGIC, 1 , ioctl_cip700_info )  

#define IOCTL_GBM700_RD			_IOR(IOCTL_GBM700_MAGIC, 0 , ioctl_gbm700_info )  
#define IOCTL_GBM700_WR			_IOW(IOCTL_GBM700_MAGIC, 1 , ioctl_gbm700_info )  

#define IOCTL_JNS70MN_RD			_IOR(IOCTL_JNS70MN_MAGIC, 0 , ioctl_jns70mn_info )  
#define IOCTL_JNS70MN_WR			_IOW(IOCTL_JNS70MN_MAGIC, 1 , ioctl_jns70mn_info )  

#define IOCTL_CGW500_RD			_IOR(IOCTL_CGW500_MAGIC, 0 , ioctl_cgw500_info )  
#define IOCTL_CGW500_WR			_IOW(IOCTL_CGW500_MAGIC, 1 , ioctl_cgw500_info )  

#define IOCTL_GUARD_RD			_IOR(IOCTL_GUARD_MAGIC, 0 , ioctl_guard_info )  
#define IOCTL_GUARD_WR			_IOW(IOCTL_GUARD_MAGIC, 1 , ioctl_guard_info )  

static void	ring_timer_work(void);

static wait_queue_head_t oki_ring_tx;

struct delayed_work		oki_ring_work;
static int ringdata;
static int last_state = 1;

static int be_door_open = 0;

int is_door_open(void)
{
	return be_door_open;
}
EXPORT_SYMBOL(is_door_open);

void reset_ma009a(void)
{
	int i;
	
	__gpio_set_value(ma009a_cd.gpio.gpio, 1); // C/DB
	__gpio_set_value(ma009a_cd.gpio.gpio, 1); // C/DB

 	sw_gpio_set(&ma009a_sda.gpio, 1);// CLK1  --> J20 3 --> SDA

	for(i = 0; i < 16; i++)
	{
		if(0xfeff & (0x8000 >> i))
		{
			__gpio_set_value(ma009a_sda.gpio.gpio, 1); // SDA
			__gpio_set_value(ma009a_sda.gpio.gpio, 1); // SDA
		}
		else 
		{
			__gpio_set_value(ma009a_sda.gpio.gpio, 0); // SDA
			__gpio_set_value(ma009a_sda.gpio.gpio, 0); // SDA
		}
		__gpio_set_value(ma009a_clk.gpio.gpio, 1); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 1); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 0); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 0); // SCL
	}
}

void write_ma009a(int comm, int data)
{
	int i;
	
	__gpio_set_value(ma009a_cd.gpio.gpio, 1); // C/DB
	__gpio_set_value(ma009a_cd.gpio.gpio, 1); // C/DB
	sw_gpio_set(&ma009a_sda.gpio, 1);// CLK1  --> J20 3 --> SDA
	for(i = 0; i < 8; i++)
	{
		if(comm & (0x80 >> i)) 
		{
			__gpio_set_value(ma009a_sda.gpio.gpio, 1); // SDA
			__gpio_set_value(ma009a_sda.gpio.gpio, 1); // SDA
		}
		else 
		{
			__gpio_set_value(ma009a_sda.gpio.gpio, 0); // SDA
			__gpio_set_value(ma009a_sda.gpio.gpio, 0); // SDA
		}
		__gpio_set_value(ma009a_clk.gpio.gpio, 1); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 1); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 0); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 0); // SCL
	}
	__gpio_set_value(ma009a_cd.gpio.gpio, 0); // C/DB
	__gpio_set_value(ma009a_cd.gpio.gpio, 0); // C/DB
	for(i = 0; i < 8; i++)
	{
		if(data & (0x80 >> i)) 
		{
			__gpio_set_value(ma009a_sda.gpio.gpio, 1); // SDA
			__gpio_set_value(ma009a_sda.gpio.gpio, 1); // SDA
		}
		else 
		{
			__gpio_set_value(ma009a_sda.gpio.gpio, 0); // SDA
			__gpio_set_value(ma009a_sda.gpio.gpio, 0); // SDA
		}
		__gpio_set_value(ma009a_clk.gpio.gpio, 1); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 1); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 0); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 0); // SCL
	}
}


#define IOState 	0x0B<<3
#define IOIntEna 	0x0C<<3
#define IOControl	0x0E<<3

static int iostate = 0x04;

int write_cdpvdp(int comm, int data)
{
	int ret = 0;
	if(strcmp(vdp_name, "line2") == 0)
	{
		if(comm == 1)
		{
				ret = sc16is752_write(IOState, data);
		}
		if(comm == 2)
		{
			if(data == 0x80)
			{
				sc16is7x2_reset();
				printk("Reset SC16IS752-SPI\n");
			}
			if(data == 0x40)
			{
				//sc16is7x2_i2c_reset();
				//printk("Reset SC16IS752-I2C\n");
			}
		}		
	}
	return ret;
}

int read_cdpvdp(int comm)
{
	int ret = 0;
	if(strcmp(vdp_name, "line2") == 0)
	{
		if(comm == 2)
		{
			//ret = sc16is752_io_read();
		}	
	}
	return ret;	
}

void ht9200a_ce_cont(int con)
{
	__gpio_set_value(ht9200a_ce.gpio.gpio, con);
}
EXPORT_SYMBOL(ht9200a_ce_cont);

void ht9200a_dat_cont(int con)
{
	__gpio_set_value(ht9200a_dat.gpio.gpio, con);	
}
EXPORT_SYMBOL(ht9200a_dat_cont);

void ht9200a_clk_cont(int con)
{
	__gpio_set_value(ht9200a_clk.gpio.gpio, con);
}
EXPORT_SYMBOL(ht9200a_clk_cont);

int ht9200a_rdat_cont(void)
{
	return __gpio_get_value(ht9200a_rdat.gpio.gpio);
}
EXPORT_SYMBOL(ht9200a_rdat_cont);

void write_clh700(int comm, int data)
{
	
	int i;

	if(strcmp(vdp_name, "guard") == 0)
	{
		if(comm == 1)
		{
			for(i = 0; i < 8; i++)
			{
				if(data & (1 << i))
				{
					switch(i)
					{
						case 0:
							__gpio_set_value(mute.gpio.gpio, 1);
						break;
#if 0
						case 1:
							//__gpio_set_value(conv_sel_ry.gpio.gpio, 1);
						break;
						case 2:
							__gpio_set_value(tel_ry_cont.gpio.gpio, 1);
						break;
						case 3:
							__gpio_set_value(ht9200a_ce.gpio.gpio, 1);
						break;
						case 4:
							__gpio_set_value(ht9200a_dat.gpio.gpio, 1);
						break;
						case 5:
							__gpio_set_value(ht9200a_clk.gpio.gpio, 1);
						break;
#endif						
					}
				}
				else
				{
					switch(i)
					{
						case 0:
							__gpio_set_value(mute.gpio.gpio, 0);
						break;
#if 0						
						case 1:
							//__gpio_set_value(conv_sel_ry.gpio.gpio, 0);
						break;
						case 2:
							__gpio_set_value(tel_ry_cont.gpio.gpio, 0);
						break;
						case 3:
							__gpio_set_value(ht9200a_ce.gpio.gpio, 0);
						break;
						case 4:
							__gpio_set_value(ht9200a_dat.gpio.gpio, 0);
						break;
						case 5:
							__gpio_set_value(ht9200a_clk.gpio.gpio, 0);
						break;
#endif						
					}
				}		
			}
		}		
	}
	else if(strcmp(vdp_name, "lobby") == 0) 
	{
		if(comm == 1)
		{
			for(i = 0; i < 8; i++)
			{
				if(data & (1 << i))
				{
					switch(i)
					{
						case 0:
							__gpio_set_value(cmos_en.gpio.gpio, 1);
						break;
						case 1:
							__gpio_set_value(door_open1.gpio.gpio, 1);
						break;
						case 2:
							__gpio_set_value(power12v_onoff.gpio.gpio, 1);
						break;
					}
				}
				else
				{
					switch(i)
					{
						case 0:
							__gpio_set_value(cmos_en.gpio.gpio, 0);
						break;
						case 1:
							__gpio_set_value(door_open1.gpio.gpio, 0);
						break;
						case 2:
							__gpio_set_value(power12v_onoff.gpio.gpio, 0);
						break;
					}
				}		
			}
		}		
		
	}
	else
	{
		if(comm == 2)
		{
			for(i = 0; i < 8; i++)
			{
				if(data & (1 << i))
				{
					switch(i)
					{
						case 0:
							__gpio_set_value(ma009a_door1_open_cont.gpio.gpio, 1);
						break;
						case 1:
							__gpio_set_value(ma009a_door2_open_cont.gpio.gpio, 1);
						break;
						case 2:
							__gpio_set_value(ma009a_int_call_out.gpio.gpio, 1);
						break;
						case 3:
							__gpio_set_value(ma009a_relay1_cont.gpio.gpio, 1);
						break;
						case 4:
							__gpio_set_value(ma009a_relay2_cont.gpio.gpio, 1);
						break;
						case 5:
							__gpio_set_value(ma009a_relay3_cont.gpio.gpio, 1);
						break;
						case 6:
							__gpio_set_value(ma009a_talk_sel_a.gpio.gpio, 1);
						break;
						case 7:
							__gpio_set_value(ma009a_talk_sel_b.gpio.gpio, 1);
						break;
					}
				}
				else
				{
					switch(i)
					{
						case 0:
							__gpio_set_value(ma009a_door1_open_cont.gpio.gpio, 0);
						break;
						case 1:
							__gpio_set_value(ma009a_door2_open_cont.gpio.gpio, 0);
						break;
						case 2:
							__gpio_set_value(ma009a_int_call_out.gpio.gpio, 0);
						break;
						case 3:
							__gpio_set_value(ma009a_relay1_cont.gpio.gpio, 0);
						break;
						case 4:
							__gpio_set_value(ma009a_relay2_cont.gpio.gpio, 0);
						break;
						case 5:
							__gpio_set_value(ma009a_relay3_cont.gpio.gpio, 0);
						break;
						case 6:
							__gpio_set_value(ma009a_talk_sel_a.gpio.gpio, 0);
						break;
						case 7:
							__gpio_set_value(ma009a_talk_sel_b.gpio.gpio, 0);
						break;
					}
				}		
			}
		}
		else if(comm == 3)
		{
			for(i = 0; i < 8; i++)
			{
				if(data & (1 << i))
				{
					switch(i)
					{
						case 0:
							__gpio_set_value(ma009a_il34118_power_cont.gpio.gpio, 1);
						break;
						case 1:
							__gpio_set_value(ma009a_talk_on.gpio.gpio, 1);
						break;
						case 2:
							__gpio_set_value(ma009a_fire_power_cont.gpio.gpio, 1);
						break;
						case 3:
							__gpio_set_value(ma009a_ca_power_cont.gpio.gpio, 1);
						break;
						case 4:
							__gpio_set_value(ma009a_chime_cont.gpio.gpio, 1);
						break;
						case 5:
							__gpio_set_value(ma009a_amp_cont.gpio.gpio, 1);
						break;
					}
				}
				else
				{
					switch(i)
					{
						case 0:
							__gpio_set_value(ma009a_il34118_power_cont.gpio.gpio, 0);
						break;
						case 1:
							__gpio_set_value(ma009a_talk_on.gpio.gpio, 0);
						break;
						case 2:
							__gpio_set_value(ma009a_fire_power_cont.gpio.gpio, 0);
						break;
						case 3:
							__gpio_set_value(ma009a_ca_power_cont.gpio.gpio, 0);
						break;
						case 4:
							__gpio_set_value(ma009a_chime_cont.gpio.gpio, 0);
						break;
						case 5:
							__gpio_set_value(ma009a_amp_cont.gpio.gpio, 0);
						break;
					}
				}		
			}
		}	
	}
#if 0
printk("JOON1 GPIO %d\n",  __gpio_get_value(ma009a_bug1.gpio.gpio));
printk("JOON2 GPIO %d\n",  __gpio_get_value(ma009a_bug2.gpio.gpio));
printk("JOON3 GPIO %d\n",  __gpio_get_value(ma009a_bug3.gpio.gpio));
printk("JOON4 GPIO %d\n",  __gpio_get_value(ma009a_bug4.gpio.gpio));
printk("JOON5 GPIO %d\n",  __gpio_get_value(ma009a_bug5.gpio.gpio));
printk("JOON6 GPIO %d\n",  __gpio_get_value(ma009a_fire.gpio.gpio));
printk("JOON7 GPIO %d\n",  __gpio_get_value(ma009a_em.gpio.gpio));
printk("JOON8 GPIO %d\n",  __gpio_get_value(ma009a_gas.gpio.gpio));

printk("JOON11 GPIO %d\n",  __gpio_get_value(ma009a_ca1_call.gpio.gpio));
printk("JOON12 GPIO %d\n",  __gpio_get_value(ma009a_ca2_call.gpio.gpio));
printk("JOON13 GPIO %d\n",  __gpio_get_value(ma009a_ux_gd_call.gpio.gpio));
printk("JOON14 GPIO %d\n",  __gpio_get_value(ma009a_int_open.gpio.gpio));
printk("JOON15 GPIO %d\n",  __gpio_get_value(ma009a_int_hook.gpio.gpio));
printk("JOON16 GPIO %d\n",  __gpio_get_value(ma009a_int_call_in.gpio.gpio));
printk("JOON17 GPIO %d\n",  __gpio_get_value(ma009a_conv_detect.gpio.gpio));

printk("JOON21 GPIO %d\n",  __gpio_get_value(ma009a_door1_open_cont.gpio.gpio));
printk("JOON22 GPIO %d\n",  __gpio_get_value(ma009a_door2_open_cont.gpio.gpio));
printk("JOON23 GPIO %d\n",  __gpio_get_value(ma009a_int_call_out.gpio.gpio));
printk("JOON24 GPIO %d\n",  __gpio_get_value(ma009a_relay1_cont.gpio.gpio));
printk("JOON25 GPIO %d\n",  __gpio_get_value(ma009a_relay2_cont.gpio.gpio));
printk("JOON26 GPIO %d\n",  __gpio_get_value(ma009a_relay3_cont.gpio.gpio));
printk("JOON27 GPIO %d\n",  __gpio_get_value(ma009a_talk_sel_a.gpio.gpio));
printk("JOON28 GPIO %d\n",  __gpio_get_value(ma009a_talk_sel_b.gpio.gpio)); 

printk("JOON31 GPIO %d\n",  __gpio_get_value(ma009a_il34118_power_cont.gpio.gpio));
printk("JOON32 GPIO %d\n",  __gpio_get_value(ma009a_talk_on.gpio.gpio));
printk("JOON33 GPIO %d\n",  __gpio_get_value(ma009a_fire_power_cont.gpio.gpio));
printk("JOON34 GPIO %d\n",  __gpio_get_value(ma009a_ca_power_cont.gpio.gpio));
printk("JOON35 GPIO %d\n",  __gpio_get_value(ma009a_chime_cont.gpio.gpio));
printk("JOON36 GPIO %d\n",  __gpio_get_value(ma009a_amp_cont.gpio.gpio));
#endif

}

int read_clh700(int comm)
{
	int data = 0;
	if(strcmp(vdp_name, "guard") == 0)
	{
		if(comm == 0)
		{
			data |= __gpio_get_value(hook_sw.gpio.gpio) << 0;
			//data |= __gpio_get_value(tel_ring.gpio.gpio) << 1;
		}		
	}
	else if(strcmp(vdp_name, "lobby") == 0) 
	{
		data = 0;
	}
	else
	{
		if(comm == 0)
		{
			data |= __gpio_get_value(ma009a_bug1.gpio.gpio) << 0;
			data |= __gpio_get_value(ma009a_bug2.gpio.gpio) << 1;
			data |= __gpio_get_value(ma009a_bug3.gpio.gpio) << 2;
			data |= __gpio_get_value(ma009a_bug4.gpio.gpio) << 3;
			data |= __gpio_get_value(ma009a_bug5.gpio.gpio) << 4;
			data |= __gpio_get_value(ma009a_fire.gpio.gpio) << 5;
			data |= __gpio_get_value(ma009a_em.gpio.gpio) << 6;
			data |= __gpio_get_value(ma009a_gas.gpio.gpio) << 7;
		}
		else if(comm == 1)
		{
	
			data |= __gpio_get_value(ma009a_ca1_call.gpio.gpio) << 0;
			data |= __gpio_get_value(ma009a_ca2_call.gpio.gpio) << 1;
			data |= __gpio_get_value(ma009a_ux_gd_call.gpio.gpio) << 2;
			data |= __gpio_get_value(ma009a_int_open.gpio.gpio) << 3;
			data |= __gpio_get_value(ma009a_int_hook.gpio.gpio) << 4;
			data |= __gpio_get_value(ma009a_int_call_in.gpio.gpio) << 5;
			data |= __gpio_get_value(ma009a_conv_detect.gpio.gpio) << 6;
		}
	}

	
	return data;	
}

void write_cip700(int comm, int data)
{
	int i;
	u8 tmp;

	if(comm == 2)
	{
		for(i = 0; i < 8; i++)
		{
			if(data & (1 << i))
			{
				switch(i)
				{
					case 0:
						//__gpio_set_value(PAD_GPIOB(28), 1); //TALK_ON
						//tmp = hc574readb();
						//hc574outb(tmp|TALK_ON);						
					break;
					case 1:
						//tmp = hc574readb();
						//hc574outb(tmp|IL34118_POWER_CONT);						
					break;
					case 2:
						//__gpio_set_value(PAD_GPIOC(10), 1); //CHIME_CONT
						//tmp = hc574readb();
						//hc574outb(tmp|CHIME_CONT);						
					break;
					case 5:
						//tmp = hc574readb();
						//hc574outb(tmp|CA_CONV_CONT);						
					break;
					case 6:
						//__gpio_set_value(PAD_GPIOB(30), 1); //CA_RECALL_CONT
						//tmp = hc574readb();
						//hc574outb(tmp|CA_RECALL_CONT);						
					break;
					case 7: 
						//__gpio_set_value(PAD_GPIOC(17), 1); //CA_POWER_CONT
						//tmp = hc574readb();
						//hc574outb(tmp|CA_POWER_CONT);						
					break;
				}
			}
			else
			{
				switch(i)
				{
					case 0:
						//__gpio_set_value(PAD_GPIOB(28), 0); //TALK_ON
						//tmp = hc574readb();
						//tmp &= ~TALK_ON;
						//hc574outb(tmp);						
					break;
					case 1:
						//__gpio_set_value(PAD_GPIOC(10), 0); //CHIME_CONT
						//tmp = hc574readb();
						//tmp &= ~IL34118_POWER_CONT;
						//hc574outb(tmp);						
					break;
					case 2:
						//__gpio_set_value(PAD_GPIOC(10), 0); //CHIME_CONT
						//tmp = hc574readb();
						//tmp &= ~CHIME_CONT;
						//hc574outb(tmp);						
					break;
					case 5:
						//__gpio_set_value(PAD_GPIOB(30), 0); //CA_RECALL_CONT
						//tmp = hc574readb();
						//tmp &= ~CA_CONV_CONT;
						//hc574outb(tmp);						
					break;
					case 6:
						//__gpio_set_value(PAD_GPIOB(30), 0); //CA_RECALL_CONT
						//tmp = hc574readb();
						//tmp &= ~CA_RECALL_CONT;
						//hc574outb(tmp);						
					break;
					case 7: 
						//__gpio_set_value(PAD_GPIOC(17), 0); //CA_POWER_CONT
						//tmp = hc574readb();
						//tmp &= ~CA_POWER_CONT;
						//hc574outb(tmp);						
					break;
				}
			}		
		}
	}
}


int read_cip700(int comm)
{
	int data = 0;

	if(comm == 0)
	{
		//if(__gpio_get_value(PAD_GPIOC(17))) //CA_CALL
		{
			data |= 1<<0;
		}
		//if(alive_2_status) //EM
		{
			data |= 1<<1;
		}
		//if(alive_1_status) //FIRE
		{
			data |= 1<<2;
		}
		//if(__gpio_get_value(PAD_GPIOB(27))) //BURG1
		{
			data |= 1<<3;
		}
		//if(__gpio_get_value(PAD_GPIOB(28))) //BURG2
		{
			data |= 1<<4;
		}
		//if(__gpio_get_value(PAD_GPIOB(30))) //BURG3
		{
			data |= 1<<5;
		}
		//if(__gpio_get_value(PAD_GPIOA(26))) //BURG4
		{
			data |= 1<<6;
		}
		//if(__gpio_get_value(PAD_GPIOA(27))) //BURG5
		{
			data |= 1<<7;
		}
	}
	if(comm == 1)
	{
		//if(__gpio_get_value(PAD_GPIOC(14))) //GAS
		{
			data |= 1<<0;
		}
	}	
	
	return data;	
}


void write_gbm700(int comm, int data)
{

}

int read_gbm700(int comm)
{
	int data = 0;

	if(comm == 0)
	{
		//if(__gpio_get_value(PAD_GPIOC(10)))  //GAS
		{
			data |= 1<<0;
		}
		//if(__gpio_get_value(S3C64XX_GPQ(3))) //BURG4
		//{
		//	data |= 1<<1;
		//}
		//if(__gpio_get_value(PAD_GPIOB(30))) //MS
		{
			data |= 1<<2;
		}
	}
	
	
	return data;	
}


void write_jns70mn(int comm, int data)
{
	int i;

	if(comm == 1)
	{
		for(i = 0; i < 8; i++)
		{
			if(data & (1 << i))
			{
				switch(i)
				{
					case 0:
						//__gpio_set_value(PAD_GPIOC(17), 1); // TEL_RING:HOLD_CNT
					break;
					case 1:
						//__gpio_set_value(PAD_GPIOB(30), 1); // HT9200_CE:RST
					break;
					case 2:
						//__gpio_set_value(PAD_GPIOB(28), 1); // CONV_SEL_RY:TALK_CNT
					break;
					case 3:
						//__gpio_set_value(PAD_GPIOB(27), 1); // MUTE:RX_MUTE
					break;
				}
			}
			else
			{
				switch(i)
				{
					case 0:
						//__gpio_set_value(PAD_GPIOC(17), 0); // TEL_RING:HOLD_CNT
					break;
					case 1:
						//__gpio_set_value(PAD_GPIOB(30), 0); // HT9200_CE:RST
					break;
					case 2:
						//__gpio_set_value(PAD_GPIOB(28), 0); // CONV_SEL_RY:TALK_CNT
					break;
					case 3:
						//__gpio_set_value(PAD_GPIOB(27), 0); // MUTE:RX_MUTE
					break;
				}
			}		
		}
	}
}

int read_jns70mn(int comm)
{
	int data = 0;

	if(comm == 0)
	{
		//if(__gpio_get_value(PAD_GPIOC(14)))  //HOOK_SW
		{
			data |= 1<<0;
		}
		//if(__gpio_get_value(S3C64XX_GPQ(8))) //DATA_ERR
		//{
		//	data |= 1<<1;
		//}
	}	
	
	return data;	
}


void write_cns70mn(int comm, int data)
{
	int i;

	if(comm == 1)
	{
		for(i = 0; i < 8; i++)
		{
			if(data & (1 << i))
			{
				switch(i)
				{
					case 0:
						//__gpio_set_value(PAD_GPIOB(27), 1); //2, RX_AUDIO_MUTE
					break;
					case 1:
						//__gpio_set_value(PAD_GPIOB(28), 1); //3, TALK_POWER_CNT
					break;
					case 2:
						//__gpio_set_value(PAD_GPIOC(17), 1); //6, TALK_RELAY
					break;
					case 3:
						//__gpio_set_value(PAD_GPIOB(30), 1); //7, SUB_RESET_CONT
					break;
					case 4:
						//__gpio_set_value(PAD_GPIOC(10), 1); //8, RS-485_CONT
					break;
					//case 5:
					//	__gpio_set_value(S3C64XX_GPQ(3), 1); //9, DATA_RELAY_CONT
					//break;
				}
			}
			else
			{
				switch(i)
				{
					case 0:
						//__gpio_set_value(PAD_GPIOB(27), 0); //2, RX_AUDIO_MUTE
					break;
					case 1:
						//__gpio_set_value(PAD_GPIOB(28), 0); //3, TALK_POWER_CNT
					break;
					case 2:
						//__gpio_set_value(PAD_GPIOC(17), 0); //6, TALK_RELAY
					break;
					case 3:
						//__gpio_set_value(PAD_GPIOB(30), 0); //7, SUB_RESET_CONT
					break;
					case 4:
						//__gpio_set_value(PAD_GPIOC(10), 0); //8, RS-485_CONT
					break;
					//case 5:
					//	__gpio_set_value(S3C64XX_GPQ(3), 0); //9, DATA_RELAY_CONT
					//break;
				}
			}		
		}
	}
}

int read_cns70mn(int comm)
{
	int data = 0;

	if(comm == 0)
	{
		//if(alive_2_status) //1, HOOK_SW
		{
			data |= 1<<0;
		}
		//if(__gpio_get_value(S3C64XX_GPQ(8))) //4, 485-LINE_A_CHECK
		//{
		//	data |= 1<<1;
		//}
		//if(__gpio_get_value(PAD_GPIOC(14))) //5, 485-LINE_B_CHECK
		{
			data |= 1<<2;
		}
	}	
	
	return data;	
}


void write_cgw500(int comm, int data)
{
	int i;

	if(comm == 1)
	{
		for(i = 0; i < 8; i++)
		{
			if(data & (1 << i))
			{
				switch(i)
				{
					case 0:
						//__gpio_set_value(PAD_GPIOC(14), 1); 
					break;
					case 1:
						//__gpio_set_value(PAD_GPIOC(17), 1); 
					break;
					case 2:
						//__gpio_set_value(PAD_GPIOB(30), 1); 
					break;
				}
			}
			else
			{
				switch(i)
				{
					case 0:
						//__gpio_set_value(PAD_GPIOC(14), 0); 
					break;
					case 1:
						//__gpio_set_value(PAD_GPIOC(17), 0); 
					break;
					case 2:
						//__gpio_set_value(PAD_GPIOB(30), 0); 
					break;
				}
			}		
		}
	}
}

int read_cgw500(int comm)
{
	int data = 0;

	if(comm == 0)
	{
		//if(__gpio_get_value(PAD_GPIOB(27)))
		{
			data |= 1<<0;
		}
		//if(__gpio_get_value(PAD_GPIOB(28)))
		{
			data |= 1<<1;
		}
		//if(__gpio_get_value(S3C64XX_GPQ(8)))
		//{
		//	data |= 1<<2;
		//}
	}	
	
	return data;	
}

void write_guard(int comm, int data)
{
	int i;

	if(comm == 1)
	{
		for(i = 0; i < 8; i++)
		{
			if(data & (1 << i))
			{
				switch(i)
				{
					case 0:
						//__gpio_set_value(PAD_GPIOB(27), 1); //MUTE
					break;
					case 1:
						//__gpio_set_value(PAD_GPIOB(28), 1); //CONV_SEL_RY
					break;
					case 2:
						//__gpio_set_value(S3C64XX_GPQ(8), 1); //TEL_RY_CONT
					break;
					case 3:
						//__gpio_set_value(PAD_GPIOB(30), 1); //HT9200_CE
					break;
					case 4:
						//__gpio_set_value(PAD_GPIOC(10), 1); //HT9200_DAT
					break;
					//case 5:
					//	__gpio_set_value(S3C64XX_GPQ(3), 1); //HT9200_CLK
					//break;
					case 6:
						//__gpio_set_value(S3C64XX_GPO(12), 1); //CCD_12V_ON
					break;
					case 7: 
						//__gpio_set_value(PAD_GPIOL(2), 1);
						//soc_gpio_set_io_dir(PAD_GPIOL(2), PAD_MODE_OUT); //door open
						be_door_open = 1;
					break;
				}
			}
			else
			{
				switch(i)
				{
					case 0:
						//__gpio_set_value(PAD_GPIOB(27), 0); //MUTE
					break;
					case 1:
						//__gpio_set_value(PAD_GPIOB(28), 0); //CONV_SEL_RY
					break;
					case 2:
						//__gpio_set_value(S3C64XX_GPQ(8), 0); //TEL_RY_CONT
					break;
					case 3:
						//__gpio_set_value(PAD_GPIOB(30), 0); //HT9200_CE
					break;
					case 4:
						//__gpio_set_value(PAD_GPIOC(10), 0); //HT9200_DAT
					break;
					//case 5:
					//	__gpio_set_value(S3C64XX_GPQ(3), 0); //HT9200_CLK
					//break;
					case 6:
						//__gpio_set_value(S3C64XX_GPO(12), 0); //CCD_12V_ON
					break;
					case 7:
						if(be_door_open)
						{
							//__gpio_set_value(PAD_GPIOL(2), 0);
							//soc_gpio_set_io_dir(PAD_GPIOL(2), PAD_MODE_OUT); //door open
						}
					break;
				}
			}		
		}
	}
}

int read_guard(int comm)
{
	int data = 0;

	if(comm == 0)
	{
		//if(__gpio_get_value(PAD_GPIOC(14)))  //HOOK_SW
		{
			data |= 1;
		}
		//if(__gpio_get_value(S3C64XX_GPO(8))) //MOT_TRACK
		{
			data |= 0x2;
		}
		return data;
	}
	
	return 0;	
}

int read_ma009a(int comm)
{
	int i, data;
	
	data = 0;
	
	__gpio_set_value(ma009a_cd.gpio.gpio, 1); // C/DB
	__gpio_set_value(ma009a_cd.gpio.gpio, 1); // C/DB
	sw_gpio_set(&ma009a_sda.gpio, 1);// MISO1 --> J20 2 --> SDA
	for(i = 0; i < 8; i++)
	{
		if(comm & (0x80 >> i)) 
		{
			__gpio_set_value(ma009a_sda.gpio.gpio, 1); // SDA
			__gpio_set_value(ma009a_sda.gpio.gpio, 1); // SDA
		}
		else 
		{
			__gpio_set_value(ma009a_sda.gpio.gpio, 0); // SDA
			__gpio_set_value(ma009a_sda.gpio.gpio, 0); // SDA
		}
		__gpio_set_value(ma009a_clk.gpio.gpio, 1); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 1); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 0); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 0); // SCL
	}
	
	__gpio_set_value(ma009a_cd.gpio.gpio, 0); // C/DB
	__gpio_set_value(ma009a_cd.gpio.gpio, 0); // C/DB
	__gpio_set_value(ma009a_sda.gpio.gpio, 0); // SDA
	__gpio_set_value(ma009a_sda.gpio.gpio, 0); // SDA

	sw_gpio_set(&ma009a_sda.gpio, 0);// MISO1 --> J20 2 --> SDA
	for(i = 0; i < 8; i++)
	{
		__gpio_set_value(ma009a_clk.gpio.gpio, 1); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 1); // SCL
		if(__gpio_get_value(ma009a_sda.gpio.gpio) == 1) data |= 0x80 >> i;
		__gpio_set_value(ma009a_clk.gpio.gpio, 0); // SCL
		__gpio_set_value(ma009a_clk.gpio.gpio, 0); // SCL
	}
		
	return data;	
}

void ring_timer_work(void)
{

	static int pre_status;
	
    pre_status = last_state;
    last_state = __gpio_get_value(tel_ring.gpio.gpio) ? RING_ON_S : RING_OFF_S;
    
    if (pre_status != last_state)
    {
    	ringdata = last_state;
		wake_up_interruptible(&oki_ring_tx);
	}
	schedule_delayed_work(&oki_ring_work, (HZ/50));

}

static int nxp_ma009a_open(struct inode *inode, struct file *file)
{
	return 0;	
}

static int nxp_ma009a_release(struct inode *inode, struct file *file)
{
	return 0;	
}

static int nxp_guard_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int nxp_guard_release(struct inode *inode, struct file *file)
{
 	
    return 0;
}

static int nxp_oki_ring_open(struct inode *inode, struct file *file)
{
	ringdata = 0;
	
    return 0;
}


static int nxp_oki_ring_release(struct inode *inode, struct file *file)
{ 	
    return 0;
}

static ssize_t nxp_oki_ring_read(struct file *fp, char *buf, size_t count, loff_t *ppos)
{
	int ret;
	
	if(ringdata == 0) interruptible_sleep_on(&oki_ring_tx);

    ret = copy_to_user((void *)buf, (const void *)&ringdata, 4);
	ringdata = 0;
	
    return count;
}


static int nxp_clh700_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int nxp_clh700_release(struct inode *inode, struct file *file)
{
 	
    return 0;
}

static int nxp_cip700_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int nxp_cip700_release(struct inode *inode, struct file *file)
{
 	
    return 0;
}

static int nxp_gbm700_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int nxp_gbm700_release(struct inode *inode, struct file *file)
{
 	
    return 0;
}

static int nxp_jns70mn_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int nxp_jns70mn_release(struct inode *inode, struct file *file)
{
 	
    return 0;
}

static int nxp_cns70mn_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int nxp_cns70mn_release(struct inode *inode, struct file *file)
{
 	
    return 0;
}

static int nxp_cgw500_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int nxp_cgw500_release(struct inode *inode, struct file *file)
{
    return 0;
}

#define RD_P0	0x3d // port0  : read only operation
#define RD_P1	0x3e // port1  : read & write operation
#define WR_P1	0x0e 
#define CT_P1	0x02 // default all input : xxxxxx10  0 -> port 3-0 bit, 1 -> port 7-4 bit, 0->input, 1->output
#define WR_P2	0x0f // port2  : write only operation


static long nxp_ma009a_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{

	int ret = -1;
    ioctl_ma009a_info ctrl_info;  
     
    switch(cmd)
    {
    	case IOCTL_MA009A_RD:    		
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_ma009a_info));

			if(be_ma009a == 1) ctrl_info.data = read_ma009a(ctrl_info.addr);
			else ctrl_info.data = 0xffffffff;
    		//printk("MA009A RD: addr[%02lx]->[%02lx]\n", ctrl_info.addr, ctrl_info.data);
        	ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_ma009a_info));
        	if(be_ma009a == 1) ret = 0;
        	else ret = -1;
        	    
    	break;
    	case IOCTL_MA009A_WR:
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_ma009a_info));
    		
			if(be_ma009a == 1) write_ma009a(ctrl_info.addr, ctrl_info.data);
        	if(be_ma009a == 1) ret = 0;
        	else ret = -1;
    		
    		//printk("MA009A WR: addr[%02lx]<-[%02lx]\n", ctrl_info.addr, ctrl_info.data);
    	break;
    	case IOCTL_MA009A_INIT:
    		be_ma009a = 1;
			reset_ma009a();
			write_ma009a(0xfd, 0);
			if(be_ma009a == 1)
			{
				write_ma009a(CT_P1, p1cfg);// 7-4 bit output, 3-0 bit input
				write_ma009a(WR_P1, p1set);	
				write_ma009a(WR_P2, p2set);
				printk("CDP MA009A Init!\n");
#if 0				
				if(strcmp(vdp_name, "line2") == 0)
				{
					sc16is7x2_reset();
					printk("SPI Uart Init!\n");
				}
#endif				
				ret = 0;
			}
			else ret = -1;
    	break;
	}
    return ret;
}

static int nxp_cdpvdp_open(struct inode *inode, struct file *file)
{
	if(strcmp(vdp_name, "line2") == 0) 
	{
		if(sc16is7x2_check()) return 0;
		else return -1;
	}
	return 0;
}

static int nxp_cdpvdp_release(struct inode *inode, struct file *file)
{
	return 0;	
}


static long nxp_cdpvdp_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	int ret, ret1;
    ioctl_cdpvdp_info ctrl_info;  

    switch(cmd)
    {
    	case IOCTL_CDPVDP_RD:    		
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_cdpvdp_info));

			ret1 = ctrl_info.data = read_cdpvdp(ctrl_info.addr);
    		//printk("CDPVDP RD: addr[%02lx]->[%02lx]\n", ctrl_info.addr, ctrl_info.data);
        	ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_cdpvdp_info));    
    	break;
    	case IOCTL_CDPVDP_WR:
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_cdpvdp_info));
    		
			ret1 = write_cdpvdp(ctrl_info.addr, ctrl_info.data);
    		
    		//printk("CDPVDP WR: addr[%02lx]<-[%02lx]\n", ctrl_info.addr, ctrl_info.data);
    	break;
	}

    return ret1;

}

static long nxp_guard_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{

	int ret;
    ioctl_guard_info ctrl_info;  

    switch(cmd)
    {
    	case IOCTL_GUARD_RD:    		
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_guard_info));

			ctrl_info.data = read_guard(ctrl_info.addr);
    		//printk("GUARD RD: addr[%02lx]->[%02lx]\n", ctrl_info.addr, ctrl_info.data);
        	ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_guard_info));    
    	break;
    	case IOCTL_GUARD_WR:
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_guard_info));
    		
			write_guard(ctrl_info.addr, ctrl_info.data);
    		
    		//printk("GUARD WR: addr[%02lx]<-[%02lx]\n", ctrl_info.addr, ctrl_info.data);
    	break;
	}
    return 0;

}

static long nxp_clh700_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{

	int ret;
    ioctl_clh700_info ctrl_info;  

    switch(cmd)
    {
    	case IOCTL_CLH700_RD:    		
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_clh700_info));

			ctrl_info.data = read_clh700(ctrl_info.addr);
    		//printk("JOON CLH RD: addr[%02lx]->[%02lx]\n", ctrl_info.addr, ctrl_info.data);
        	ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_clh700_info));    
    	break;
    	case IOCTL_CLH700_WR:
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_clh700_info));
    		
			write_clh700(ctrl_info.addr, ctrl_info.data);
    		
    		//printk("JOON CLH WR: addr[%02lx]<-[%02lx]\n", ctrl_info.addr, ctrl_info.data);
    	break;
	}
    return 0;

}


static long nxp_cip700_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{

	int ret;
    ioctl_cip700_info ctrl_info;  

    switch(cmd)
    {
    	case IOCTL_CIP700_RD:    		
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_cip700_info));

			ctrl_info.data = read_cip700(ctrl_info.addr);
//    		printk("CIP RD: addr[%02lx]->[%02lx]\n", ctrl_info.addr, ctrl_info.data);
        	ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_cip700_info));    
    	break;
    	case IOCTL_CIP700_WR:
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_cip700_info));
    		
			write_cip700(ctrl_info.addr, ctrl_info.data);
    		
//    		printk("CIP WR: addr[%02lx]<-[%02lx]\n", ctrl_info.addr, ctrl_info.data);
    	break;
	}
    return 0;

}

static long nxp_gbm700_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{

	int ret;
    ioctl_gbm700_info ctrl_info;  

    switch(cmd)
    {
    	case IOCTL_GBM700_RD:    		
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_gbm700_info));

			ctrl_info.data = read_gbm700(ctrl_info.addr);
    		//printk("GUARD RD: addr[%02lx]->[%02lx]\n", ctrl_info.addr, ctrl_info.data);
        	ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_gbm700_info));    
    	break;
    	case IOCTL_GBM700_WR:
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_gbm700_info));
    		
			write_gbm700(ctrl_info.addr, ctrl_info.data);
    		
    		//printk("GUARD WR: addr[%02lx]<-[%02lx]\n", ctrl_info.addr, ctrl_info.data);
    	break;
	}
    return 0;

}

static long nxp_jns70mn_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{

	int ret;
    ioctl_jns70mn_info ctrl_info;  

    switch(cmd)
    {
    	case IOCTL_JNS70MN_RD:    		
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_jns70mn_info));

			ctrl_info.data = read_jns70mn(ctrl_info.addr);
    		//printk("GUARD RD: addr[%02lx]->[%02lx]\n", ctrl_info.addr, ctrl_info.data);
        	ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_jns70mn_info));    
    	break;
    	case IOCTL_JNS70MN_WR:
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_jns70mn_info));
    		
			write_jns70mn(ctrl_info.addr, ctrl_info.data);
    		
    		//printk("GUARD WR: addr[%02lx]<-[%02lx]\n", ctrl_info.addr, ctrl_info.data);
    	break;
	}
    return 0;

}

static long nxp_cns70mn_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{

	int ret;
    ioctl_cns70mn_info ctrl_info;  

    switch(cmd)
    {
    	case IOCTL_JNS70MN_RD:    		
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_cns70mn_info));

			ctrl_info.data = read_cns70mn(ctrl_info.addr);
    		//printk("GUARD RD: addr[%02lx]->[%02lx]\n", ctrl_info.addr, ctrl_info.data);
        	ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_cns70mn_info));    
    	break;
    	case IOCTL_JNS70MN_WR:
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_cns70mn_info));
    		
			write_cns70mn(ctrl_info.addr, ctrl_info.data);
    		
    		//printk("GUARD WR: addr[%02lx]<-[%02lx]\n", ctrl_info.addr, ctrl_info.data);
    	break;
	}
    return 0;

}

static long nxp_cgw500_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{

	int ret;
    ioctl_cgw500_info ctrl_info;  

    switch(cmd)
    {
    	case IOCTL_CGW500_RD:    		
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_cgw500_info));

			ctrl_info.data = read_cgw500(ctrl_info.addr);
    		//printk("GUARD RD: addr[%02lx]->[%02lx]\n", ctrl_info.addr, ctrl_info.data);
        	ret = copy_to_user((void *)arg, (const void *)&ctrl_info, sizeof(ioctl_cgw500_info));    
    	break;
    	case IOCTL_CGW500_WR:
    		ret = copy_from_user((void *)&ctrl_info, (const void *)arg, sizeof(ioctl_cgw500_info));
    		
			write_cgw500(ctrl_info.addr, ctrl_info.data);
    		
    		//printk("GUARD WR: addr[%02lx]<-[%02lx]\n", ctrl_info.addr, ctrl_info.data);
    	break;
	}
    return 0;

}

static struct file_operations nxp_ma009a_fops = { 
	owner:      THIS_MODULE,
	open:    	nxp_ma009a_open,
	unlocked_ioctl:		nxp_ma009a_ioctl,
	release:    nxp_ma009a_release,
};

static struct miscdevice nxp_ma009a_dev = {
	minor:		140,
	name:		"a20-ma009a",
	fops:		&nxp_ma009a_fops,
};

static struct file_operations nxp_cdpvdp_fops = { 
	owner:      THIS_MODULE,
	open:    	nxp_cdpvdp_open,
	unlocked_ioctl:		nxp_cdpvdp_ioctl,
	release:    nxp_cdpvdp_release,
};

static struct miscdevice nxp_cdpvdp_dev = {
	minor:		141,
	name:		"a20-cdpvdp",
	fops:		&nxp_cdpvdp_fops,
};


static struct file_operations nxp_guard_fops = { 
	owner:      THIS_MODULE,
	open:    	nxp_guard_open,
	unlocked_ioctl:		nxp_guard_ioctl,
	release:    nxp_guard_release,
};

static struct miscdevice nxp_guard_dev = {
	minor:		142,
	name:		"a20-guard",
	fops:		&nxp_guard_fops,
};

static struct file_operations nxp_oki_ring_fops = {
	owner:      THIS_MODULE,
	open:    	nxp_oki_ring_open,
	read:		nxp_oki_ring_read,
	release:    nxp_oki_ring_release,
};


static struct miscdevice nxp_oki_ring_dev = {
	minor:		143,
	name:		"a20-oki-ring",
	fops:		&nxp_oki_ring_fops,
};


static struct file_operations nxp_clh700_fops = { 
	owner:      THIS_MODULE,
	open:    	nxp_clh700_open,
	unlocked_ioctl:		nxp_clh700_ioctl,
	release:    nxp_clh700_release,
};

static struct miscdevice nxp_clh700_dev = {
	minor:		144,
	name:		"a20-clh700",
	fops:		&nxp_clh700_fops,
};


static struct file_operations nxp_cip700_fops = { 
	owner:      THIS_MODULE,
	open:    	nxp_cip700_open,
	unlocked_ioctl:		nxp_cip700_ioctl,
	release:    nxp_cip700_release,
};

static struct miscdevice nxp_cip700_dev = {
	minor:		145,
	name:		"a20-cip700",
	fops:		&nxp_cip700_fops,
};

static struct file_operations nxp_gbm700_fops = { 
	owner:      THIS_MODULE,
	open:    	nxp_gbm700_open,
	unlocked_ioctl:		nxp_gbm700_ioctl,
	release:    nxp_gbm700_release,
};

static struct miscdevice nxp_gbm700_dev = {
	minor:		146,
	name:		"a20-gbm700",
	fops:		&nxp_gbm700_fops,
};

static struct file_operations nxp_jns70mn_fops = { 
	owner:      THIS_MODULE,
	open:    	nxp_jns70mn_open,
	unlocked_ioctl:		nxp_jns70mn_ioctl,
	release:    nxp_jns70mn_release,
};

static struct miscdevice nxp_jns70mn_dev = {
	minor:		147,
	name:		"a20-jns70mn",
	fops:		&nxp_jns70mn_fops,
};

static struct file_operations nxp_cns70mn_fops = { 
	owner:      THIS_MODULE,
	open:    	nxp_cns70mn_open,
	unlocked_ioctl:		nxp_cns70mn_ioctl,
	release:    nxp_cns70mn_release,
};

static struct miscdevice nxp_cns70mn_dev = {
	minor:		149,
	name:		"a20-cns70mn",
	fops:		&nxp_cns70mn_fops,
};

static struct file_operations nxp_cgw500_fops = { 
	owner:      THIS_MODULE,
	open:    	nxp_cgw500_open,
	unlocked_ioctl:		nxp_cgw500_ioctl,
	release:    nxp_cgw500_release,
};

static struct miscdevice nxp_cgw500_dev = {
	minor:		150,
	name:		"a20-cgw500",
	fops:		&nxp_cgw500_fops,
};

int nxp_ma009a_probe(struct platform_device *pdev)
{	
	int err;
	be_ma009a = 1;
	iostate = 0x04;

	reset_ma009a();
	write_ma009a(0xfd, 0);

	if(be_ma009a == 1)
	{
		write_ma009a(CT_P1, p1cfg);// 7-4 bit output, 3-0 bit input
		write_ma009a(WR_P1, p1set);	
		write_ma009a(WR_P2, p2set);
		printk("CDP MA009A Init!\n");
	}			

	if(be_ma009a == 1)
	{
		err = misc_register(&nxp_ma009a_dev);
		if(err) {
			printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n", 104, err);
		}
	}

	err = misc_register(&nxp_cdpvdp_dev);
	if(err) {
		printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n", 106, err);
	}
		
	err = misc_register(&nxp_guard_dev);
	if(err) {
		printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n", 108, err);
	}

	err = misc_register(&nxp_oki_ring_dev);
	if (err) {
		printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n", 135, err);
	}

	err = misc_register(&nxp_clh700_dev);
	if (err) {
		printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n", 135, err);
	}

	err = misc_register(&nxp_cip700_dev);
	if (err) {
		printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n", 135, err);
	}

	err = misc_register(&nxp_gbm700_dev);
	if (err) {
		printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n", 135, err);
	}

	err = misc_register(&nxp_jns70mn_dev);
	if (err) {
		printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n", 135, err);
	}

	err = misc_register(&nxp_cns70mn_dev);
	if (err) {
		printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n", 135, err);
	}

	err = misc_register(&nxp_cgw500_dev);
	if (err) {
		printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n", 135, err);
	}

	return err;
	
}

static int nxp_ma009a_suspend(struct platform_device *dev, pm_message_t state)
{
	return 0;
}

static int nxp_ma009a_resume(struct platform_device *pdev)
{
	return 0;
}

static int nxp_ma009a_remove(struct platform_device *dev)
{
	if(be_ma009a == 1)
	{
		misc_deregister(&nxp_ma009a_dev);
	}
	misc_deregister(&nxp_guard_dev);
	misc_deregister(&nxp_oki_ring_dev);	
	misc_deregister(&nxp_clh700_dev);
	misc_deregister(&nxp_cip700_dev);
	misc_deregister(&nxp_gbm700_dev);
	misc_deregister(&nxp_jns70mn_dev);
	misc_deregister(&nxp_cns70mn_dev);
	misc_deregister(&nxp_cgw500_dev);
	return 0;
}

static struct platform_device nxp_ma009a_device = {
	.name           	= "a20-ma009a",
	.id             	= -1,//??
};

static struct platform_driver nxp_ma009a_driver = {
       .probe          = nxp_ma009a_probe,
       .remove         = nxp_ma009a_remove,
       .suspend        = nxp_ma009a_suspend,
       .resume         = nxp_ma009a_resume,
       .driver		= {
		    .owner	= THIS_MODULE,
		    .name	= "a20-ma009a",
	}, 
};

int __init nxp_ma009a_init(void)
{
 	unsigned int ret;
    script_item_u val;
    script_item_value_type_e type;
    char *vdp;

	if(0 > ma009a_sys_config("ma009a_para", "cd", &ma009a_cd)) return -1;
	if(0 > ma009a_sys_config("ma009a_para", "sda", &ma009a_sda)) return -1;
	if(0 > ma009a_sys_config("ma009a_para", "clk", &ma009a_clk)) return -1;

	ret = ma009a_sys_config("ma009a_para", "bug1", &ma009a_bug1);
	ret = ma009a_sys_config("ma009a_para", "bug2", &ma009a_bug2);
	ret = ma009a_sys_config("ma009a_para", "bug3", &ma009a_bug3);
	ret = ma009a_sys_config("ma009a_para", "bug4", &ma009a_bug4);
	ret = ma009a_sys_config("ma009a_para", "bug5", &ma009a_bug5);
	ret = ma009a_sys_config("ma009a_para", "fire", &ma009a_fire);
	ret = ma009a_sys_config("ma009a_para", "em", &ma009a_em);
	ret = ma009a_sys_config("ma009a_para", "gas", &ma009a_gas);
	
	ret = ma009a_sys_config("ma009a_para", "ca1_call", &ma009a_ca1_call);
	ret = ma009a_sys_config("ma009a_para", "ca2_call", &ma009a_ca2_call);
	ret = ma009a_sys_config("ma009a_para", "ux_gd_call", &ma009a_ux_gd_call);
	ret = ma009a_sys_config("ma009a_para", "int_open", &ma009a_int_open);
	ret = ma009a_sys_config("ma009a_para", "int_hook", &ma009a_int_hook);
	ret = ma009a_sys_config("ma009a_para", "int_call_in", &ma009a_int_call_in);
	ret = ma009a_sys_config("ma009a_para", "conv_detect", &ma009a_conv_detect);
	
	ret = ma009a_sys_config("ma009a_para", "door1_open_cont", &ma009a_door1_open_cont);
	ret = ma009a_sys_config("ma009a_para", "door2_open_cont", &ma009a_door2_open_cont);
	ret = ma009a_sys_config("ma009a_para", "int_call_out", &ma009a_int_call_out);
	ret = ma009a_sys_config("ma009a_para", "relay1_cont", &ma009a_relay1_cont);
	ret = ma009a_sys_config("ma009a_para", "relay2_cont", &ma009a_relay2_cont);
	ret = ma009a_sys_config("ma009a_para", "relay3_cont", &ma009a_relay3_cont);
	ret = ma009a_sys_config("ma009a_para", "talk_sel_a", &ma009a_talk_sel_a);
	ret = ma009a_sys_config("ma009a_para", "talk_sel_b", &ma009a_talk_sel_b);
	
	ret = ma009a_sys_config("ma009a_para", "il34118_power_cont", &ma009a_il34118_power_cont);
	ret = ma009a_sys_config("ma009a_para", "talk_on", &ma009a_talk_on);
	ret = ma009a_sys_config("ma009a_para", "fire_power_cont", &ma009a_fire_power_cont);
	ret = ma009a_sys_config("ma009a_para", "ca_power_cont", &ma009a_ca_power_cont);
	ret = ma009a_sys_config("ma009a_para", "chime_cont", &ma009a_chime_cont);
	ret = ma009a_sys_config("ma009a_para", "amp_cont", &ma009a_amp_cont);
// guard	
	ret = ma009a_sys_config("ma009a_para", "mute", &mute);
	ret = ma009a_sys_config("ma009a_para", "ht9200a_rdat", &ht9200a_rdat);
	ret = ma009a_sys_config("ma009a_para", "tel_ry_cont", &tel_ry_cont);
	ret = ma009a_sys_config("ma009a_para", "hook_sw", &hook_sw);
	ret = ma009a_sys_config("ma009a_para", "tel_ring", &tel_ring);
	ret = ma009a_sys_config("ma009a_para", "ht9200a_ce", &ht9200a_ce);
	ret = ma009a_sys_config("ma009a_para", "ht9200a_dat", &ht9200a_dat);
	ret = ma009a_sys_config("ma009a_para", "ht9200a_clk", &ht9200a_clk);
//lobby
	ret = ma009a_sys_config("ma009a_para", "cmos_en", &cmos_en);
	ret = ma009a_sys_config("ma009a_para", "door_open1", &door_open1);
	ret = ma009a_sys_config("ma009a_para", "power12v_onoff", &power12v_onoff);

    type = script_get_item("board", "name", &val);
    if (type != SCIRPT_ITEM_VALUE_TYPE_STR) {
		printk(KERN_ERR "A20 MA009A platform device register failed!\n");
        return -1;
    }
    sprintf(vdp_str, "%s", val.str);
    vdp_name = strstr(vdp_str, "line");
    if(vdp_name == NULL) 
    {
    	strcpy(vdp_name_null, "none");
    	vdp_name = vdp_name_null;
    }
    else goto printvdp;
    	
    vdp_name = strstr(vdp_str, "guard");
    if(vdp_name == NULL) 
    {
    	strcpy(vdp_name_null, "none");
    	vdp_name = vdp_name_null;
    }
    else 
    {
	    last_state = __gpio_get_value(tel_ring.gpio.gpio) ? RING_ON_S : RING_OFF_S;
		init_waitqueue_head(&oki_ring_tx);
		INIT_DELAYED_WORK_DEFERRABLE(&oki_ring_work, ring_timer_work);
		schedule_delayed_work(&oki_ring_work, (HZ/50));
    	goto printvdp;
    }
    	
    vdp_name = strstr(vdp_str, "lobby");
    if(vdp_name == NULL) 
    {
    	strcpy(vdp_name_null, "none");
    	vdp_name = vdp_name_null;
    }

printvdp:
	    
    printk("VDP NAME: %s\n", vdp_name);

    type = script_get_item("ma009a_para", "p1cfg", &val);
    if (type != SCIRPT_ITEM_VALUE_TYPE_INT) {
		printk(KERN_ERR "A20 MA009A platform device register failed!\n");
        return -1;
    }
    p1cfg = val.val;
    printk("MA009A P1CFG: %02x\n", p1cfg);

    type = script_get_item("ma009a_para", "p1set", &val);
    if (type != SCIRPT_ITEM_VALUE_TYPE_INT) {
		printk(KERN_ERR "A20 MA009A platform device register failed!\n");
        return -1;
    }
    p1set = val.val;
    printk("MA009A P1SET: %02x\n", p1set);

    type = script_get_item("ma009a_para", "p2set", &val);
    if (type != SCIRPT_ITEM_VALUE_TYPE_INT) {
		printk(KERN_ERR "A20 MA009A platform device register failed!\n");
        return -1;
    }
    p2set = val.val;
    printk("MA009A P2SET: %02x\n", p2set);

	ret = platform_device_register(&nxp_ma009a_device);
	if (ret) {
		printk(KERN_ERR "A20 MA009A platform device register failed!\n");
		return -1;
	}

	ret = platform_driver_register(&nxp_ma009a_driver);
	if( ret != 0) {
		printk(KERN_ERR "A20 MA009A platform device register failed\n");
		return -1;
	}
	
	printk("A20 MA009 Driver, (c) 2013 dadamMicro INC\n");


	return 0;
}

void __exit nxp_ma009a_exit(void)
{
	gpio_free(ma009a_cd.gpio.gpio);
	gpio_free(ma009a_sda.gpio.gpio);
	gpio_free(ma009a_clk.gpio.gpio);
	
	platform_driver_unregister(&nxp_ma009a_driver);
	platform_device_unregister(&nxp_ma009a_device);
	
	printk("nxp2120 Wallpad MA009A module exit\n");
}

module_init(nxp_ma009a_init);
module_exit(nxp_ma009a_exit);

MODULE_AUTHOR("Duckbae Yun <dadpeat@dadammicro.com");
MODULE_DESCRIPTION("nxp2120 Wallpad MA009A Device Driver");
MODULE_LICENSE("GPL");
