#include <linux/module.h>
#include <linux/init.h>
#include <linux/pm.h>
#include <linux/fs.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/device.h>
#include <linux/clk.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/regulator/consumer.h>
#include <linux/uaccess.h>
#include <linux/bug.h>
#include <linux/errno.h>
#include <linux/resource.h>
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <linux/of_net.h>
#include <linux/of_gpio.h>
#include <linux/io.h>
#include <linux/sys_config.h>

//error number
#define ECFGNOEX            1   //config not exit
#define ECFGERR             2   //config error
#define ECAMNUMERR          3   //camera number error
#define ECLKSRC             4   //clock source error
#define EDETECTFAIL         5   //detect camera fail
#define EPMUPIN             6   //get pmu pin fail
#define ENORMALPIN          7   //get normal pin fail
#define ECFGPIN             8   //config pin fail
#define EI2CADAPTER         9   //get I2C adapter fail


#define IOCTL_CMXCTL_MAGIC     'G'  
#define DEFAULT_BUTTON		0x09

script_item_u joons;
typedef struct 
{ 
	unsigned long addr;
	unsigned long data;
} __attribute__ ((packed)) ioctl_cmxctl_info; 
 
 
#define IOCTL_CMXCTL_RD			_IOR(IOCTL_CMXCTL_MAGIC, 0 , ioctl_cmxctl_info )  
#define IOCTL_CMXCTL_WR			_IOW(IOCTL_CMXCTL_MAGIC, 1 , ioctl_cmxctl_info )  


static int a20_cmxctl_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int a20_cmxctl_release(struct inode *inode, struct file *file)
{
 	
    return 0;
}

static long a20_cmxctl_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{

    int ret;
    int sensor_state;
    unsigned int val;
    ioctl_cmxctl_info ctrl_info;  

	ret = 0;

}

static struct file_operations a20_cmxctl_fops = { 
	owner:      THIS_MODULE,
	open:    	a20_cmxctl_open,
	unlocked_ioctl:		a20_cmxctl_ioctl,
	release:    a20_cmxctl_release,
};
static struct miscdevice a20_cmxctl_dev = {
	minor:		142,
	name:		"a20-cmxctl",
	fops:		&a20_cmxctl_fops,
};

int a20_cmxctl_probe(struct platform_device *pdev)
{	
	int err;

	err = misc_register(&a20_cmxctl_dev);
	if(err) {
		printk (KERN_ERR "cannot register miscdev on minor=%d (%d)\n", 108, err);
	}

	return err;
	
}

static int a20_cmxctl_suspend(struct platform_device *dev, pm_message_t state)
{
	return 0;
}

static int a20_cmxctl_resume(struct platform_device *pdev)
{
	return 0;
}

static int a20_cmxctl_remove(struct platform_device *dev)
{
	misc_deregister(&a20_cmxctl_dev);

	return 0;
}

static struct platform_device a20_cmxctl_device = {
	.name           	= "a20-cmxctl",
	.id             	= -1,//??
};

static struct platform_driver a20_cmxctl_driver = {
       .probe          = a20_cmxctl_probe,
       .remove         = a20_cmxctl_remove,
       .suspend        = a20_cmxctl_suspend,
       .resume         = a20_cmxctl_resume,
       .driver		= {
		    .owner	= THIS_MODULE,
		    .name	= "a20-cmxctl",
	}, 
};

/*

	struct device_node *wpd_gpio_spk = NULL;
	struct gpio_config wpd_spkamp_en;

	wpd_gpio_spk = of_find_node_by_type(NULL, "wpd_gpio");
	wpd_spkamp_en.gpio = of_get_named_gpio_flags(wpd_gpio_spk, "spkamp_en", 0, (enum of_gpio_flags *)(&(wpd_spkamp_en)));

	if (!gpio_is_valid(wpd_spkamp_en.gpio))
			printk("%s: wpd_spkamp_en_rst_gpio is invalid. \n",__func__ );

	if(0 != gpio_request(wpd_spkamp_en.gpio, NULL)) 
		printk("####Get wpd_spkamp_en gpio failed####\n");

	gpio_direction_output(wpd_spkamp_en.gpio, 1);

*/


int __init a20_cmxctl_init(void)
{
	unsigned int ret;
	struct gpio_config wakeup_gpio;
	struct gpio_config wakeup_gpio1;
	struct device_node *np = NULL;
	np = of_find_node_by_name(NULL,"wpd_gpio");


	wakeup_gpio1.gpio = of_get_named_gpio_flags(np, "spkamp_en", 0, (enum of_gpio_flags *)(&(wakeup_gpio1)));
	if (!gpio_is_valid(wakeup_gpio1.gpio))
			pr_err("%s: wakeup_gpio is invalid. \n",__func__ );
	wakeup_gpio.gpio = of_get_named_gpio_flags(np, "door_pwr_en", 0, (enum of_gpio_flags *)(&(wakeup_gpio)));
	if (!gpio_is_valid(wakeup_gpio.gpio))
			pr_err("%s: wakeup_gpio is invalid. \n",__func__ );


	if(0 != gpio_request(wakeup_gpio1.gpio, NULL)) 
		printk("####Get wpd_spkamp_en gpio failed####\n");
	

	if(0 != gpio_request(wakeup_gpio.gpio, NULL)) 
		printk("####Get wpd_spkamp_en gpio failed####\n");



	gpio_direction_output(wakeup_gpio1.gpio, 0);
	msleep(20);
	gpio_direction_output(wakeup_gpio1.gpio, 1);

	gpio_direction_output(wakeup_gpio.gpio, 0);
	msleep(500);
	gpio_direction_output(wakeup_gpio.gpio, 1);
	msleep(500);
	gpio_direction_output(wakeup_gpio.gpio, 0);
	msleep(500);
	gpio_direction_output(wakeup_gpio.gpio, 1);
	msleep(500);
	gpio_direction_output(wakeup_gpio.gpio, 0);
	msleep(500);
	gpio_direction_output(wakeup_gpio.gpio, 1);
/*
	gpio_direction_output(wakeup_gpio.gpio, 1);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	msleep(1000);
	gpio_direction_output(wakeup_gpio.gpio, 0);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	msleep(1000);
	gpio_direction_output(wakeup_gpio.gpio, 1);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	msleep(1000);
	gpio_direction_output(wakeup_gpio.gpio, 0);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
*/

/*
	if (!gpio_is_valid(wpd_spkamp_en.gpio))
			printk("%s: wpd_spkamp_en_rst_gpio is invalid. \n",__func__ );

	if(0 != gpio_request(wpd_spkamp_en.gpio, NULL)) 
		printk("####Get wpd_spkamp_en gpio failed####\n");
	
	if (0 != gpio_direction_output(wpd_spkamp_en.gpio, 1))
		printk("####Set wpd_spkamp_en reset gpio failed####\n");
*/

#if 0
#if 1
	struct gpio_config wakeup_gpio;
	struct gpio_config wakeup_gpio1;
	struct device_node *np = NULL;
	np = of_find_node_by_name(NULL,"wpd_gpio");


	wakeup_gpio1.gpio = of_get_named_gpio_flags(np, "spkamp_en", 0, (enum of_gpio_flags *)(&(wakeup_gpio1)));
	if (!gpio_is_valid(wakeup_gpio1.gpio))
			pr_err("%s: wakeup_gpio is invalid. \n",__func__ );
	wakeup_gpio.gpio = of_get_named_gpio_flags(np, "door_pwr_en", 0, (enum of_gpio_flags *)(&(wakeup_gpio)));
	if (!gpio_is_valid(wakeup_gpio.gpio))
			pr_err("%s: wakeup_gpio is invalid. \n",__func__ );


	gpio_direction_output(wakeup_gpio1.gpio, 0);
	msleep(20);
	gpio_direction_output(wakeup_gpio1.gpio, 1);

	gpio_direction_output(wakeup_gpio.gpio, 1);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));

	msleep(1000);
	gpio_direction_output(wakeup_gpio.gpio, 0);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	msleep(1000);
	gpio_direction_output(wakeup_gpio.gpio, 1);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	msleep(1000);
	gpio_direction_output(wakeup_gpio.gpio, 0);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));

#else
	script_item_u val;
	struct gpio_config wakeup_gpio;
	struct gpio_config wakeup_gpio1;
	script_item_value_type_e type;
	struct device_node *np = NULL;
	np = of_find_node_by_type(NULL, "wpd_gpio");


//	wakeup_gpio.gpio = of_get_named_gpio_flags(np, "door_pwr_en", 0, (enum of_gpio_flags *)(&(wakeup_gpio)));
	wakeup_gpio.gpio = of_get_named_gpio(np, "door_pwr_en", 0);
	wakeup_gpio1.gpio = of_get_named_gpio(np, "led_ncall", 0);

	gpio_direction_output(wakeup_gpio.gpio, 1);
	gpio_direction_output(wakeup_gpio1.gpio, 1);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));

	msleep(1000);
	gpio_direction_output(wakeup_gpio.gpio, 0);
	gpio_direction_output(wakeup_gpio1.gpio, 0);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	msleep(1000);
	gpio_direction_output(wakeup_gpio.gpio, 1);
	gpio_direction_output(wakeup_gpio1.gpio, 1);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	msleep(1000);
	gpio_direction_output(wakeup_gpio.gpio, 0);
	gpio_direction_output(wakeup_gpio1.gpio, 0);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	msleep(1000);
	gpio_direction_output(wakeup_gpio.gpio, 1);
	gpio_direction_output(wakeup_gpio1.gpio, 1);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	msleep(1000);
	gpio_direction_output(wakeup_gpio.gpio, 0);
	gpio_direction_output(wakeup_gpio1.gpio, 0);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	msleep(1000);
	gpio_direction_output(wakeup_gpio.gpio, 1);
	gpio_direction_output(wakeup_gpio1.gpio, 1);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));

	gpio_direction_output(wakeup_gpio.gpio, 0);
	gpio_direction_output(wakeup_gpio1.gpio, 0);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
#endif
/*

	printk("joons data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	gpio_direction_output(joons.gpio, 1);
	msleep(1000);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	gpio_direction_output(joons.gpio, 0);
	msleep(1000);
	printk("joons0 data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	gpio_direction_output(joons.gpio, 1);
	msleep(1000);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	gpio_direction_output(joons.gpio, 0);
	msleep(1000);
	printk("joons0 data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	gpio_direction_output(joons.gpio, 1);
	msleep(1000);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	gpio_direction_output(joons.gpio, 0);
	msleep(1000);
	printk("joons0 data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));
	gpio_direction_output(joons.gpio, 1);
	msleep(1000);
	printk("wakeup_gpio data default = %d\n",__gpio_get_value(wakeup_gpio.gpio));

*/
#if 0
	ret = of_property_read_u32(np, "def_btn", &joons);
	printk("1 JOONS=0, joons=%d\n", joons);

	gpio_direction_output(ret, 0);
	of_property_read_u32("cmxctlcfg", "def_btn", joons);
	printk("2 JOONS=0, joons=%d\n", joons);

	gpio_direction_output(joons, 1);
	of_property_read_u32("cmxctlcfg", "def_btn", joons);
	printk("3 JOONS=0, joons=%d\n", joons);

	gpio_direction_output(joons, 0);
	of_property_read_u32("cmxctlcfg", "def_btn", joons);
	printk("4 JOONS=0, joons=%d\n", joons);

	gpio_direction_output(joons, 1);
	of_property_read_u32("cmxctlcfg", "def_btn", joons);
	printk("5 JOONS=0, joons=%d\n", joons);

	gpio_direction_output(joons, 0);
	of_property_read_u32("cmxctlcfg", "def_btn", joons);
	printk("6 JOONS=0, joons=%d\n", joons);

	gpio_direction_output(joons, 1);
	of_property_read_u32("cmxctlcfg", "def_btn", joons);
	printk("7 JOONS=0, joons=%d\n", joons);
#endif
#endif
	ret = platform_device_register(&a20_cmxctl_device);
	if (ret) {
		printk(KERN_ERR "A20 cmxctl platform device register failed!\n");
		return -1;
	}

	ret = platform_driver_register(&a20_cmxctl_driver);
	if( ret != 0) {
		printk(KERN_ERR "A20 cmxctl platform device register failed\n");
		return -1;
	}

	printk("A20 cmxctl Driver 1111, (c) 2017 dadamMicro INC\n");


	return 0;
}

void __exit a20_cmxctl_exit(void)
{
	platform_driver_unregister(&a20_cmxctl_driver);
	platform_device_unregister(&a20_cmxctl_device);
	
	printk("a20 Wallpad cmxctl module exit\n");
}

module_init(a20_cmxctl_init);
module_exit(a20_cmxctl_exit);

MODULE_AUTHOR("MyoungJoon <ohara@dadammicro.com");
MODULE_DESCRIPTION("A20 COMMAX IO contrl Device Driver");
MODULE_LICENSE("GPL");
